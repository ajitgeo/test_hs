from django.db import transaction
from rest_framework.views import APIView
from rest_framework import status
from ecp_hs_app.models import Patient, Session, Spiro, Pulseox, Survey
from .serializers import PatientSerializer, SessionSerializer, SpiroSerializer, PulseSerializer, SurveySerializer
from rest_framework.response import Response
from django.conf import settings
from rest_framework.parsers import FormParser, MultiPartParser
import os
from django.core.files.storage import default_storage
from .permissions import APIKeyAuth
import pandas as pd
from datetime import datetime, timedelta
import enum
from ECP_HS_DASHBOARD.database_functions import DBFunctions, DB, MODE
from ECP_HS_DASHBOARD.report_functions import ReportFunctions
from ECP_HS_DASHBOARD.statemachine import StateMachine
from ECP_HS_DASHBOARD.tasks import mytask, email_tech_support
import dill
from base64 import b64encode
import django_rq
from multiprocessing import Process
import time
import json
from pandas.io.json import json_normalize
from urllib.parse import urlparse
from ECP_HS_DASHBOARD.tests import Testing


# Using enum class create enumerations
class STATUS(enum.Enum):
    SUCCESS = 0
    NOPATIENTFOUND = 1
    SESSIONDUPLICATE = 2
    UNEXPECTEDERROR = 3
    SESSIONEXITS = 4


class FILESTATUS(enum.Enum):
    FILECORRUPTED = 1


class PatientView(APIView):
    permission_classes = (APIKeyAuth,)

    def get(self, request):
        try:
            imei = request.META['HTTP_IMEI']
            # ts = int(request.META['HTTP_TS'])
            ts = datetime.strptime(request.META['HTTP_TS'], '%Y%m%d_%H%M%S')
            today_date = ts.date()
            print('TS %s'%ts)
            print('Date %s'%today_date)
            if Patient.objects.filter(currentimei=int(imei)).exists():
                patient = Patient.objects.get(currentimei=int(imei))
                if Session.objects.filter(patientid=patient.id).exists():
                    session = Session.objects.filter(patientid=patient.id).order_by('-datetime')[0]
                else:
                    session = None
                sess_fields = ('last_session_ts', '')
                if patient.currentmode == MODE.PRESURV.value:
                    fields = ('currentmode', 'imei')
                elif patient.currentmode == MODE.CONVERTEDTOSURV.value or patient.currentmode == MODE.SURV.value:
                    fields = ('survhighfev1', 'survlowfev1', 'currentmode', 'imei')
                elif patient.currentmode == MODE.DIFFERENTARM.value or patient.currentmode == MODE.OUTOFSTUDY.value:
                    return Response(data={"detail": "Session not allowed", "currentmode": patient.currentmode},
                                    status=status.HTTP_406_NOT_ACCEPTABLE)                    
                pat_serializer = PatientSerializer(patient, fields=fields)
                if not session is None:
                    sess_serializer = SessionSerializer(session, fields=sess_fields)
                    serialized_data = {**pat_serializer.data, **sess_serializer.data}
                    session_exists = Session.objects.filter(patientid=patient.id, sessionmode=patient.currentmode,
                                                            datetime__contains=today_date).exists()
                    state, next_date = get_next_state(patient, session, 'GET', today_date=today_date)
                    next_session_dt = get_next_session_date(patient=patient, session=session, state=state, session_status=session_exists, dt=today_date, request_type='GET')
                    if session_exists:
                        return Response(data={"detail": "Session already completed for this date",
                                              "currentmode": patient.currentmode,
                                              'next_session_date': next_session_dt, **sess_serializer.data},
                                        status=status.HTTP_409_CONFLICT)
                        # return Response(data={"detail": "Session already completed for this date",
                        #                       "currentmode": patient.currentmode, 'next_session_date': session.nextsessiondate,
                        #                        **sess_serializer.data},
                        #                 status=status.HTTP_409_CONFLICT)
                    if state == 'NOT_ALLOWED':
                        return Response(data={"detail": "Session not allowed", "currentmode": patient.currentmode,'next_session_date': next_session_dt},
                        # return Response(data={"detail": "Session not allowed", "currentmode": patient.currentmode, 'next_session_date': session.nextsessiondate},
                                        status=status.HTTP_406_NOT_ACCEPTABLE)
                    next_state = {'current_state': state}
                    next_session_date = {'next_session_date': next_session_dt}
                    # next_session_date = {'next_session_date': session.nextsessiondate}
                    serialized_data = {**serialized_data, **next_state, **next_session_date}
                else:
                    if patient.currentmode == MODE.PRESURV.value:
                        sess_data = {'last_session_ts': None, 'current_state': 'PS', 'next_session_date': str(today_date)}
                    serialized_data = {**pat_serializer.data, **sess_data}
                return Response(serialized_data, status=status.HTTP_200_OK)
            else:
                return Response(data={"detail": "Patient with imei number %s not found in patient records" % imei},
                                status=status.HTTP_404_NOT_FOUND)
        except:
            return Response(data={"detail": "Unexpected error"}, status=status.HTTP_501_NOT_IMPLEMENTED)


class FileUploadView(APIView):
    permission_classes = (APIKeyAuth,)
    parser_classes = (FormParser, MultiPartParser)

    def post(self, request):
        try:
            data_folder = settings.MEDIA_ROOT
            imei = request.META['HTTP_IMEI']
            # ts = time.gmtime(int(request.META['HTTP_TS']))
            ts = datetime.strptime(request.META['HTTP_TS'], '%Y%m%d_%H%M%S')
            uri = request.build_absolute_uri()

            test = bool(int(request.META['HTTP_TEST']))

            report = ReportFunctions(imei=int(imei), uri=uri)
            patient = Patient.objects.get(currentimei=int(imei))

            try:
                files_list = request.FILES
            except:
                keywords = {'patientnum': patient.patientnum, 'error_code': '500',
                            'saved_file': None}
                django_rq.enqueue(email_tech_support, kwargs=keywords)
                # p = Process(target=email_tech_support, kwargs=keywords)
                # p.daemon = True
                # p.start()
                return Response(data={"upload_status": "file missing",
                                      "detail": "Session file could not be saved"},
                                status=status.HTTP_500_INTERNAL_SERVER_ERROR)
            for file in files_list:
                saved_file = os.path.join(data_folder, report.folder, imei, files_list[file].name)
                if os.path.isfile(saved_file):
                    os.remove(saved_file)
                try:
                    default_storage.save(saved_file, files_list[file])
                except:
                    keywords = {'patientnum': patient.patientnum, 'error_code': '500',
                                'saved_file': saved_file}
                    django_rq.enqueue(email_tech_support, kwargs=keywords)
                    # p = Process(target=email_tech_support, kwargs=keywords)
                    # p.daemon = True
                    # p.start()
                    return Response(data={"upload_status": "%s file not uploaded" % files_list[file].name,
                                          "detail": "Session file could not be saved"},
                                    status=status.HTTP_500_INTERNAL_SERVER_ERROR)
                if not test:
                    fields = file_parser(saved_file, patient)
                else:
                    convert = bool(int(request.META['HTTP_CONVERT']))
                    revert = bool(int(request.META['HTTP_REVERT']))
                    truepositive = bool(int(request.META['HTTP_TRUEPOSITIVE']))
                    pulse_histmeds = bool(int(request.META['HTTP_PULSEHISTMEDS']))
                    fields = file_parser(saved_file, patient, convert=convert, revert=revert, truepositive=truepositive, pulseoxhistmeds=pulse_histmeds)


                if fields == FILESTATUS.FILECORRUPTED:
                    keywords = {'patientnum': patient.patientnum, 'error_code': '406',
                                'saved_file': saved_file}                    
                    django_rq.enqueue(email_tech_support, kwargs=keywords)
                    return Response(data={"upload_status": "%s file not uploaded" % files_list[file].name,
                                          "detail": "Corrupted File"}, status=status.HTTP_406_NOT_ACCEPTABLE)

                if test:
                    unitTest = Testing()
                    unitTest.patient = patient
                    unitTest.url = urlparse(uri).netloc
                    unitTest.fields = fields
                    unitTest.check_report = False
                    unitTest.check_email = False
                    unitTest.check_revert()

                update_status = update_database_report(ts, fields, report, imei)
                if update_status == STATUS.SUCCESS:
                    session = Session.objects.filter(patientid=patient.id).order_by('-datetime')[0]
                    if report.report_gen:
                        rep_str = b64encode(dill.dumps(report.rep)).decode('utf-8')
                        report_str = b64encode(dill.dumps(report)).decode('utf-8')
                        keywords = {'myreport': report_str, 'rep': rep_str, 'csv': report.csv, 'rep_type':report.rep_type, 'email_rep': report.email,
                                    'site_id': patient.siteid.id, 'patientnum': patient.patientnum, 'slidenum': report.slidenum}
                        if test:
                            mytask(myreport=report_str, rep=rep_str, csv=report.csv, rep_type=report.rep_type, email_rep=report.email, site_id=patient.siteid.id,
                                   patientnum=patient.patientnum, slidenum=report.slidenum)
                            unitTest.report_id = report.report_id
                            unitTest.check_report = True
                            unitTest.report = report_str
                            if report.email:
                                unitTest.check_email = True
                        else:
                            django_rq.enqueue(mytask, kwargs=keywords)
                            # p = Process(target=mytask, kwargs=keywords)
                            # p.daemon = True
                            # p.start()
                        # os.system('killall --older-than 30s phantomjs')
                    # next_session_dt = get_next_session_date(patient=patient, session=session, state=session.nextsession, session_status=None, ts=time.gmtime(int(session.datetime.timestamp())),

                    if test:
                        unitTest.session = session
                        unitTest.session_complete()
                        session = Session.objects.filter(patientid=patient.id).order_by('-datetime')[0]
                        check_list = [unitTest.bool_val]
                        if hasattr(unitTest, 'cmi_out'):
                            check_list.append(unitTest.cmi_out)
                        if unitTest.check_report:
                            check_list.append(unitTest.report_found)

                        server_bool = {'server_ready': all(check_list)}

                    next_session_dt = get_next_session_date(patient=patient, session=session, state=session.nextsession, session_status=None, dt=session.datetime.date(),
                                                            request_type='POST')
                    response_data = {"upload_status": "%s file successfully uploaded on %s"
                                      % (files_list[file].name,
                                         (datetime.now()).strftime(
                                             '%B %d, %Y at %I:%M %p')),
                            'next_state': session.nextsession,
                            'next_session_date': next_session_dt}

                    if test:
                        response_data = {**response_data, **server_bool}

                    if session.nextsession == 'PER' or session.nextsession == 'PM':
                        spiro = Spiro.objects.filter(sessionid=session.id).order_by('-id')[0]
                        per_count = {'persistence_count': spiro.persistencecount}
                        response_data = {**response_data, **per_count}
                        return Response(data=response_data,
                                        status=status.HTTP_200_OK)
                    return Response(data=response_data,
                                    status=status.HTTP_200_OK)
                elif update_status == STATUS.NOPATIENTFOUND:
                    os.remove(saved_file)
                    return Response(data={"upload_status": "%s file not uploaded" % files_list[file].name,
                                          "detail":  "Patient with imei number %s not found in patient records" % imei},
                                    status=status.HTTP_404_NOT_FOUND)
                elif update_status == STATUS.SESSIONDUPLICATE:
                    os.remove(saved_file)
                    return Response(data={"upload_status": "%s file not uploaded" % files_list[file].name,
                                          "detail": "Session already completed for this date"},
                                    status=status.HTTP_409_CONFLICT)
                elif update_status == STATUS.UNEXPECTEDERROR:
                    keywords = {'patientnum': patient.patientnum, 'error_code': '501',
                                'saved_file': saved_file}
                    django_rq.enqueue(email_tech_support, kwargs=keywords)
                    # p = Process(target=email_tech_support, kwargs=keywords)
                    # p.daemon = True
                    # p.start()
                    return Response(data={"upload_status": "%s file not uploaded" % files_list[file].name,
                                          "detail": "Unexpected error"}, status=status.HTTP_501_NOT_IMPLEMENTED)
                elif update_status == STATUS.SESSIONEXITS:
                    return Response(data={"upload_status": "%s file not uploaded" % files_list[file].name,
                                          "detail": "This session file already exists"}, status=status.HTTP_409_CONFLICT)
        except:
            return Response(data={"detail": "Unexpected error"}, status=status.HTTP_501_NOT_IMPLEMENTED)


def get_next_state(patient, session, request_type, prev_date=None, today_date=None):
    pat_fields = ('historypulseormeds', '')
    pat_serializer = PatientSerializer(patient, fields=pat_fields)
    sess_fields = ('smweekcount', 'emweekcount', 'sessionstate', 'nextsessiondate', 'nextsession', 'truepositive')
    sess_serializer = SessionSerializer(session, fields=sess_fields)
    spiro_fields = ('last_session_persistence_count', 'spirovariance')
    spiro = Spiro.objects.get(sessionid=session.id)
    spiro_serializer = SpiroSerializer(spiro, fields=spiro_fields)
    if sess_serializer.data['sessionstate'] != 'EM' and sess_serializer.data['sessionstate'] != 'TM':
        pulse_fields = ('pulseboolean', 'o2satboolean')
        pulse = Pulseox.objects.get(sessionid=session.id)
        pulse_serializer = PulseSerializer(pulse, fields=pulse_fields)
        survey_fields = ('surveysymptom', '')
        try:
            survey = Survey.objects.get(sessionid=session.id)
            survey_serializer = SurveySerializer(survey, fields=survey_fields)
            serialized_data = {**pat_serializer.data, **sess_serializer.data, **spiro_serializer.data, **pulse_serializer.data, **survey_serializer.data}
        except:
            serialized_data = {**pat_serializer.data, **sess_serializer.data, **spiro_serializer.data, **pulse_serializer.data}
            serialized_data['surveysymptom'] = False
    else:
        serialized_data = {**pat_serializer.data, **sess_serializer.data, **spiro_serializer.data}
        serialized_data['pulseboolean'] = False
        serialized_data['surveysymptom'] = False
        serialized_data['o2satboolean'] = False
    start_state = serialized_data['sessionstate']
    if start_state == 'PS' and patient.currentmode in [2,3]:
        start_state = 'FM'
    var_sym = {'o2_sat': serialized_data['o2satboolean'], 'pulse': serialized_data['pulseboolean'],
               'spiro': serialized_data['spirovariance'], 'symptoms': serialized_data['surveysymptom'],
               'pulse_histmeds': serialized_data['historypulseormeds'], 'truepositive': serialized_data['truepositive']}
    weekDays = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]
    counter = 0
    if start_state == 'SM':
        counter = serialized_data['smweekcount']
    elif start_state == 'PER':
        counter = serialized_data['last_session_persistence_count']
    elif start_state == 'EM':
        counter = serialized_data['emweekcount']
    if request_type == 'GET':
        ideal_date = datetime.strptime(serialized_data['nextsessiondate'], '%Y-%m-%d').date()
    else:
        ideal_date = prev_date

    window = (today_date - ideal_date).days
    today = weekDays[today_date.weekday()]
    m = StateMachine()
    m.request_type = request_type
    m.add_state('Start_' + start_state, m.start_transitions)
    m.set_start('Start_' + start_state)
    m.run({'var_sym': var_sym, 'window': window, 'counter': counter, 'today': today, 'ideal_date': ideal_date, 'today_date': today_date,
           'nextState': ''})
    return m.newState.upper(), m.newDate


# def file_parser(file, patient):
#     try:
#         field_names = ['imei', 'session_date', 'tablet_version',
#                        'o2_var', 'pulse_var', 'spiro_var', 'symptoms', 'session_state',
#                        'fev1', 'fev2', 'fev3', 'fev4', 'fev5', 'fev6',
#                        'lowo2sat', 'minHR', 'maxHR', 'timeAbnormal', 'timeMinRate', 'raw_pef',
#                        'q1', 'q2', 'q3', 'q4', 'q5',
#                        'q11', 'q12', 'q13', 'q14', 'q15', 'q16', 'q17',
#                        'q21', 'q22', 'q23', 'q24',
#                        'q31', 'q32', 'q33', 'q34', 'q35',
#                        'q41', 'q42', 'q43',
#                        'q51', 'q52', 'q53', 'q54']
#         raw_pulse = ['pulserate', 'oxygensaturation', 'pulsestrength', 'perfusionindex', 'pulsewave1', 'pulsewave2',
#                      'pulsewave3']
#         raw_pef = ['peakflow', 'fev7', 'peakflowtime', 'extrapolatedvol']
#         df = pd.read_table(file, sep='\n', header=None)
#         raw_blows = ['pef', 'fev1', 'fvc', 'fev1fvcratio', 'fev6', 'fef2575']
#         if df.iloc[1].values[0].split('!')[1] == 'EM' or df.iloc[1].values[0].split('!')[1] == 'TM':
#             field_names = ['imei', 'session_date', 'tablet_version',
#                           'spiro_var', 'session_state',
#                            'fev1', 'fev2', 'fev3', 'fev4', 'fev5', 'fev6',
#                            'raw_pef']
#             blows = df[3:9]
#             blows.columns = ['data']
#             blows[raw_blows] = blows.data.str.split(expand=True)
#             blows = blows.drop(columns=['data']).reset_index(drop=True).astype(float)
#             frames = [df[0:3], df[9:]]
#             result = pd.concat(frames).reset_index(drop=True)
#             field_values = []
#             for index, row in result.iterrows():
#                 temp_values = row[0].split('!')
#                 if index == 2:
#                     temp_values.pop()
#                 field_values = field_values + temp_values
#             fields = zip(field_names, field_values)
#             fields = dict(fields)
#         else:
#             blows = df[4:10]
#             blows.columns = ['data']
#             blows[raw_blows] = blows.data.str.split(expand=True)
#             blows = blows.drop(columns=['data']).reset_index(drop=True).astype(float)
#             symptoms = bool(int(df.iloc[1].values[0].split('!')[3]))
#             if symptoms:
#                 sym_sub = df[-1:].values[0][0].split('!')
#                 sym_q = df[-2:].values[0][0].split('!')
#                 if len(sym_sub) < 23 or len(sym_q) < 5:
#                     return FILESTATUS.FILECORRUPTED
#                 else:
#                     frames = [df[0:4], df[10:11], df[-2:]]
#                     result = pd.concat(frames).reset_index(drop=True)
#                     raw = df[11:-2]
#             else:
#                 frames = [df[0:4], df[10:11]]
#                 result = pd.concat(frames).reset_index(drop=True)
#                 raw = df[11:]
#             field_values = []
#             for index, row in result.iterrows():
#                 temp_values = row[0].split('!')
#                 if index == 2:
#                     temp_values.pop()
#                 field_values = field_values + temp_values
#             fields = zip(field_names, field_values)
#             fields = dict(fields)
#             raw.columns = ['data']
#             raw[raw_pulse[0:5]] = raw.data.str.split(expand=True)
#             raw[raw_pulse[-3:]] = raw.pulsewave1.str.split(',', expand=True)
#             raw = raw.drop(columns=['data']).reset_index(drop=True).astype(float)
#             fields['raw_pulse'] = raw
#         fields['raw_pef'] = dict(zip(raw_pef, list(map(float, fields['raw_pef'].split(' ')))))
#         fields['raw_blows'] = blows
#         fields['smweekcount'] = 0
#         fields['emweekcount'] = 0
#         fields['persistence_count'] = 0
#         if Session.objects.filter(patientid=patient.id).exists():
#             session = Session.objects.filter(patientid=patient.id).order_by('-datetime')[0]
#             # if session.nextsession != fields['session_state']:
#             #     return FILESTATUS.FILECORRUPTED
#         return fields
#     except:
#         return FILESTATUS.FILECORRUPTED

def file_parser(file, patient, convert=False, revert=False, truepositive=False, pulseoxhistmeds=False):
    try:
        with open(file) as json_file:
            data = json.load(json_file)
        fields = {}
        field = json_normalize(data['generalInformation'])
        fields['session_date'] = field['time'][0]
        fields['tablet_version'] = field['versionNumber'][0]
        field = json_normalize(data['generalInformation']['userdata'])
        fields['imei'] = field['imei'][0]
        fields['session_state'] = field['currentSessionState'][0]
        field = json_normalize(data['spirometer']['fvcResults'])
        raw_pef = ['peakflow', 'fev7', 'peakflowtime', 'extrapolatedvol']
        raw_pef_dict = dict(zip(list(field.columns), raw_pef))
        field.rename(columns=raw_pef_dict, inplace=True)
        fields['raw_pef'] = field
        field = json_normalize(data['spirometer']['fev1Results'])
        raw_blows = ['pef', 'fev1', 'fvc', 'fev1fvcratio', 'fev6', 'fef2575']
        raw_blows_dict = dict(zip(list(field.columns), raw_blows))
        field.rename(columns=raw_blows_dict, inplace=True)
        fields['raw_blows'] = field
        fev1 = ['fev1', 'fev2', 'fev3', 'fev4', 'fev5', 'fev6']
        for index, fd in fields['raw_blows'].iterrows():
            fields[fev1[index]] = fd['fev1']
        field = json_normalize(data['spirometer'])
        fields['spiro_var'] = str(int(field['isSpiroVar'][0]))
        fields['survey'] = False

        if fields['session_state'] != 'EM' and fields['session_state'] != 'TM':
            field = json_normalize(data['pulseOx']).iloc[:,0:7]
            pulseox = ['minHR', 'maxHR', 'lowo2sat', 'timeAbnormal', 'timeMinRate', 'o2_var', 'pulse_var']
            pulseox_dict = dict(zip(list(field.columns), pulseox))
            field.rename(columns=pulseox_dict, inplace=True)
            field = field.to_dict('r')[0]
            fields = {**fields, **field}
            fields['pulse_var'] = str(int(fields['pulse_var']))
            fields['o2_var'] = str(int(fields['o2_var']))
            field = json_normalize(data['pulseOx']['pulseData'])
            field[['w1','w2','w3']] = pd.DataFrame(field.waves.tolist())
            field.drop(['waves', 'timeDiff'],axis=1,inplace=True)
            raw_pulse = ['pulserate', 'oxygensaturation', 'pulsestrength', 'perfusionindex', 'pulsewave1', 'pulsewave2', 'pulsewave3']
            raw_pulse_dict = dict(zip(list(field.columns), raw_pulse))
            field.rename(columns=raw_pulse_dict, inplace=True)
            fields['raw_pulse'] = field
            if 'survey' in data:
                fields['survey'] = True
                field = json_normalize(data['survey'])
                field[['q1', 'q2', 'q3', 'q4', 'q5']] = pd.DataFrame(field.qAnsArr.tolist())
                field[['q11', 'q12', 'q13', 'q14', 'q15', 'q16', 'q17']] = pd.DataFrame(field.q1SubArr.tolist())
                field[['q21', 'q22', 'q23', 'q24']] = pd.DataFrame(field.q2SubArr.tolist())
                field[['q31', 'q32', 'q33', 'q34', 'q35']] = pd.DataFrame(field.q3SubArr.tolist())
                field[['q41', 'q42', 'q43']] = pd.DataFrame(field.q4SubArr.tolist())
                field[['q51', 'q52', 'q53', 'q54']] = pd.DataFrame(field.q5SubArr.tolist())
                field.drop(['qAnsArr','q1SubArr','q2SubArr','q3SubArr','q4SubArr','q5SubArr'],axis=1,inplace=True)
                survey = ['symptoms', 'q1', 'q2', 'q3', 'q4', 'q5', 'q11', 'q12', 'q13', 'q14', 'q15', 'q16', 'q17', 'q21', 'q22', 'q23', 'q24', 'q31', 'q32', 'q33', 'q34', 'q35', 'q41', 'q42', 'q43', 'q51', 'q52', 'q53', 'q54']
                survey_dict = dict(zip(list(field.columns), survey))
                field.rename(columns=survey_dict, inplace=True)
                field = field.to_dict('r')[0]
                fields = {**fields, **field}
                fields['symptoms'] = str(int(fields['symptoms']))
            else:
                fields['symptoms'] = '0'
        fields['smweekcount'] = 0
        fields['emweekcount'] = 0
        fields['persistence_count'] = 0

        # fields['convert'] = data['convert']
        # fields['revert'] = data['revert']

        fields['convert'] = convert
        fields['revert'] = revert
        if fields['revert']:
            cur_state = fields['session_state']
            fields['session_state'] = 'FM'
        fields['truepositive'] = truepositive
        fields['pulseoxhistmeds'] = pulseoxhistmeds
        # fields['truepositive'] = False
        # fields['pulseoxhistmeds'] = False

        if Session.objects.filter(patientid=patient.id).exists():
            session = Session.objects.filter(patientid=patient.id).order_by('-datetime')[0]
            if session.nextsession != fields['session_state']:
                return FILESTATUS.FILECORRUPTED

        if fields['revert']:
            fields['session_state'] = cur_state

        return fields
    except:
        return FILESTATUS.FILECORRUPTED


@transaction.atomic
def update_database_report(ts, fields, report, imei):
    try:
        db = DBFunctions()
        db.app = 'API'
        db.imei = int(imei)
        db.fields = fields
        db.survey_bool = db.fields['survey']
        # cached_file = False
        if Patient.objects.filter(currentimei=db.imei).exists():
            db.query_patient()
            db.tmpstmp = datetime.strptime(db.fields['session_date'], '%Y%m%d_%H%M%S')
            db.date = str(db.tmpstmp).split(' ')[0]
            if Session.objects.filter(patientid=db.patient[0]['id'], sessionmode=db.patient[0]['currentmode'],
                                      datetime=db.tmpstmp).exists():
                return STATUS.SESSIONEXITS
            if not Session.objects.filter(patientid=db.patient[0]['id'], sessionmode=db.patient[0]['currentmode'],
                                          datetime__contains=db.date).exists():
                if Session.objects.filter(patientid=db.patient[0]['id'],
                                          sessionmode=db.patient[0]['currentmode']).exists():
                    db.session_count = Session.objects.filter(patientid=db.patient[0]['id'],
                                                              sessionmode=db.patient[0]['currentmode']).count() + 1
                    if db.fields['session_state'] == 'PER':
                        session = Session.objects.filter(patientid=db.patient[0]['id']).order_by('-datetime')[0]
                        spiro = Spiro.objects.get(sessionid=session.id)
                        db.fields['persistence_count'] = spiro.persistencecount + 1
                        # if session.nextsession == 'PM':
                        #     cur_dt = datetime.strptime(db.fields['session_date'], '%Y%m%d_%H%M%S').date()
                        #     if (session.nextsessiondate - cur_dt).days > 0:
                        #         cached_file = True
                        #         db.nextsession = 'PER'
                        #         db.nextsessiondate = get_next_day(session.datetime.date())
                        #         db.truepositive = session.truepositive
                        #         db.session_id = session.id
                        #         db.update_session(DB.UPDATE)
                    if db.fields['session_state'] == 'SM':
                        session = Session.objects.filter(patientid=db.patient[0]['id']).order_by('-datetime')[0]
                        db.fields['smweekcount'] = session.smweekcount + 1
                    if db.fields['session_state'] == 'EM':
                        session = Session.objects.filter(patientid=db.patient[0]['id']).order_by('-datetime')[0]
                        db.fields['emweekcount'] = session.emweekcount + 1
                        db.fields['o2_var'] = '0'
                        db.fields['pulse_var'] = '0'
                        db.fields['symptoms'] = '0'                        
                else:
                    db.session_count = 1

                if int(db.patient[0]['currentmode']) == MODE.CONVERTEDTOSURV.value:
                    db.today_dt = db.tmpstmp.date()
                    # db.today_dt = datetime.fromtimestamp(int(ts)).date()
                    db.update_patient()
                    db.query_patient()

                db.update_session(DB.INSERT)
                db.get_data()
                db.query_session()
                db.update_spiro(DB.INSERT)
                db.query_spiro()
                db.update_spiro_raw()
                if db.fields['session_state'] != 'EM' and db.fields['session_state'] != 'TM':
                    db.update_survey()
                    db.update_pulseox(DB.INSERT)
                    db.query_pulseox()
                    db.update_pulseox_raw()
                db.get_stats()
                db.update_patient()
                db.update_spiro(DB.UPDATE)
                db.query_patient()
                db.session_id = db.session[0]['id']
                db.truepositive = False
                db.pulsehistmeds = db.patient[0]['historypulseormeds']

                if int(db.patient[0]['currentmode']) == MODE.PRESURV.value:
                    report.report_gen = False
                    report.email = False
                    report.slidenum = 0
                    if db.session_count >= 30 and (db.session_count - 30) % 7 == 0:
                        db.spiro_val_cols = ['patientnum', 'fev11', 'fev12', 'fev13', 'fev14', 'fev15', 'fev16',
                                             'maxfev1', 'presurvhighfev1', 'presurvmeanfev1', 'fev1k', 'datetime',
                                             'starttestdate', 'dateoftransplant', 'sessionmode', 'sessionstate']
                        db.get_report_data()
                        report.create_update_presurv_report_data(db)
                        db.assessment = False
                        db.senttosite = False
                        db.update_report(DB.INSERT)
                        db.query_report()
                        db.report_id = db.report[0]['id']
                        report.report_id = int(db.report[0]['id'])
                        db.update_report_details()
                        report.rep.excel = False
                        report.rep.summary_table()
                        if db.session_count <= 44:
                            summary = report.rep.summary_df.tail(1)
                        elif db.session_count >= 51:
                            summary = report.rep.summary_df.tail(3)
                        for index, db.rep_summary in summary.iterrows():
                            db.summarytype = db.rep_summary['Weeks from Start Date']
                            db.update_report_stats()
                        report.csv = db.csv_filename
                        report.rep_type = db.rep_type
                        report.report_gen = True
                    db.nextsession = 'PS'
                    # today_date = datetime.fromtimestamp(int(ts)).date()
                    today_date = db.tmpstmp.date()
                    db.nextsessiondate = today_date + timedelta(1)
                    db.update_session(DB.UPDATE)                    
                   

                if int(db.patient[0]['currentmode']) == MODE.SURV.value:
                    db.parse_truthtable()
                    if db.tt_data['reportgen'][0]:
                        db.spiro_val_cols = ['patientnum', 'fev11', 'fev12', 'fev13', 'fev14', 'fev15', 'fev16',
                                             'maxfev1', 'survhighfev1', 'survmeanfev1', 'fev1k', 'datetime',
                                             'starttestdate', 'dateoftransplant', 'sessionmode', 'sessionstate']
                        db.get_report_data()
                        report.create_update_variance_report_data(db)
                        if db.tt_data['emailreport'][0]:
                            db.senttosite = True
                        db.update_report(DB.INSERT)
                        db.query_report()
                        db.report_id = db.report[0]['id']
                        report.report_id = int(db.report[0]['id'])
                        db.update_report_details()
                        report.rep.excel = False
                        report.rep.summary_table(report)
                        summary = report.rep.summary_df
                        for index, db.rep_summary in summary.iterrows():
                            db.summarytype = db.rep_summary['Weeks from Current Session']
                            db.update_report_stats()
                        report.csv = db.csv_filename
                        report.rep_type = db.rep_type
                        report.slidenum = db.tt_data['slidenum'][0]
                    patient = Patient.objects.get(id=db.patient[0]['id'])
                    session = Session.objects.filter(patientid=db.patient[0]['id']).order_by('-datetime')[0]
                    prev_session = Session.objects.filter(patientid=db.patient[0]['id']).order_by('-datetime')[1]
                    prev_date = prev_session.nextsessiondate
                    # today_date = datetime.fromtimestamp(int(ts)).date()
                    today_date = db.tmpstmp.date()
                    db.nextsession, db.nextsessiondate = get_next_state(patient, session, 'POST', prev_date=prev_date, today_date=today_date)
                    # if cached_file and db.nextsession == 'PER':
                    #     db.nextsession = 'PM'
                    #     cur_dt = datetime.strptime(db.fields['session_date'], '%Y%m%d_%H%M%S').date()
                    #     db.nextsessiondate = get_next_monday(cur_dt)
                    db.update_session(DB.UPDATE)
                    report.email = db.tt_data['emailreport'][0]
                    report.report_gen = db.tt_data['reportgen'][0]
                return STATUS.SUCCESS
            else:
                return STATUS.SESSIONDUPLICATE
        else:
            return STATUS.NOPATIENTFOUND
    except:
        transaction.set_rollback(True)
        return STATUS.UNEXPECTEDERROR


def get_next_session_date(patient=None, session=None, state=None, session_status=None,dt=None, request_type=None):
    # dt = datetime(ts.tm_year, ts.tm_mon, ts.tm_mday).date()
    # dt = datetime.fromtimestamp(ts).date()
    if request_type == 'GET':
        if patient.currentmode == MODE.PRESURV.value:
            if state == 'NOT_ALLOWED':
                next_sess_dt = get_next_day(dt)
            elif state == 'PS':
                next_sess_dt = dt
        elif patient.currentmode == MODE.CONVERTEDTOSURV.value or patient.currentmode == MODE.SURV.value:
            if state == 'NOT_ALLOWED':
                if session.nextsession != 'PER':
                    next_sess_dt = get_next_monday(dt)
                elif session.nextsession == 'PER':
                    if dt.weekday() in [0,1,2,3]:
                        next_sess_dt = get_next_day(dt)
                    elif dt.weekday() > 3:
                        next_sess_dt = get_next_monday(dt)
            elif state == 'PER':
                if dt.weekday() in [0, 1, 2, 3]:
                    next_sess_dt = dt
                elif dt.weekday() > 3 and session_status:
                    next_sess_dt = get_next_monday(dt)
                elif dt.weekday() > 3:
                    next_sess_dt = dt
            elif state in ['FM','SM','PM','EM','TM']:
                next_sess_dt = dt
    if request_type == 'POST':
        if state == 'PS':
            next_sess_dt = get_next_day(dt)
        elif state in ['FM','SM','PM','EM','TM']:
            next_sess_dt = get_next_monday(dt)
        elif state == 'PER':
            if dt.weekday() in [0,1,2,3]:
                next_sess_dt = get_next_day(dt)
            elif dt.weekday() > 3:
                next_sess_dt = get_next_monday(dt)
    return str(next_sess_dt)


def get_next_monday(dt):
    if dt.weekday() == 0:
        dt += timedelta(1)
    while dt.weekday() != 0:
        dt += timedelta(1)
    return dt

def get_next_day(dt):
    dt += timedelta(1)
    return dt

