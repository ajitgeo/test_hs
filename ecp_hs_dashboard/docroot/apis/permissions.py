from rest_framework.permissions import BasePermission
import base64
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC
import time
from decouple import config


class APIKeyAuth(BasePermission):
    def has_permission(self, request, view):
        # API_KEY should be in request headers to authenticate requests
        password = config('API_PASSWORD')
        salt = config('API_SALT').encode()
        received_api_key = request.META['HTTP_API']
        # ts = request.META['HTTP_TS']
        kdf = PBKDF2HMAC(
            algorithm=hashes.SHA256(),
            length=32,
            salt=salt,
            iterations=100000,
            backend=default_backend()
        )
        generated_key = base64.urlsafe_b64encode(kdf.derive(password.encode())).hex()

        # return received_api_key == generated_key and (abs(int(ts) - int(time.time())) < 130)
        return received_api_key == generated_key