from rest_framework import serializers
from ecp_hs_app.models import Patient, Session, Spiro, Pulseox, Survey


class DynamicFieldsModelSerializer(serializers.ModelSerializer):
    """
    A ModelSerializer that takes an additional `fields` argument that
    controls which fields should be displayed.
    """

    def __init__(self, *args, **kwargs):
        # Don't pass the 'fields' arg up to the superclass
        fields = kwargs.pop('fields', None)

        # Instantiate the superclass normally
        serializers.ModelSerializer.__init__(self, *args, **kwargs)

        if fields is not None:
            # Drop any fields that are not specified in the `fields` argument.
            allowed = set(fields)
            existing = set(self.fields.keys())
            for field_name in existing - allowed:
                self.fields.pop(field_name)


class PatientSerializer(DynamicFieldsModelSerializer):
    imei = serializers.IntegerField(source='currentimei')
    class Meta:
        model = Patient
        fields = '__all__'

class SessionSerializer(DynamicFieldsModelSerializer):
    last_session_ts = serializers.SerializerMethodField('get_datetime')

    def get_datetime(self, obj):
        return int(obj.datetime.timestamp())

    class Meta:
        model = Session
        fields = '__all__'

class SpiroSerializer(DynamicFieldsModelSerializer):
    last_session_persistence_count = serializers.SerializerMethodField('get_persistencecount')

    def get_persistencecount(self, obj):
        return obj.persistencecount

    class Meta:
        model = Spiro
        fields = '__all__'

class PulseSerializer(DynamicFieldsModelSerializer):

    class Meta:
        model = Pulseox
        fields = '__all__'

class SurveySerializer(DynamicFieldsModelSerializer):

    class Meta:
        model = Survey
        fields = '__all__'