from django.urls import re_path
from .views import PatientView, FileUploadView, CrashRepUploadView


urlpatterns = [
    re_path(r'^data/$', PatientView.as_view()),
    re_path(r'^upload/$', FileUploadView.as_view()),
    re_path(r'^crashrep/$', CrashRepUploadView.as_view()),
]