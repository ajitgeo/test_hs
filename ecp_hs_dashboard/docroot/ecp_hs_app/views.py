import django

django.setup()
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .forms import MyUserLoginForm, PreSurvForm, SurvForm, MonForm
import os
from django.contrib.auth import (
    authenticate,
    login,
    logout,
)
from .models import Site, Patient, Session, Report
from datetime import datetime, timedelta
from django.http import JsonResponse
from multiprocessing import Process
from ECP_HS_DASHBOARD.report_functions import ReportFunctions
from ECP_HS_DASHBOARD.database_functions import DBFunctions, MODE, DB
import pandas as pd
from ECP_HS_DASHBOARD.tasks import modify_report
from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm
import django_rq
# Create your views here.
@login_required(login_url='ecp_hs_app:login')
def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            messages.success(request, 'Your password was successfully updated!')
            return redirect('ecp_hs_app:home')
        else:
            messages.error(request, 'Please correct the error below.')
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'ecp_hs_app/change_password.html', {'form': form})


@login_required(login_url='ecp_hs_app:login')
def home(request):
    context = {"home_page": "active"}
    return render(request, 'ecp_hs_app/home.html', context)


@login_required(login_url='ecp_hs_app:login')
def dashboard_all_sites(request):
    sites = Site.objects.raw('select * from site order by site.id')
    pat_tot = Site.objects.raw(
        'select site.id, count(patient.id) as pat_tot from site left join patient on patient.siteid=site.id group by site.id order by site.id')
    presurv_tot = Site.objects.raw(
        'select site.id, count(patient.id) as presurv_tot from site left join patient on patient.siteid=site.id and patient.currentmode=1 group by site.id order by site.id')
    surv_tot = Site.objects.raw(
        'select site.id, count(patient.id) as surv_tot from site left join patient on patient.siteid = site.id and (patient.currentmode = 2 or patient.currentmode = 3) group by site.id order by site.id')
    sessions_tot = Site.objects.raw(
        'select site.id, count(session.id) as sessions_tot from site left join patient on patient.siteid = site.id left join session on session.patientid = patient.id '
        'group by site.id order by site.id')
    var_tot = Site.objects.raw(
        'select site.id, count(pulseox.pulseboolean) as var_tot from site left join patient on patient.siteid = site.id left join session on session.patientid = patient.id '
        'left join spiro on spiro.sessionid = session.id left join pulseox on pulseox.sessionid = session.id and (spiro.spirovariance = True or pulseox.pulseboolean = True or '
        'pulseox.o2satboolean = True) group by site.id order by site.id')
    ps_missed = Site.objects.raw(
        'select site.id, sum(missedsessions) as ps_missed from site left join missedsession on missedsession.siteid = site.id and sessionmode=1 '        
        ' group by site.id order by site.id')
    surv_missed = Site.objects.raw(
        'select site.id, sum(missedsessions) as surv_missed from site left join missedsession on missedsession.siteid = site.id and (sessionmode=2 or sessionmode=3) '
        ' and siteid=site.id group by site.id order by site.id')
    rep_tot = Site.objects.raw(
        'select site.id, count(report.id) as rep_tot from site left join patient on patient.siteid = site.id left join report on report.patientid = patient.id '
        'group by site.id order by site.id')
    cmi_yes = Site.objects.raw(
        'select site.id, count(report.id) as cmi_yes from site left join patient on patient.siteid = site.id left join report on report.patientid = patient.id '
        'and report.assessment = True group by site.id order by site.id')
    cmi_no = Site.objects.raw(
        'select site.id, count(report.id) as cmi_no from site left join patient on patient.siteid = site.id left join report on report.patientid = patient.id '
        'and report.assessment = False group by site.id order by site.id')

    # var_tot = Site.objects.raw(
    #     'select site.id, count(survey.surveysymptom) as var_tot from site left join patient on patient.siteid = site.id left join session on session.patientid = patient.id '
    #     'left join spiro on spiro.sessionid = session.id left join pulseox on pulseox.sessionid = session.id left join survey on survey.sessionid = session.id '
    #     'and (spiro.spirovariance = True or pulseox.pulseboolean = True or pulseox.o2satboolean = True and survey.surveysymptom=False) '
    #     'and (patient.currentmode = 2 or patient.currentmode = 3) group by site.id order by site.id')

    all_sites = zip(sites, pat_tot, presurv_tot, surv_tot, sessions_tot, var_tot, ps_missed, surv_missed, rep_tot, cmi_yes, cmi_no)
    context = {"all_sites": all_sites, "dashboard_page": "active"}
    return render(request, "ecp_hs_app/dashboard_all_sites.html", context)


@login_required(login_url='ecp_hs_app:login')
def dashboard_site(request, site_id):
    sites = Patient.objects.raw(
        'select patient.id, patient.patientnum, patient.enrolleddate, patient.studyarm, patient.presurvenddate, site.sitenum, site.name from'
        ' patient inner join site on patient.siteid = site.id where patient.siteid= %s order by patient.id',
        [site_id])
    sessions_tot = Patient.objects.raw(
        'select patient.id, count(session.id) as sessions_tot from patient left join session on session.patientid = patient.id and patient.siteid = %s '
        'group by patient.id order by patient.id', [site_id])
    search_strings = ["%Spiro%", "%Pulse%", "%O2sat%"]
    spiro_tot = Patient.objects.raw(
        'select patient.id, count(report.id) as spiro_tot from patient left join report on report.patientid = patient.id and patient.siteid = %s '
        'and senttosite = True and variancetype like %s group by patient.id order by patient.id', [site_id, search_strings[0]])
    pulse_tot = Patient.objects.raw(
        'select patient.id, count(report.id) as pulse_tot from patient left join report on report.patientid = patient.id and patient.siteid = %s '
        'and senttosite = True and variancetype like %s group by patient.id order by patient.id', [site_id, search_strings[1]])
    o2sat_tot = Patient.objects.raw(
        'select patient.id, count(report.id) as o2sat_tot from patient left join report on report.patientid = patient.id and patient.siteid = %s '
        'and senttosite = True and variancetype like %s group by patient.id order by patient.id', [site_id, search_strings[2]])
    surv_missed = Patient.objects.raw(
        'select patient.id, sum(missedsessions) as surv_missed from patient left join missedsession on missedsession.siteid = %s  and (sessionmode=2 or sessionmode=3) '
        ' and missedsession.patientid=patient.id group by patient.id order by patient.id', [site_id])
    ps_missed = Patient.objects.raw(
        'select patient.id, sum(missedsessions) as ps_missed from patient left join missedsession on missedsession.siteid = %s  and sessionmode=1 '
        ' and missedsession.patientid=patient.id group by patient.id order by patient.id', [site_id])

    site = zip(sites, sessions_tot, spiro_tot, pulse_tot, o2sat_tot, surv_missed, ps_missed)
    context = {"sites": sites, "site": site, "dashboard_page": "active", 'site_id': site_id}
    return render(request, "ecp_hs_app/dashboard_site.html", context)


@login_required(login_url='ecp_hs_app:login')
def dashboard_sessions(request, site_id, patient_id):
    sessions = Session.objects.raw(
        'select spiro.*, pulseox.*, survey.*, session.*, patient.currentmode, patient.enrolleddate, patient.patientnum, patient.studyarm, patient.presurvlowfev1, patient.presurvhighfev1, patient.survlowfev1, patient.survhighfev1,'
        'site.sitenum, site.name as sitename, site.id as siteid, report.reporttype, report.variancetype, report.symptoms, report.id as reportid from site left join patient on patient.siteid = site.id left join session on session.patientid = patient.id  '
        'left join report on report.patientid = patient.id and report.sessionid = session.id '
        'left join spiro on spiro.sessionid = session.id left join pulseox on pulseox.sessionid = session.id left join survey on survey.sessionid = session.id where patient.id = %s and site.id = %s order by session.datetime',
        [patient_id, site_id])
    for session in sessions:
        if session.datetime != None:
            tmpstmp = datetime.timestamp(session.datetime)
            session.datetime = datetime.fromtimestamp(tmpstmp).strftime("%Y-%m-%d")
            session.nextsessiondate = datetime.strptime(str(session.nextsessiondate), "%Y-%m-%d").strftime("%Y-%m-%d")

    context = {"sessions": sessions, "dashboard_page": "active", "site_id": site_id, "patient_id": patient_id}
    request.session['previous'] = request.path
    request.session['uri'] = request.build_absolute_uri()
    return render(request, "ecp_hs_app/dashboard_session.html", context)


@login_required(login_url='ecp_hs_app:login')
def dashboard_reports(request, site_id, patient_id):
    if Report.objects.filter(patientid=patient_id).exists():
        reports = Report.objects.raw(
            'select report.*, patient.currentmode, patient.enrolleddate, patient.patientnum, patient.studyarm, site.sitenum, site.name as sitename, site.id as siteid, '
            'session.sessionnum, session.datetime, report.reporttype, report.id as reportid from site left join patient on patient.siteid = site.id left join session on session.patientid = patient.id  '
            'left join report on report.sessionid = session.id where patient.id = %s and site.id = %s and report.id is not null order by report.id',
            [patient_id, site_id])
        for report in reports:
            if report.datetime != None:
                tmpstmp = datetime.timestamp(report.datetime)
                report.datetime = datetime.fromtimestamp(tmpstmp).strftime("%B %d, %Y, %I:%M %p")
    else:
        reports = Report.objects.raw(
            'select report.*, patient.currentmode, patient.enrolleddate, patient.patientnum, patient.studyarm, site.sitenum, site.name as sitename, site.id as siteid, '
            'session.sessionnum, session.datetime, report.reporttype, report.id as reportid from site left join patient on patient.siteid = site.id left join session on session.patientid = patient.id  '
            'left join report on report.sessionid = session.id where patient.id = %s and site.id = %s order by report.id limit 1',
            [patient_id, site_id])
        for report in reports:
            report.datetime = None
    context = {"reports": reports, "dashboard_page": "active", "site_id": site_id, "patient_id": patient_id}
    request.session['previous'] = request.path
    request.session['uri'] = request.build_absolute_uri()
    return render(request, "ecp_hs_app/dashboard_report.html", context)

@login_required(login_url='ecp_hs_app:login')
def all_worklist(request):
    if Report.objects.filter(assessment=True).exists():
        all_worklist = Report.objects.raw('select report.*, patient.currentmode, patient.patientnum, site.sitenum, site.name as sitename, '
                                          'session.sessionnum as sessnum, session.sessionmode from site left join patient on patient.siteid = site.id left join session on '
                                          'session.patientid = patient.id left join report on report.sessionid = session.id and report.assessment=True '
                                          'where report.id is not null order by report.reportreviewed desc')
        for wlist in all_worklist:
            if wlist.reportreviewed != None:
                tmpstmp = datetime.timestamp(wlist.reportreviewed)
                wlist.reportreviewed = datetime.fromtimestamp(tmpstmp).strftime("%B %d, %Y, %I:%M %p")
            if wlist.reportdate != None:
                wlist.reportdate = wlist.reportdate.strftime("%B %d, %Y")
    else:
        all_worklist = Report.objects.raw('select report.*, patient.currentmode, patient.patientnum, site.sitenum, site.name as sitename, '
                                          'session.sessionnum as sessnum from site left join patient on patient.siteid = site.id left join session on '
                                          'session.patientid = patient.id and session.sessionmode = patient.currentmode '
                                          'left join report on report.sessionid = session.id and report.assessment=True order by report.reportreviewed desc limit 1')
        for wlist in all_worklist:
            wlist.currentmode = None
            wlist.patientnum = None

    context = {"all_worklist": all_worklist, "all_worklist_page": "active"}
    request.session['previous'] = request.path
    request.session['uri'] = request.build_absolute_uri()
    return render(request, 'ecp_hs_app/all_worklist.html', context)

@login_required(login_url='ecp_hs_app:login')
def highpr_worklist(request):
    text = ''
    if 'text' in request.session:
        text = request.session['text']
    request.session['text'] = ''
    if Report.objects.filter(assessment=False).exists() and Report.objects.filter(cmipriority=True).exists():
        highpr_worklist = Report.objects.raw('select report.*, patient.currentmode, patient.patientnum, site.sitenum, site.name as sitename, '
                                            'session.sessionnum as sessnum, session.sessionmode from site left join patient on patient.siteid = site.id left join session on '
                                            'session.patientid = patient.id and session.sessionmode = patient.currentmode left join report on report.sessionid = session.id and '
                                            'report.assessment=False and report.cmipriority=True where report.id is not null order by report.reportdate desc')
        for wlist in highpr_worklist:
            if wlist.reportdate != None:
                wlist.reportdate = wlist.reportdate.strftime("%B %d, %Y")
    else:
        highpr_worklist = Report.objects.raw('select report.*, patient.currentmode, patient.patientnum, site.sitenum, site.name as sitename, '
                                            'session.sessionnum as sessnum, session.sessionmode from site left join patient on patient.siteid = site.id left join session on '
                                            'session.patientid = patient.id and session.sessionmode = patient.currentmode left join report on report.sessionid = session.id and '
                                            'report.assessment=False and report.cmipriority=True order by report.reportdate desc limit 1')
        for wlist in highpr_worklist:
            wlist.currentmode = None
            wlist.patientnum = None
    context = {"highpr_worklist": highpr_worklist, "highpr_worklist_page": "active", 'text': text}
    request.session['previous'] = request.path
    request.session['uri'] = request.build_absolute_uri()
    return render(request, 'ecp_hs_app/highpr_worklist.html', context)

@login_required(login_url='ecp_hs_app:login')
def lowpr_worklist(request):
    text = ''
    if 'text' in request.session:
        text = request.session['text']
    request.session['text'] = ''

    if Report.objects.filter(assessment=False).exists() and Report.objects.filter(cmipriority=False).exists():
        lowpr_worklist = Report.objects.raw('select report.*, patient.currentmode, patient.patientnum, site.sitenum, site.name as sitename, '
                                            'session.sessionnum as sessnum, session.sessionmode from site left join patient on patient.siteid = site.id left join session on '
                                            'session.patientid = patient.id and session.sessionmode = patient.currentmode left join report on report.sessionid = session.id and '
                                            'report.assessment=False and report.cmipriority=False where report.id is not null order by report.reportdate desc')
        for wlist in lowpr_worklist:
            if wlist.reportdate != None:
                wlist.reportdate = wlist.reportdate.strftime("%B %d, %Y")
    else:
        lowpr_worklist = Report.objects.raw('select report.*, patient.currentmode, patient.patientnum, site.sitenum, site.name as sitename, '
                                            'session.sessionnum as sessnum, session.sessionmode from site left join patient on patient.siteid = site.id left join session on '
                                            'session.patientid = patient.id and session.sessionmode = patient.currentmode left join report on report.sessionid = session.id and '
                                            'report.assessment=False and report.cmipriority=False order by report.reportdate desc limit 1')
        for wlist in lowpr_worklist:
            wlist.currentmode = None
            wlist.patientnum = None

    context = {"lowpr_worklist": lowpr_worklist, "lowpr_worklist_page": "active", 'text': text}
    request.session['previous'] = request.path
    request.session['uri'] = request.build_absolute_uri()
    return render(request, 'ecp_hs_app/lowpr_worklist.html', context)



@login_required(login_url='ecp_hs_app:login')
def report_view(request, report_id):
    report = Report.objects.raw('select report.*, (select sessionnum as sessnum from session where '
                                'report.patientid = session.patientid and '
                                'session.sessionmode = patient.currentmode and report.sessionid = session.id), '
                                'patient.currentmode from report inner join patient on '
                                'report.patientid = patient.id where report.id = %s', [report_id])
    report_select = Report.objects.get(id=int(report_id))
    patient = Patient.objects.get(id=report_select.patientid.id)
    session_num = [rep.sessnum for rep in report][0]
    report_type = [rep.reporttype for rep in report][0]
    report_file = [rep.filepath for rep in report][0]
    imei = os.path.split(report_file)[1].split('_')[0]
    db = DBFunctions()
    db.app = 'DASHBOARD'
    db.imei = int(imei)
    uri = request.build_absolute_uri()
    rep = ReportFunctions(int(imei), uri)
    report_data_pdf = os.path.join(rep.path,
                                   os.path.splitext(os.path.basename(report_file))[0] + '.pdf')
    user_perm = request.user.groups.filter(name__in=['Report_View_Only']).exists()
    decision_dict = {'PreSurv': 'Continuance of Pre-surveillance Testing', 'Surv': 'Conversion to Surveillance', 'Retrain': 'Unstable Baseline',
                     'ConvIneligible': 'Ineligible for Home Spirometry Cohort', 'FPV': 'Consistent with False Positive Variance',
                     'TPV': 'Persistent FEV1 variance without Symptoms and without Pulse Oximetry Alert Values; Consider BOS.'}
    if report_type == 'Conversion':
        if 'highprworklist' in request.session['previous'] or 'lowprworklist' in request.session['previous']:
            form = PreSurvForm(request.POST or None, session=session_num, user=user_perm)
            form_bool = True
        else:
            form_bool = False
            form = ''
    elif report_type == 'Variance':
        rep.get_summary(report_file)
        rep.get_raw()
        fev1_val = sorted(list(rep.raw_table['FEV1xK'].astype(int).unique()))
        low = patient.survlowfev1
        high = patient.survhighfev1

        fev1_val = [x for x in fev1_val if x <= low * 1000 or x >= high * 1000]
        fev1_lowchoices = [tuple([x, x]) for x in fev1_val if x <= low * 1000]
        fev1_highchoices = [tuple([x, x]) for x in fev1_val if x >= high * 1000]
        current_fev1range = {'fev1_low': int(low * 1000), 'fev1_high': int(high * 1000)}
        if 'highprworklist' in request.session['previous'] or 'lowprworklist' in request.session['previous']:
            form = SurvForm(request.POST or None, session=session_num, user=user_perm, fev1lowdata=fev1_lowchoices,
                            fev1highdata=fev1_highchoices, initial=current_fev1range)
            form_bool = True
        else:
            form_bool = False
            form = ''

    elif report_type == 'Monthly':
        if 'highprworklist' in request.session['previous'] or 'lowprworklist' in request.session['previous']:
            form = MonForm(request.POST or None, session=session_num, user=user_perm)
            form_bool = True
        else:
            form_bool = False
            form = ''

    if request.method != 'POST' and not request.is_ajax():
        context = {"report": report, 'from': request.GET.get('from', None), 'form': form, 'form_bool': form_bool, 'return_prev': True}
        # Report parser function
        db.query_survey_answers()
        rep.sur_ans = pd.DataFrame(db.survey_ans)
        rep.sur_ans.rename(columns={'id': 'Number', 'questionid': 'Survey Question Number','answer': 'Symptom'}, inplace=True)
        rep.report_parser(file=report_file, report_type=report_type)
        context = {**context, **rep.context}

    if request.method == 'POST':
        if form.is_valid():
            choices = form.cleaned_data.get('choices')
            comment = form.cleaned_data.get('comment')
            db.decision = decision_dict[choices]
            db.assessment = True
            db.report_id = report_id
            db.comment = comment
            db.patient = patient
            email = False
            if choices == 'PreSurv':
                db.mode = MODE.PRESURV.value
                db.senttosite = False
                db.update_report(DB.UPDATE)
                slidenum = 0
                rep_type = 'Conversion'
            elif choices == 'Surv':
                db.query_email_info(1)
                email = True
                db.mode = MODE.CONVERTEDTOSURV.value
                db.survfev1high = patient.presurvhighfev1
                db.survfev1low = patient.presurvlowfev1
                db.survfev1mean = patient.presurvmeanfev1
                db.presurvenddate = datetime.now().date()
                db.senttosite = True
                db.update_patient()
                db.update_report(DB.UPDATE)
                session = Session.objects.filter(patientid=patient.id).order_by('-datetime')[0]
                db.session_id = session.id
                db.nextsession = 'FM'
                # db.nextsessiondate = get_next_Monday()
                db.nextsessiondate = get_next_Monday(dt=session.datetime.date())
                db.truepositive = session.truepositive
                db.update_session(DB.UPDATE)
                slidenum = 1
                rep_type = 'Conversion'
            elif choices == 'Retrain':
                db.query_email_info(2)
                email = True
                db.senttosite = True
                db.update_report(DB.UPDATE)
                slidenum = 2
                rep_type = 'Conversion'
            elif choices == 'ConvIneligible':
                db.query_email_info(10)
                email = True
                db.mode = MODE.DIFFERENTARM.value
                db.senttosite = True
                db.presurvenddate = datetime.now().date()
                db.update_patient()
                db.update_report(DB.UPDATE)
                slidenum = 10
                rep_type = 'Conversion'
            elif choices == 'FPV':
                fev1_low = float(form.cleaned_data.get('fev1_low')) / 1000
                fev1_high = float(form.cleaned_data.get('fev1_high')) / 1000
                email = False
                db.mode = MODE.SURV.value
                db.survfev1high = fev1_high
                db.survfev1low = fev1_low
                db.survfev1mean = patient.survmeanfev1
                db.presurvenddate = patient.presurvenddate
                db.senttosite = False
                db.update_patient()
                db.update_report(DB.UPDATE)
                slidenum = 0
                rep_type = 'Variance'

            elif choices == 'TPV':
                db.query_email_info(5)
                email = True
                db.senttosite = True
                db.update_report(DB.UPDATE)
                session = Session.objects.get(id=report_select.sessionid.id)
                db.session_id = session.id
                db.nextsession = 'EM'
                db.nextsessiondate = get_next_Monday(dt=session.datetime.date())
                # db.nextsessiondate = get_next_Monday()
                db.truepositive = True
                db.update_session(DB.UPDATE)
                slidenum = 5
                rep_type = 'Variance'
            keywords = {'report_file': report_file, 'report_data_pdf': report_data_pdf,
                        'rep_type': rep_type, 'email': email, 'imei': imei, 'comment': comment, 'slidenum': slidenum,
                        'text': db.decision}
            django_rq.enqueue(modify_report, kwargs=keywords)
            redirect = request.session['previous']
            f_data = {
                'is_error': False,
                'error': '',
                'redirect_url': redirect,
                'text': 'Form successfully submitted'
            }
            request.session['text'] = f_data['text']
            return JsonResponse(f_data)
        else:
            form_errors = [{"field": k, "message": v[0]} for k, v in form.errors.items()]
            f_data = {
                'is_error': True,
                'error': form_errors[0]['message'],
                'redirect_url': '',
                'text': ''
            }
            return JsonResponse(f_data)
    if 'highprworklist' in request.session['previous']:
        context = {**context, **{"highpr_worklist_page": "active"}}
    elif 'lowprworklist' in request.session['previous']:
        context = {**context, **{"lowpr_worklist_page": "active"}}
    elif 'allworklist' in request.session['previous']:
        context = {**context, **{"all_worklist_page": "active"}}
    elif 'dashboard' in request.session['previous']:
        context = {**context, **{"dashboard_page": "active"}}
    return render(request, 'ecp_hs_app/report.html', context)



def login_view(request):
    if request.user.is_authenticated:
        return redirect('ecp_hs_app:home')
    form = MyUserLoginForm(request.POST or None)
    if request.method != 'POST':
        if 'next' in request.GET and 'password' in request.GET.get('next'):
            return render(request, 'ecp_hs_app/login.html', {'form': form, 'msg': 'Please sign in to change password'})
    if request.method == 'POST':
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            login(request, user)
            if 'next' in request.POST:
                return redirect(request.POST.get('next'))
            else:
                return redirect('ecp_hs_app:home')
    return render(request, 'ecp_hs_app/login.html', {'form': form, 'msg': ''})


@login_required(login_url='ecp_hs_app:login')
def logout_view(request):
    logout(request)
    return redirect('ecp_hs_app:login')

def get_next_Monday(dt=None):
    if dt != None:
        nextmon = dt
    else:
        nextmon = datetime.today().date()
    if nextmon.weekday() == 0:
        nextmon += timedelta(1)
    while nextmon.weekday() != 0:
        nextmon += timedelta(1)
    return nextmon
