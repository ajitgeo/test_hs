from django import forms
from django.contrib.auth import (
    authenticate,
)
from django.forms.widgets import Select


class MyUserLoginForm(forms.Form):

    username = forms.CharField(label='', widget=forms.TextInput(
        attrs={
            'class': 'form-control',
            'autofocus': '',
            'placeholder': 'Enter username...',
        }
    ), )

    password = forms.CharField(label='', widget=forms.PasswordInput(
        attrs={
            'class': 'form-control',
            'placeholder': 'Enter password...',
        }
    ), )

    # https://docs.djangoproject.com/en/2.1/ref/forms/validation/#cleaning-and-validating-fields-that-depend-on-each-other
    def clean(self):
        cleaned_data = super().clean()
        username = cleaned_data.get('username')
        password = cleaned_data.get('password')

        if username and password:
            user = authenticate(username=username, password=password)
            if not user:
                raise forms.ValidationError(
                    'Incorrect Username or Password')
            elif not user.check_password(password):
                raise forms.ValidationError(
                    'Incorrect Username or Password')
            elif not user.is_active:
                raise forms.ValidationError(
                    'User no longer valid')


class SelectWidget(Select):
    """
    Subclass of Django's select widget that allows disabling options.
    """
    def __init__(self, *args, **kwargs):
        self._disabled_choices = []
        super(SelectWidget, self).__init__(*args, **kwargs)

    @property
    def disabled_choices(self):
        return self._disabled_choices

    @disabled_choices.setter
    def disabled_choices(self, other):
        self._disabled_choices = other

    def create_option(self, name, value, label, selected, index, subindex=None, attrs=None):
        option_dict = super(SelectWidget, self).create_option(
            name, value, label, selected, index, subindex=subindex, attrs=attrs
        )
        if value in self.disabled_choices:
            option_dict['attrs']['disabled'] = 'disabled'
        return option_dict


class WorkListForms(forms.Form):
    choice = (())
    choices = forms.ChoiceField(choices=choice, label="", required=True,
                                widget=SelectWidget(attrs={'class': 'dropdown-content',
                                                           'align': 'start', }))
    comment = forms.CharField(label="", required=False, widget=forms.Textarea(attrs={
        'class': 'form-control',
        'placeholder': 'Enter comments here...',
        'rows': 2, 'cols': 15,
        'align': 'start',
    }))
    expand_radio = (())
    range_radio = forms.ChoiceField(widget=forms.RadioSelect, label="Would you like to widen FEV1 range?", required=False,
                                    choices=expand_radio)
    fev1_l = (())
    fev1_low = forms.ChoiceField(choices=fev1_l, label="Lower FEV1", required=False,
                                 widget=SelectWidget(attrs={'class': 'dropdown-content',
                                                            'align': 'start',
                                                            }))
    fev1_u = (())
    fev1_high = forms.ChoiceField(choices=fev1_u, label="Upper FEV1", required=False,
                                  widget=SelectWidget(attrs={'class': 'dropdown-content',
                                                             'align': 'start',
                                                             }))


    def clean(self):
        cleaned_data = super().clean()
        choices = cleaned_data.get('choices')
        if choices == self.value:
            raise forms.ValidationError(self.msg)
        fev1_low = cleaned_data.get('fev1_low')
        fev1_high = cleaned_data.get('fev1_high')
        if (fev1_high or fev1_low) is not None:
            if int(fev1_high) < int(fev1_low):
                raise forms.ValidationError(self.fev1_msg)

    def as_table(self):
        "Return this form rendered as HTML <tr>s -- excluding the <table></table>."
        return self._html_output(
            normal_row='<tr%(html_class_attr)s><th>%(label)s</th><td>%(errors)s%(field)s%(help_text)s</td></tr>',
            error_row='',
            row_ender='</td></tr>',
            help_text_html='<br><span class="helptext">%s</span>',
            errors_on_separate_row=False,
        )

    def __init__(self, *args, **kwargs):
        super(WorkListForms, self).__init__(*args, **kwargs)

        if self.no_perm:
            self.fields['choices'].widget.attrs['disabled'] = True
            self.fields['comment'].widget.attrs['readonly'] = True
            self.fields['fev1_low'].widget.attrs['disabled'] = True
            self.fields['fev1_high'].widget.attrs['disabled'] = True

class PreSurvForm(WorkListForms):

    def __init__(self, *args, **kwargs):
        self.session = kwargs.pop('session')
        self.no_perm = kwargs.pop('user')

        super(PreSurvForm, self).__init__(*args, **kwargs)
        self.fields['choices'].choices = (('CM', ""), ('PreSurv', "Continue Pre-surveillance Mode"),
                                          ('Surv', "Advance to Surveillance Mode"),
                                          ('Retrain', "Retraining"), ('ConvIneligible', "Conversion Ineligible"))
        self.value = "CM"
        self.msg = "Please Choose Report Decision"

        if self.session == 72:
            self.fields['choices'].widget.disabled_choices = ['Retrain']
        elif self.session == 44:
            self.fields['choices'].widget.disabled_choices = ['ConvIneligible']
        else:
            self.fields['choices'].widget.disabled_choices = ['Retrain', 'ConvIneligible']

        del self.fields["fev1_low"]
        del self.fields["fev1_high"]
        del self.fields["range_radio"]



class SurvForm(WorkListForms):

    def __init__(self, *args, **kwargs):
        self.session = kwargs.pop('session')
        self.no_perm = kwargs.pop('user')
        self.fev1lowdata = kwargs.pop('fev1lowdata')
        self.fev1highdata = kwargs.pop('fev1highdata')
        super(SurvForm, self).__init__(*args, **kwargs)
        self.fields['range_radio'].choices = [('1', 'No'), ('2', 'Yes')]
        self.fields['choices'].choices = (('RC', ""),
                                          ('FPV', "Consistent with False Positive Variance"),
                                          ('TPV', "Consistent with True Positive Variance"),
                                          )
        self.value = "RC"
        self.msg = "Please Choose Report Decision"
        self.fields['fev1_low'].choices = self.fev1lowdata
        self.fields['fev1_high'].choices = self.fev1highdata
        self.fev1_msg = "Upper FEV1 should be greater than Lower FEV1"




class MonForm(WorkListForms):

    def __init__(self, *args, **kwargs):
        self.session = kwargs.pop('session')
        self.no_perm = kwargs.pop('user')
        super(MonForm, self).__init__(*args, **kwargs)
        del self.fields["choices"]
        del self.fields["fev1_low"]
        del self.fields["fev1_high"]
        del self.fields["range_radio"]

    def clean(self):
        pass
