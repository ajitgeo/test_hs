from django.apps import AppConfig


class EcpHsAppConfig(AppConfig):
    name = 'ecp_hs_app'
