from django.contrib import admin
from .models import Site, Patient, Session, Report, Spiro, Pulseox, Survey, EmailInfo, TruthTable

# Register your models here.

admin.site.register(Site)
admin.site.register(Patient)
admin.site.register(Session)
admin.site.register(Report)
admin.site.register(Spiro)
admin.site.register(Pulseox)
admin.site.register(Survey)
admin.site.register(EmailInfo)
admin.site.register(TruthTable)
