from django.urls import path,re_path
from . import views


app_name = 'ecp_hs_app'

urlpatterns = [
    path('', views.home, name='home'),
    path('dashboard_all_sites/', views.dashboard_all_sites, name='dashboard_all_sites'),
    re_path(r'^dashboard_all_sites/(?P<site_id>\d+)/$', views.dashboard_site, name='dashboard_site'),
    re_path(r'^dashboard_all_sites/(?P<site_id>\d+)/(?P<patient_id>\d+)/$', views.dashboard_sessions, name='dashboard_sessions'),
    re_path(r'^dashboard_reports/(?P<site_id>\d+)/(?P<patient_id>\d+)/$', views.dashboard_reports, name='dashboard_reports'),
    path('allworklist/', views.all_worklist, name='all_worklist'),
    path('highprworklist/', views.highpr_worklist, name='highpr_worklist'),
    path('lowprworklist/', views.lowpr_worklist, name='lowpr_worklist'),
    re_path(r'^reports/(?P<report_id>\d+)/$', views.report_view, name='report_view'),
    path('login/', views.login_view, name='login'),
    path('logout/', views.logout_view, name='logout'),
]