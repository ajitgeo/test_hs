# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=150)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.BooleanField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=150)
    email = models.CharField(max_length=254)
    is_staff = models.BooleanField()
    is_active = models.BooleanField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.SmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class EmailInfo(models.Model):
    slidenum = models.IntegerField(blank=True, null=True)
    subject = models.CharField(max_length=100, blank=True, null=True)
    messagebody = models.CharField(max_length=1000, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'email_info'


class Missedsession(models.Model):
    siteid = models.ForeignKey('Site', models.DO_NOTHING, db_column='siteid', blank=True, null=True)
    patientid = models.ForeignKey('Patient', models.DO_NOTHING, db_column='patientid', blank=True, null=True)
    imei = models.CharField(max_length=20)
    sessionmode = models.IntegerField(blank=True, null=True)
    sessionstate = models.CharField(max_length=5, blank=True, null=True)
    datetime = models.DateTimeField(blank=True, null=True)
    nextsessiondate = models.DateField(blank=True, null=True)
    missedpersistence = models.BooleanField(blank=True, null=True)
    twomissedsessions = models.BooleanField(blank=True, null=True)
    psmissedsessions = models.BooleanField(blank=True, null=True)
    missedsessions = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'missedsession'


class Patient(models.Model):
    siteid = models.ForeignKey('Site', models.DO_NOTHING, db_column='siteid', blank=True, null=True)
    patientnum = models.IntegerField(blank=True, null=True)
    currentimei = models.IntegerField(blank=True, null=True)
    enrolleddate = models.DateField(blank=True, null=True)
    studyarm = models.CharField(max_length=64, blank=True, null=True)
    starttestdate = models.DateField(blank=True, null=True)
    presurvenddate = models.DateField(blank=True, null=True)
    dateoftransplant = models.DateField(blank=True, null=True)
    presurvhighfev1 = models.DecimalField(max_digits=16, decimal_places=2, blank=True, null=True)
    presurvlowfev1 = models.DecimalField(max_digits=16, decimal_places=2, blank=True, null=True)
    presurvmeanfev1 = models.DecimalField(max_digits=16, decimal_places=2, blank=True, null=True)
    survhighfev1 = models.DecimalField(max_digits=16, decimal_places=2, blank=True, null=True)
    survlowfev1 = models.DecimalField(max_digits=16, decimal_places=2, blank=True, null=True)
    survmeanfev1 = models.DecimalField(max_digits=16, decimal_places=2, blank=True, null=True)
    o2satmax = models.DecimalField(max_digits=16, decimal_places=0, blank=True, null=True)
    o2satmin = models.DecimalField(max_digits=16, decimal_places=0, blank=True, null=True)
    o2satmean = models.DecimalField(max_digits=16, decimal_places=0, blank=True, null=True)
    heartmax = models.DecimalField(max_digits=16, decimal_places=0, blank=True, null=True)
    heartmin = models.DecimalField(max_digits=16, decimal_places=0, blank=True, null=True)
    heartmean = models.DecimalField(max_digits=16, decimal_places=0, blank=True, null=True)
    currentmode = models.IntegerField(blank=True, null=True)
    survstartdate = models.DateField(blank=True, null=True)
    historypulseormeds = models.BooleanField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'patient'


class PatientDescription(models.Model):
    colname = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'patient_description'


class PulseDescription(models.Model):
    colname = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pulse_description'


class Pulseox(models.Model):
    sessionid = models.ForeignKey('Session', models.DO_NOTHING, db_column='sessionid', blank=True, null=True)
    maxheartrate = models.DecimalField(max_digits=16, decimal_places=0, blank=True, null=True)
    minheartrate = models.DecimalField(max_digits=16, decimal_places=0, blank=True, null=True)
    meanheartrate = models.DecimalField(max_digits=16, decimal_places=0, blank=True, null=True)
    timeheartrateabnormal = models.DecimalField(max_digits=16, decimal_places=0, blank=True, null=True)
    pulseboolean = models.BooleanField(blank=True, null=True)
    maxo2sat = models.DecimalField(max_digits=16, decimal_places=0, blank=True, null=True)
    mino2sat = models.DecimalField(max_digits=16, decimal_places=0, blank=True, null=True)
    meano2sat = models.DecimalField(max_digits=16, decimal_places=0, blank=True, null=True)
    timeo2satabnormal = models.DecimalField(max_digits=16, decimal_places=0, blank=True, null=True)
    o2satboolean = models.BooleanField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pulseox'


class PulseoxRaw(models.Model):
    pulseoxid = models.ForeignKey(Pulseox, models.DO_NOTHING, db_column='pulseoxid', blank=True, null=True)
    pulserate = models.IntegerField(blank=True, null=True)
    o2sat = models.IntegerField(blank=True, null=True)
    pulse_strength = models.IntegerField(blank=True, null=True)
    perfusion_index = models.DecimalField(max_digits=16, decimal_places=1, blank=True, null=True)
    pwave1 = models.IntegerField(blank=True, null=True)
    pwave2 = models.IntegerField(blank=True, null=True)
    pwave3 = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pulseox_raw'


class RawDescription(models.Model):
    colname = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'raw_description'


class Report(models.Model):
    patientid = models.ForeignKey(Patient, models.DO_NOTHING, db_column='patientid', blank=True, null=True)
    sessionid = models.ForeignKey('Session', models.DO_NOTHING, db_column='sessionid', blank=True, null=True)
    assessment = models.BooleanField(blank=True, null=True)
    cmipriority = models.BooleanField(blank=True, null=True)
    sitepriority = models.BooleanField(blank=True, null=True)
    startdate = models.DateField(blank=True, null=True)
    reportdate = models.DateTimeField(blank=True, null=True)
    reportreviewed = models.DateTimeField(blank=True, null=True)
    reportname = models.CharField(max_length=40, blank=True, null=True)
    reporttype = models.CharField(max_length=20, blank=True, null=True)
    variancetype = models.CharField(max_length=40, blank=True, null=True)
    symptoms = models.CharField(max_length=140, blank=True, null=True)
    decision = models.CharField(max_length=140, blank=True, null=True)
    senttosite = models.BooleanField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    filepath = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'report'


class ReportDescription(models.Model):
    colname = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'report_description'


class ReportDetails(models.Model):
    reportid = models.ForeignKey(Report, models.DO_NOTHING, db_column='reportid', blank=True, null=True)
    surveyid = models.ForeignKey('Survey', models.DO_NOTHING, db_column='surveyid', blank=True, null=True)
    fev1var = models.BooleanField(blank=True, null=True)
    fev1varpersist = models.BooleanField(blank=True, null=True)
    presurvhighfev1 = models.DecimalField(max_digits=16, decimal_places=2, blank=True, null=True)
    presurvlowfev1 = models.DecimalField(max_digits=16, decimal_places=2, blank=True, null=True)
    survhighfev1 = models.DecimalField(max_digits=16, decimal_places=2, blank=True, null=True)
    survlowfev1 = models.DecimalField(max_digits=16, decimal_places=2, blank=True, null=True)
    lowestfev1 = models.DecimalField(max_digits=16, decimal_places=2, blank=True, null=True)
    o2satmean_nl = models.DecimalField(max_digits=16, decimal_places=0, blank=True, null=True)
    o2satmin_nl = models.DecimalField(max_digits=16, decimal_places=0, blank=True, null=True)
    o2satmax_nl = models.DecimalField(max_digits=16, decimal_places=0, blank=True, null=True)
    o2satmean_abnl = models.DecimalField(max_digits=16, decimal_places=0, blank=True, null=True)
    o2satmin_abnl = models.DecimalField(max_digits=16, decimal_places=0, blank=True, null=True)
    o2satmax_abnl = models.DecimalField(max_digits=16, decimal_places=0, blank=True, null=True)
    o2satduration_abnl = models.DecimalField(max_digits=16, decimal_places=0, blank=True, null=True)
    heartmean_nl = models.DecimalField(max_digits=16, decimal_places=0, blank=True, null=True)
    heartmin_nl = models.DecimalField(max_digits=16, decimal_places=0, blank=True, null=True)
    heartmax_nl = models.DecimalField(max_digits=16, decimal_places=0, blank=True, null=True)
    heartmean_abnl = models.DecimalField(max_digits=16, decimal_places=0, blank=True, null=True)
    heartmin_abnl = models.DecimalField(max_digits=16, decimal_places=0, blank=True, null=True)
    heartmax_abnl = models.DecimalField(max_digits=16, decimal_places=0, blank=True, null=True)
    heartduration_abnl = models.DecimalField(max_digits=16, decimal_places=0, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'report_details'


class ReportStats(models.Model):
    reportid = models.ForeignKey(Report, models.DO_NOTHING, db_column='reportid', blank=True, null=True)
    summarytype = models.CharField(max_length=130, blank=True, null=True)
    mean = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    min = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    max = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    sd = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    cv = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    slope = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    rvalue = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    rsquare = models.DecimalField(max_digits=8, decimal_places=3, blank=True, null=True)
    sessions = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    pvalue = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'report_stats'


class Session(models.Model):
    patientid = models.ForeignKey(Patient, models.DO_NOTHING, db_column='patientid', blank=True, null=True)
    imei = models.CharField(max_length=20)
    sessionnum = models.IntegerField(blank=True, null=True)
    sessionmode = models.IntegerField(blank=True, null=True)
    sessionstate = models.CharField(max_length=5, blank=True, null=True)
    nextsession = models.CharField(max_length=5, blank=True, null=True)
    datetime = models.DateTimeField(blank=True, null=True)
    nextsessiondate = models.DateField(blank=True, null=True)
    smweekcount = models.IntegerField(blank=True, null=True)
    emweekcount = models.IntegerField(blank=True, null=True)
    truepositive = models.BooleanField(blank=True, null=True)
    tabletver = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'session'


class SessionDescription(models.Model):
    colname = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'session_description'


class Site(models.Model):
    sitenum = models.IntegerField(blank=True, null=True)
    name = models.CharField(max_length=64, blank=True, null=True)
    status = models.CharField(max_length=32, blank=True, null=True)
    irbversion = models.CharField(max_length=20, blank=True, null=True)
    irbdate = models.DateField(blank=True, null=True)
    protocolconvertdate = models.DateField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'site'
        unique_together = (('sitenum', 'name'),)


class SiteDescription(models.Model):
    colname = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'site_description'


class Sitepersonnel(models.Model):
    siteid = models.IntegerField(blank=True, null=True)
    title = models.CharField(max_length=64, blank=True, null=True)
    firstname = models.CharField(max_length=64, blank=True, null=True)
    lastname = models.CharField(max_length=64, blank=True, null=True)
    emailaddress = models.CharField(max_length=72, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'sitepersonnel'


class Spiro(models.Model):
    sessionid = models.ForeignKey(Session, models.DO_NOTHING, db_column='sessionid', blank=True, null=True)
    spirovariance = models.BooleanField(blank=True, null=True)
    persistencecount = models.IntegerField(blank=True, null=True)
    fev11 = models.DecimalField(max_digits=16, decimal_places=2, blank=True, null=True)
    fev12 = models.DecimalField(max_digits=16, decimal_places=2, blank=True, null=True)
    fev13 = models.DecimalField(max_digits=16, decimal_places=2, blank=True, null=True)
    fev14 = models.DecimalField(max_digits=16, decimal_places=2, blank=True, null=True)
    fev15 = models.DecimalField(max_digits=16, decimal_places=2, blank=True, null=True)
    fev16 = models.DecimalField(max_digits=16, decimal_places=2, blank=True, null=True)
    maxfev1 = models.DecimalField(max_digits=16, decimal_places=2, blank=True, null=True)
    maxpresurv = models.DecimalField(max_digits=16, decimal_places=2, blank=True, null=True)
    meanpresurv = models.DecimalField(max_digits=16, decimal_places=2, blank=True, null=True)
    percentmaxpresurv = models.DecimalField(max_digits=16, decimal_places=0, blank=True, null=True)
    percentmeanpresurv = models.DecimalField(max_digits=16, decimal_places=0, blank=True, null=True)
    fev1k = models.DecimalField(max_digits=16, decimal_places=2, blank=True, null=True)
    fev17 = models.DecimalField(max_digits=16, decimal_places=2, blank=True, null=True)
    peakflow = models.DecimalField(max_digits=16, decimal_places=2, blank=True, null=True)
    peakflowtime = models.IntegerField(blank=True, null=True)
    extrapolatedvol = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'spiro'


class SpiroDescription(models.Model):
    colname = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'spiro_description'


class SpiroRaw(models.Model):
    spiroid = models.ForeignKey(Spiro, models.DO_NOTHING, db_column='spiroid', blank=True, null=True)
    pef = models.DecimalField(max_digits=16, decimal_places=1, blank=True, null=True)
    fev1 = models.DecimalField(max_digits=16, decimal_places=2, blank=True, null=True)
    fvc = models.DecimalField(max_digits=16, decimal_places=2, blank=True, null=True)
    fevfvcratio = models.DecimalField(max_digits=16, decimal_places=2, blank=True, null=True)
    fev6 = models.DecimalField(max_digits=16, decimal_places=2, blank=True, null=True)
    fef2575 = models.DecimalField(max_digits=16, decimal_places=2, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'spiro_raw'


class Survey(models.Model):
    sessionid = models.ForeignKey(Session, models.DO_NOTHING, db_column='sessionid', blank=True, null=True)
    surveysymptom = models.BooleanField(blank=True, null=True)
    q1 = models.TextField(blank=True, null=True)
    q11 = models.TextField(blank=True, null=True)
    q12 = models.TextField(blank=True, null=True)
    q13 = models.TextField(blank=True, null=True)
    q14 = models.TextField(blank=True, null=True)
    q15 = models.TextField(blank=True, null=True)
    q16 = models.TextField(blank=True, null=True)
    q17 = models.TextField(blank=True, null=True)
    q2 = models.TextField(blank=True, null=True)
    q21 = models.TextField(blank=True, null=True)
    q22 = models.TextField(blank=True, null=True)
    q23 = models.TextField(blank=True, null=True)
    q24 = models.TextField(blank=True, null=True)
    q3 = models.TextField(blank=True, null=True)
    q31 = models.TextField(blank=True, null=True)
    q32 = models.TextField(blank=True, null=True)
    q33 = models.TextField(blank=True, null=True)
    q34 = models.TextField(blank=True, null=True)
    q35 = models.TextField(blank=True, null=True)
    q4 = models.TextField(blank=True, null=True)
    q41 = models.TextField(blank=True, null=True)
    q42 = models.TextField(blank=True, null=True)
    q43 = models.TextField(blank=True, null=True)
    q5 = models.TextField(blank=True, null=True)
    q51 = models.TextField(blank=True, null=True)
    q52 = models.TextField(blank=True, null=True)
    q53 = models.TextField(blank=True, null=True)
    q54 = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'survey'


class SurveyAnswers(models.Model):
    questionid = models.IntegerField(blank=True, null=True)
    answer = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'survey_answers'


class SurveyDescription(models.Model):
    colname = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'survey_description'


class SurveyQuestions(models.Model):
    questions = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'survey_questions'


class TruthTable(models.Model):
    sessionstate = models.CharField(max_length=5, blank=True, null=True)
    o2sat = models.BooleanField(blank=True, null=True)
    pulse = models.BooleanField(blank=True, null=True)
    spiro = models.BooleanField(blank=True, null=True)
    symptoms = models.BooleanField(blank=True, null=True)
    pulsehistmeds = models.BooleanField(blank=True, null=True)
    truepositive = models.BooleanField(blank=True, null=True)
    persistence = models.BooleanField(blank=True, null=True)
    reportgen = models.BooleanField(blank=True, null=True)
    sitereppriority = models.CharField(max_length=10, blank=True, null=True)
    cmireppriority = models.CharField(max_length=10, blank=True, null=True)
    emailreport = models.BooleanField(blank=True, null=True)
    slidenum = models.IntegerField(blank=True, null=True)
    amireport = models.BooleanField(blank=True, null=True)
    alertreport = models.BooleanField(blank=True, null=True)
    allowpersistence = models.BooleanField(blank=True, null=True)
    reportheaders = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'truth_table'
