import os
import django
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'ECP_HS_DASHBOARD.settings')
django.setup()
from django.core.mail import EmailMessage

subject = 'test'
message = 'test'
sender = 'notify@mir.wustl.edu'
# receivers = ['homespirotest@gowustl.onmicrosoft.com']
receivers = ['hstest1@gowustl.onmicrosoft.com']
# receivers = ['ajitgeorge@wustl.edu']
msg = EmailMessage(subject, message, sender, receivers)
msg.content_subtype = "html"
msg.send()
