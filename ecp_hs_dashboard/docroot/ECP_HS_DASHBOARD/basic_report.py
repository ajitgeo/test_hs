from openpyxl import load_workbook
import os
import pandas as pd
from xlsx2csv import Xlsx2csv


class BasicReport:
    def __init__(self, workbook_path=None, save_path=None):
        self.raw_df = pd.DataFrame(columns=['PTN', 'FEV11', 'FEV12', 'FEV13', 'FEV14', 'FEV15', 'FEV16',
                                            'FEV1MAX', 'MAX', 'Percent MAX', 'Mean', 'Percent Mean', 'FEV1xK',
                                            'Test Date', 'Test Start Date', 'Days', 'DTx',
                                            'Weeks from Start Date', 'Status'])
        self.summary_df = pd.DataFrame(columns=['Liters', 'Mean', 'Min', 'Max', 'SD 2', 'CV (2SD)', 'Slope (mL/M)',
                                                'R', 'R-square', 'Sessions', 'P-value']
                                       )
        self.workbook_path = 'Report.xlsx' if workbook_path == None else workbook_path
        self.wb = load_workbook(self.workbook_path)
        self.save_path = save_path

    def header_table(self):
        self.ws['E2'] = 'NA'
        self.ws['E3'] = 'NA'
        self.ws['E4'] = 'NA'
        self.ws['D6'] = 'NA'
        self.ws['E6'] = 'NA'
        self.ws['D7'] = 'NA'
        self.ws['E7'] = 'NA'
        self.ws['E9'] = 'NA'
        self.ws['E10'] = 'NA'
        self.ws['E11'] = 'NA'
        self.ws['I3'] = 'NA'
        self.ws['J3'] = 'NA'
        self.ws['I7'] = 'NA'
        self.ws['I8'] = 'NA'
        self.ws['I9'] = 'NA'
        self.ws['I10'] = 'NA'
        self.ws['I11'] = 'NA'
        self.ws['J7'] = 'NA'
        self.ws['J9'] = 'NA'
        self.ws['J10'] = 'NA'
        self.ws['J11'] = 'NA'
        self.ws['C12'] = 'NA'
        self.ws['C13'] = 'NA'

    def summary_table(self):
        pass

    def raw_table(self):
        book = load_workbook(self.save_path)
        writer = pd.ExcelWriter(self.save_path, engine='openpyxl')
        writer.book = book
        writer.sheets = {ws.title: ws for ws in writer.book.worksheets}
        self.raw_df.to_excel(writer, sheet_name='Report', startrow=120, index=False, engine='openpyxl')
        writer.save()

    def generateReport(self):
        pass

    def generatecsv(self):
        filename, ext = os.path.splitext(self.save_path)
        Xlsx2csv(self.save_path, outputencoding="utf-8").convert(filename + ".csv")

