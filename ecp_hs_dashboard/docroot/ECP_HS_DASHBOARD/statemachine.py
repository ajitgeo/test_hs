from datetime import timedelta

class StateMachine:
    def __init__(self):
        self.handlers = {}
        self.startState = None
        self.endStates = []
        self.add_state("is_symptom", self.is_symptom_transitions)
        self.add_state("not_symptom", self.not_symptom_transitions)
        self.add_state("is_spiro", self.is_spiro_transitions)
        self.add_state("not_spiro", self.not_spiro_transitions)
        self.add_state("is_o2sat", self.is_o2sat_transitions)
        self.add_state("not_o2sat", self.not_o2sat_transitions)
        self.add_state("is_pulse", self.is_pulse_transitions)
        self.add_state("not_pulse", self.not_pulse_transitions)
        self.add_state("is_truepositive", self.is_truepositive_transitions)
        self.add_state("not_truepositive", self.not_truepositive_transitions)
        self.add_state("not_allowed", None, end_state=1)
        self.add_state("PS", None, end_state=1)
        self.add_state("FM", None, end_state=1)
        self.add_state("PER", None, end_state=1)
        self.add_state("PM", None, end_state=1)
        self.add_state("SM", None, end_state=1)
        self.add_state("EM", None, end_state=1)
        self.add_state("error_state", None, end_state=1)


    def add_state(self, name, handler, end_state=0):
        name = name.upper()
        self.handlers[name] = handler
        if end_state:
            self.endStates.append(name)

    def set_start(self, name):
        self.startState = name.upper()

    def run(self, cargo):
        try:
            handler = self.handlers[self.startState]
        except:
            raise "InitializationError"(".set_start() has to be called before .run()")
        if not self.endStates:
            raise "InitializationError"(".set_start() has to be called before .run()")

        while True:
            (newState, cargo) = handler(cargo)
            if newState.upper() in self.endStates:
                self.newState = newState
                self.newDate = cargo['ideal_date']
                break
            else:
                handler = self.handlers[newState.upper()]

    def start_transitions(self, input):
        try:
            if self.startState == 'START_PS':
                # newState = "PS"
                if input['window'] >= 0:
                    newState = "PS"
                else:
                    newState = "not_allowed"
            if self.startState == 'START_FM' or self.startState == 'START_PER' or self.startState == 'START_PM':
                if input['var_sym']['o2_sat']:
                    newState = "is_o2sat"
                else:
                    newState = "not_o2sat"
            sm_counter = 3
            em_counter = 4
            if  self.startState == 'START_SM':
                if input['counter'] == sm_counter:
                    newState = "not_allowed"
                    if input['window'] >= 0:
                        if input['today'] == 'Monday' or input['today'] == 'Tuesday':
                            newState = "FM"
                            input['ideal_date'] = self.get_next_Monday(input)
                            print('Reset SM counter')
                elif input['counter'] != sm_counter:
                    if input['var_sym']['o2_sat']:
                        newState = "is_o2sat"
                    else:
                        newState = "not_o2sat"
                else:
                    newState = "error_state"

            if  self.startState == 'START_EM':
                if input['counter'] == em_counter:
                    newState = "not_allowed"
                    if input['window'] >= 0:
                        if input['today'] == 'Monday' or input['today'] == 'Tuesday':
                            newState = "FM"
                            input['ideal_date'] = self.get_next_Monday(input)
                elif input['counter'] != em_counter:
                    newState = "not_allowed"
                    if input['window'] >= 0:
                        if input['today'] == 'Monday' or input['today'] == 'Tuesday':
                            newState = "EM"
                            input['ideal_date'] = self.get_next_Monday(input)
        except:
            newState = "error_state"
        return (newState, input)

    def is_symptom_transitions(self, input):
        try:
            if self.startState == 'START_FM' or self.startState == 'START_PM' or self.startState == 'START_SM':
                newState, newInput = self.transistion('SM', input)
            if  self.startState == 'START_PER':
                newState, newInput = self.per_transistion_sm('SM', input)
        except:
            newState = "error_state"
        return (newState, newInput)

    def not_symptom_transitions(self, input):
        try:
            if self.startState == 'START_FM':
                if input['var_sym']['pulse']:
                    newState = "is_pulse"
                else:
                    newState = "not_pulse"

            if self.startState == 'START_PM':
                if input['var_sym']['truepositive']:
                    newState = "is_truepositive"
                else:
                    newState = "not_truepositive"

            if self.startState == 'START_SM':
                if input['var_sym']['spiro']:
                    newState = "is_spiro"
                else:
                    newState = "not_spiro"

            if self.startState == 'START_PER':
                newState = "not_allowed"
                if input['window'] >= 0:
                    if self.request_type == 'POST':
                        if input['today'] == 'Tuesday' or input['today'] == 'Wednesday' or input['today'] == 'Thursday' \
                                or input['today'] == 'Friday':
                            if input['counter'] != 3:
                                newState = "PER"
                                input['ideal_date'] += timedelta(1)
                            else:
                                newState = "PM"
                                input['ideal_date'] = self.get_next_Monday(input)
                    elif self.request_type == 'GET':
                        if input['today'] == 'Wednesday' and input['window'] == 0:
                            newState = "PER"
                        elif input['today'] == 'Thursday' and input['window'] in [0,1]:
                            if input['counter'] != 3:
                                newState = "PER"
                            else:
                                newState = "PM"
                        elif input['today'] == 'Friday' and input['window'] in [1,2]:
                            if input['counter'] != 3:
                                newState = "PER"
                            else:
                                newState = "PM"
                        elif input['today'] == 'Monday' or input['today'] == 'Tuesday':
                            newState = "PM"
        except:
            newState = "error_state"
        return (newState, input)


    def not_spiro_transitions(self, input):
        try:
            if self.startState == 'START_FM' or self.startState == 'START_SM':
                newState, newInput = self.transistion('FM', input)
        except:
            newState = "error_state"
        return (newState, newInput)

    def is_spiro_transitions(self, input):
        try:
            if self.startState == 'START_FM':
                newState = "not_allowed"
                if input['window'] >= 0:
                   if self.request_type == 'POST':
                        if input['today'] == 'Monday' or input['today'] == 'Tuesday':
                            newState = "PER"
                            input['ideal_date'] = self.get_closest_Tuesday(input)
                   elif self.request_type == 'GET':
                        if input['today'] == 'Tuesday' and input['window'] == 0:
                            newState = "PER"
                        elif input['today'] == 'Wednesday' and input['window'] in [0,1]:
                            newState = "PER"
                        elif input['today'] == 'Thursday' and input['window'] in [0,1,2]:
                            newState = "PER"
                        elif input['today'] == 'Friday' and input['window'] in [1,2,3]:
                            newState = "PER"
                        elif input['today'] == 'Monday' or input['today'] == 'Tuesday':
                            newState = "PM"
            if self.startState == 'START_SM':
                newState = "not_allowed"
                if input['window'] >= 0:
                    if input['today'] == 'Monday' or input['today'] == 'Tuesday':
                        newState = "SM"
                        input['ideal_date'] = self.get_next_Monday(input)
        except:
            newState = "error_state"
        return (newState, input)

    def not_o2sat_transitions(self, input):
        try:
            if self.startState == 'START_FM':
                if input['var_sym']['symptoms']:
                    newState = "is_symptom"
                else:
                    newState = "not_symptom"

            if self.startState == 'START_PER' or self.startState == 'START_PM' or self.startState == 'START_SM':
                if input['var_sym']['pulse']:
                    newState = "is_pulse"
                else:
                    newState = "not_pulse"
        except:
            newState = "error_state"
        return (newState, input)

    def not_pulse_transitions(self, input):
        try:
            if self.startState == 'START_FM':
                if input['var_sym']['spiro']:
                    newState = "is_spiro"
                else:
                    newState = "not_spiro"
            if self.startState == 'START_PER' or self.startState == 'START_PM' or self.startState == 'START_SM':
                if input['var_sym']['symptoms']:
                    newState = "is_symptom"
                else:
                    newState = "not_symptom"
        except:
            newState = "error_state"
        return (newState, input)

    def is_o2sat_transitions(self, input):
        try:
            if self.startState == 'START_FM' or self.startState == 'START_PM' or self.startState == 'START_SM':
                newState, newInput = self.transistion('SM', input)
            if self.startState == 'START_PER':
                newState, newInput = self.per_transistion_sm('SM', input)
        except:
            newState = "error_state"
        return (newState, newInput)

    def is_pulse_transitions(self, input):
        try:
            if self.startState == 'START_FM':
                if input['var_sym']['pulse_histmeds']:
                    if input['var_sym']['spiro']:
                        newState = "is_spiro"
                    else:
                        newState = "not_spiro"
                    newInput = input
                else:
                    newState, newInput = self.transistion('SM', input)

            if self.startState == 'START_PM' or self.startState == 'START_SM':
                if input['var_sym']['pulse_histmeds']:
                    if input['var_sym']['symptoms']:
                        newState = "is_symptom"
                    else:
                        newState = "not_symptom"
                    newInput = input
                else:
                    newState, newInput = self.transistion('SM', input)
            if self.startState == 'START_PER':
                if input['var_sym']['pulse_histmeds']:
                    if input['var_sym']['symptoms']:
                        newState = "is_symptom"
                    else:
                        newState = "not_symptom"
                    newInput = input
                else:
                    newState, newInput = self.per_transistion_sm('SM', input)

        except:
            newState = "error_state"
        return (newState, newInput)

    def not_truepositive_transitions(self, input):
        try:
            if self.startState == 'START_PM':
                newState, newInput = self.transistion('FM', input)
        except:
            newState = "error_state"
        return (newState, newInput)

    def is_truepositive_transitions(self, input):
        try:
            if self.startState == 'START_PM':
                newState, newInput = self.transistion('EM', input)
        except:
            newState = "error_state"
        return (newState, newInput)


    def transistion(self, state, input):
        newState = "not_allowed"
        if input['window'] >= 0:
            if input['today'] == 'Monday' or input['today'] == 'Tuesday':
                newState = state
                input['ideal_date'] = self.get_next_Monday(input)
        return (newState, input)

    def per_transistion_sm(self, state, input):
        newState = "not_allowed"
        if input['window'] >= 0:
            if self.request_type == 'POST':
                if input['today'] == 'Tuesday' or input['today'] == 'Wednesday' or input[
                    'today'] == 'Thursday' \
                        or input['today'] == 'Friday':
                    newState = state
                    input['ideal_date'] = self.get_next_Monday(input)
            elif self.request_type == 'GET':
                if input['today'] == 'Monday' or input['today'] == 'Tuesday':
                    newState = state
        return (newState, input)

    def get_next_Monday(self, input):
        dt = input['today_date']
        if dt.weekday() == 0:
            dt += timedelta(1)
        while dt.weekday() != 0:
            dt += timedelta(1)
        return dt

    def get_closest_Tuesday(self, input):
        dt = input['today_date']
        if dt.weekday() == 0:
            dt += timedelta(1)
        return dt
