import unittest
from selenium.webdriver import Chrome, ChromeOptions
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.action_chains import ActionChains
from ecp_hs_app.models import Patient, Session, Spiro, Pulseox, Survey, SpiroRaw, PulseoxRaw, Report
import pandas as pd
from datetime import timedelta
import dill
from base64 import b64decode
import os
import time
from decouple import config



class Testing(unittest.TestCase):

    def __init__(self):
        self.decision_dict = {'PreSurv': 'Continue Pre-surveillance Mode',
                         'Surv': 'Advance to Surveillance Mode', 'Retrain': 'Retraining',
                         'ConvIneligible': 'Conversion Ineligible',
                         'FPV': 'Consistent with False Positive Variance',
                         'TPV': 'Consistent with True Positive Variance'}

    def check_revert(self):
        patient = Patient.objects.get(id=self.patient.id)
        if self.fields['revert']:
            patient.presurvenddate = None
            patient.survhighfev1 = None
            patient.survlowfev1 = None
            patient.survmeanfev1 = None
            patient.currentmode = 1
            patient.historypulseormeds = self.fields['pulseoxhistmeds']
            patient.save()
            session = Session.objects.filter(patientid=patient.id).order_by('-datetime')[0]
            dt = session.datetime.date()
            session.nextsession = 'PS'
            session.nextsessiondate = dt + timedelta(1)
            session.save()
            report = Report.objects.filter(sessionid=session.id)[0]
            report.assessment = False
            report.reportreviewed = None
            report.senttosite = False
            report.save()
            self.cmi_out = cmi_simulation(self.url, report.id, self.decision_dict['PreSurv'])
        else:
            patient.historypulseormeds = self.fields['pulseoxhistmeds']
            patient.save()

    def session_complete(self):
        self.db_fields = query_database(self.session)
        self.bool_val = compare_file_db(self.fields, self.db_fields)
        if self.patient.currentmode == 1:
            if self.fields['convert']:
                self.cmi_out = cmi_simulation(self.url, self.report_id, self.decision_dict['Surv'])
        elif self.patient.currentmode == 2 or self.patient.currentmode == 3:
            if self.fields['truepositive']:
                self.cmi_out = cmi_simulation(self.url, self.report_id, self.decision_dict['TPV'])

        if self.check_report:
            try:
                Report.objects.get(id=self.report_id)
                reports = dill.loads(b64decode(self.report.encode()))
                if os.path.isfile(reports.report_data_pdf):
                    self.report_found = True
                else:
                    self.report_found = False
            except:
                self.report_found = False

def cmi_simulation(url, report_id, decision):
    options = ChromeOptions()
    options.add_argument('--window-size=1800,1200')
    options.add_argument('--headless')
    options.add_argument('--no-sandbox')
    driver = Chrome(executable_path='/data/ecp-hs/dashboard/venv/bin/chromedriver', options=options)
    driver.get('https://%s/login/' % url)
    user =  config('WB_USER', cast=str)
    key = config('WB_ID', cast=str)
    driver.find_element_by_id('id_username').send_keys(user)
    driver.find_element_by_id ('id_password').send_keys(key)
    driver.find_element_by_id('id_submit').click()
    driver.get('https://%s/highprworklist/' % url)
    num = int(report_id)
    from_txt = 'highprworklist'
    try:
        driver.find_element_by_xpath('//a[@href="/reports/%s/?from=/%s/"]' % (num, from_txt)).click()
    except:
        return False
    element_to_hover_over = driver.find_element_by_id("btn-form")
    hover = ActionChains(driver).move_to_element(element_to_hover_over)
    hover.perform()
    select = Select(driver.find_element_by_id('id_choices'))
    select.select_by_visible_text(decision)
    driver.find_element_by_id('id_comment').clear()
    driver.find_element_by_id('id_comment').send_keys("")
    driver.find_element_by_id('mysubmit').click()
    time.sleep(5)
    return True

def query_database(session):
    sessionid = session.id
    spiro = \
    Spiro.objects.filter(sessionid=sessionid).values('id', 'peakflow', 'fev17', 'peakflowtime', 'extrapolatedvol',
                                                     'spirovariance')[0]
    spiro_df = pd.DataFrame({k: [v] for k, v in spiro.items()})
    spiroid = spiro_df['id'].values[0]
    spiro_df.drop(columns='id', inplace=True)
    spiro_raw_df = pd.DataFrame(list(
        SpiroRaw.objects.filter(spiroid=spiroid).values('pef', 'fev1', 'fvc', 'fevfvcratio', 'fev6', 'fef2575')))
    try:
        pulseox = \
        Pulseox.objects.filter(sessionid=sessionid).values('id', 'minheartrate', 'maxheartrate', 'mino2sat',
                                                           'timeo2satabnormal', 'timeheartrateabnormal',
                                                           'pulseboolean', 'o2satboolean')[0]
        pulseox_df = pd.DataFrame({k: [v] for k, v in pulseox.items()})
        pulseoxid = pulseox_df['id'].values[0]
        pulseox_df.drop(columns='id', inplace=True)
        pulseox_raw_df = pd.DataFrame(list(
            PulseoxRaw.objects.filter(pulseoxid=pulseoxid).values('pulserate', 'o2sat', 'pulse_strength',
                                                                  'perfusion_index', 'pwave1', 'pwave2', 'pwave3')))
    except:
        pulseox_df = pd.DataFrame(['empty'], columns=['Data'])
        pulseox_raw_df = pd.DataFrame(['empty'], columns=['Data'])
    try:
        survey = Survey.objects.filter(sessionid=sessionid).values()[0]
        survey_df = pd.DataFrame({k: [v] for k, v in survey.items()})
        survey_df.drop(columns=['id', 'sessionid_id'], inplace=True)
        survey_df.replace(to_replace='Yes', value=1, inplace=True)
        survey_df.replace(to_replace='No', value=0, inplace=True)
        survey_df.replace(to_replace='', value=0, inplace=True)
        survey_df.replace(to_replace='[A-Z][a-z]', value=1, regex=True, inplace=True)
    except:
        survey_df = pd.DataFrame(['empty'], columns=['Data'])

    # sessions = Session.objects.raw('select session.id, sessionstate, datetime, tabletver, imei, spirovariance, o2satboolean, pulseboolean, surveysymptom '
    #                                'from session left join spiro on spiro.sessionid=session.id left join pulseox on pulseox.sessionid=session.id '
    #                                'left join survey on survey.sessionid=session.id where session.id=%s',[session.id])
    # session_df = pd.DataFrame([item.__dict__ for item in sessions])
    # session_df.drop(columns=['_state', 'id'], inplace=True)
    session_df = pd.DataFrame(
        list(Session.objects.filter(id=sessionid).values('sessionstate', 'datetime', 'tabletver', 'imei')))
    return [spiro_df, spiro_raw_df, pulseox_df, pulseox_raw_df, survey_df, session_df]

def compare_file_db(fields, db_fields):
    spiro_file_df = fields['raw_pef']
    spiro_file_df['spiro_var'] = fields['spiro_var']
    spiro_file_df = spiro_file_df.astype(float)
    spiro_db_df = db_fields[0]
    spiro_db_df.rename(columns={'fev17': 'fev7', 'spirovariance': 'spiro_var'}, inplace=True)
    spiro_db_df = spiro_db_df.astype(float)
    bool_spiro = spiro_db_df.equals(spiro_file_df)
    spiro_raw_file_df = fields['raw_blows']
    spiro_raw_file_df = spiro_raw_file_df.astype(float)
    spiro_raw_db_df = db_fields[1]
    spiro_raw_db_df.rename(columns={'fevfvcratio': 'fev1fvcratio'}, inplace=True)
    spiro_raw_db_df = spiro_raw_db_df.astype(float)
    bool_spiro_raw = spiro_raw_db_df.equals(spiro_raw_file_df)

    if fields['session_state'] != 'EM' and fields['session_state'] != 'TM':
        pulse_file = ['minHR', 'maxHR', 'lowo2sat', 'timeAbnormal', 'timeMinRate', 'pulse_var', 'o2_var']
        pulse_file = {key: fields[key] for key in pulse_file}
        pulse_file_df = pd.DataFrame({k: [v] for k, v in pulse_file.items()})
        pulse_db_df = db_fields[2]
        pulse_db_df.columns = pulse_file
        pulse_db_df = pulse_db_df.astype(float)
        pulse_file_df = pulse_file_df.astype(float)
        pulse_raw_file_df = fields['raw_pulse']
        pulse_file_raw = list(pulse_raw_file_df.columns)
        pulse_raw_file_df = pulse_raw_file_df.astype(float)
        pulse_raw_db_df = db_fields[3]
        pulse_raw_db_df.columns = pulse_file_raw
        pulse_raw_db_df = pulse_raw_db_df.astype(float)
    else:
        pulse_file_df = pd.DataFrame(['empty'], columns=['Data'])
        pulse_raw_file_df = pd.DataFrame(['empty'], columns=['Data'])
        pulse_db_df = db_fields[2]
        pulse_raw_db_df = db_fields[3]
    bool_pulse = pulse_db_df.equals(pulse_file_df)
    bool_pulse_raw = pulse_raw_db_df.equals(pulse_raw_file_df)

    if fields['survey']:
        survey = ['symptoms', 'q1', 'q2', 'q3', 'q4', 'q5', 'q11', 'q12', 'q13', 'q14', 'q15', 'q16', 'q17', 'q21',
                  'q22',
                  'q23', 'q24', 'q31', 'q32', 'q33', 'q34', 'q35', 'q41', 'q42', 'q43', 'q51', 'q52', 'q53', 'q54']
        survey_file = {key: fields[key] for key in survey}
        survey_file_df = pd.DataFrame({k: [v] for k, v in survey_file.items()})
        survey_db_df = db_fields[4]
        survey_file_df = survey_file_df.astype(int)
        survey_db_df.rename(columns={'surveysymptom': 'symptoms'}, inplace=True)
        survey_db_df = survey_db_df[survey]
        survey_db_df = survey_db_df.astype(int)
    else:
        survey_file_df = pd.DataFrame(['empty'], columns=['Data'])
        survey_db_df = db_fields[4]
    bool_survey = survey_db_df.equals(survey_file_df)

    session_file = ['session_state', 'session_date', 'tablet_version', 'imei']
    session_file = {key: fields[key] for key in session_file}
    session_file_df = pd.DataFrame({k: [v] for k, v in session_file.items()})
    session_db_df = db_fields[5]
    session_db_df['datetime'] = session_db_df['datetime'].dt.strftime('%Y%m%d_%H%M%S')

    session_db_df.columns = session_file
    bool_session = session_db_df.equals(session_file_df)

    bool_val = all([bool_spiro, bool_spiro_raw, bool_pulse, bool_pulse_raw, bool_survey, bool_session])
    return bool_val
