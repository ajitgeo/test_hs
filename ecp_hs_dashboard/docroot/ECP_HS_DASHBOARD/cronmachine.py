import os
import django
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'ECP_HS_DASHBOARD.settings')
django.setup()
from ECP_HS_DASHBOARD.database_functions import DBFunctions, DB, MODE
from ecp_hs_app.models import Patient, Session
from django.template.loader import render_to_string
from django.core.mail import EmailMessage
from decouple import config
from datetime import datetime, timedelta


def send_email(subject=None, message=None, file=None, attach=True, receivers=None):
    sender = config('EMAIL_HOST_USER', cast=str)
    headers = {'Reply-To': config('REPLY_TO', cast=str)}
    msg = EmailMessage(subject, message, sender, receivers, headers=headers)
    msg.content_subtype = "html"
    if attach:
        msg.attach_file(file)
    msg.send()

def get_next_Monday(dt):
    while dt.weekday() != 0:
        dt += timedelta(1)
    return dt

def track_missed_sessions(today_date=None):
    db = DBFunctions()
    """remove ret_state and missed_sess from final version used only for testing"""
    ret_state = []
    missed_sess = []
    for patient in Patient.objects.all().order_by('id'):
        """remove patient.id check below from final version used only for testing"""
        # if patient.id == 2 or patient.id == 3:
        # if patient.id == 2 or patient.id == 1:
            # continue
        if patient.currentmode == MODE.PRESURV.value or patient.currentmode == MODE.CONVERTEDTOSURV.value or patient.currentmode == MODE.SURV.value:
            if Session.objects.filter(patientid=patient.id).exists():
                session = Session.objects.filter(patientid=patient.id).order_by('-id')[0]
                if today_date == None:
                    today_date = datetime.now().date()
                newDate = session.nextsessiondate
                if patient.currentmode == MODE.PRESURV.value:
                    newState = 'PS'
                    missed_sessions = 0
                else:
                    window_cron = (today_date - newDate).days
                    if window_cron in [4,3,2]:
                        newState = 'IS_MISSED_PERSISTENCE'
                        missed_sessions = window_cron - 1
                    elif window_cron < 0:
                        newState = 'NOT_MISSED_SESSION'
                        missed_sessions = 0
                    elif window_cron % 2 == 0:
                        newState = 'IS_2_MISSED_SESSION'
                        missed_sessions = 1
                    else:
                        newState = 'IS_MISSED_SESSION'
                        missed_sessions = 1
                ret_state.append(newState)
                db.session_id = session.id
                db.site_id = patient.siteid.id
                db.patient_id = patient.id
                db.imei = patient.currentimei
                db.sessionmode = patient.currentmode
                db.sessionstate = session.nextsession
                db.starttestdate = patient.starttestdate
                db.datetime = datetime(today_date.year, today_date.month, today_date.day)
                db.nextsessiondate = session.nextsessiondate
                db.truepositive = session.truepositive
                db.missedpersistence = False
                db.twomissedsessions = False
                db.psmissedsessions = False
                db.missedsessions = missed_sessions
                if newState == 'IS_MISSED_PERSISTENCE':
                    db.nextsession = 'PM'
                    db.nextsessiondate = get_next_Monday(today_date)
                    db.update_session(DB.UPDATE)
                    """update missed session in database"""
                    db.missedpersistence = True
                    db.update_missedsession()
                elif newState == 'IS_2_MISSED_SESSION':
                    """update missed session in database"""
                    db.twomissedsessions = True
                    db.update_missedsession()
                elif newState == 'IS_MISSED_SESSION':
                    """update missed session in database"""
                    db.update_missedsession()
                elif newState == 'PS':
                    """update missed session in database"""
                    num = 7
                    test_date = today_date - timedelta(7)
                    if test_date < db.starttestdate:
                        num = (today_date - db.starttestdate).days
                    db.query_psmissedsession(patient.id, str(today_date), num)
                    db.missedsessions = db.psmissedsession[0]['psmissedsession']
                    if db.missedsessions != 0:
                        db.psmissedsessions = True
                        db.update_missedsession()
                missed_sess.append(db.missedsessions)
    """Send notification by email"""
    db.query_missedsession(db.datetime)
    df = db.missedsession
    new_cols = ['ID', 'SITEID', 'PATIENTID', 'IMEI', 'MODE', 'MISSED STATE', 'DATE', 'MISSED DATE', 'MISSED PERSISTENCE', 'MISSED TWO SESSIONS',
                'MISSED PS SESSIONS', 'NUMBER OF MISSED SESSIONS']
    cols_rename_dict = dict(zip(list(df), new_cols))
    df = df.rename(columns=cols_rename_dict)
    df['MISSED PERSISTENCE'] = df['MISSED PERSISTENCE'].astype('bool')
    df['MISSED TWO SESSIONS'] = df['MISSED TWO SESSIONS'].astype('bool')
    df['MISSED PS SESSIONS'] = df['MISSED PS SESSIONS'].astype('bool')
    df['SUBJECT'] = 'NA'
    df.drop(df[(df['MISSED PERSISTENCE'] == False) & (df['MISSED TWO SESSIONS'] == False) & (df['MISSED PS SESSIONS'] == False)].index, inplace=True)
    df = df.drop_duplicates(subset='PATIENTID', keep='first')
    df = df.reset_index(drop=True)
    sites = list(df['SITEID'].unique())
    if sites != []:
        for site in sites:
            for index, row in df.iterrows():
                db.imei = int(df['IMEI'].loc[index])
                db.query_patient()
                consecutive = ''
                if df['MISSED TWO SESSIONS'].loc[index]:
                    df['NUMBER OF MISSED SESSIONS'].loc[index] = int(((today_date - df['MISSED DATE'].loc[index]).days + 2)/7)
                    consecutive = 'consecutive'

                missed_number = df['NUMBER OF MISSED SESSIONS'].loc[index]

                db.query_sitepersonnel(int(site))
                receivers = [sitep[email] for sitep in db.sitepersonnel for email in sitep.keys() if
                             email == 'emailaddress']
                subject = 'SHS Missed Test Sessions'
                name = 'Dear Standardized Home Spirometry Study Team Investigators'
                email_context = {'patient': str(db.patient[0]['patientnum']), 'consecutive': consecutive,
                                 'missed_number': missed_number,
                                 'name': name}
                message = render_to_string('ecp_hs_app/email_missed_sessions.html', email_context)
                # print('Notification send on %s\n' % db.datetime.date())
                send_email(subject=subject, message=message, file=None, attach=False, receivers=receivers)

    return ret_state, missed_sess, datetime.now()

if __name__== "__main__":
    track_missed_sessions()
