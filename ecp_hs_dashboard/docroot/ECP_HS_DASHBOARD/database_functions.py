import django

django.setup()
from django.db import transaction, connection
import pandas as pd
from pandas import DataFrame
from datetime import datetime, date, timedelta
import enum
from collections import OrderedDict


class DB(enum.Enum):
    INSERT = 0
    UPDATE = 1


class MODE(enum.Enum):
    PRESURV = 1
    CONVERTEDTOSURV = 2
    SURV = 3
    DIFFERENTARM = 4
    OUTOFSTUDY = 5


class DBFunctions:
    def __init__(self):
        self.cursor = connection.cursor()

    @transaction.atomic
    def update_patient(self):
        if self.app == 'API':
            if self.patient[0]['currentmode'] == MODE.PRESURV.value:
                if self.session_count == 1:
                    self.cursor.execute("UPDATE patient SET starttestdate = %s WHERE id = %s",
                                        (self.session[0]['datetime'],
                                         self.patient[0]['id'],))
                self.cursor.execute("UPDATE patient SET presurvhighfev1 = %s, presurvlowfev1 = %s, "
                                    "presurvmeanfev1 = %s, heartmax = %s, heartmin = %s, heartmean = %s, o2satmax = %s,"
                                    "o2satmin = %s, o2satmean = %s WHERE currentimei = %s",
                                    (float(self.stat_fev1[0]['max_all_fev1']),
                                     float(self.stat_fev1[0]['min_all_fev1']),
                                     float(self.stat_fev1[0]['mean_all_fev1']),
                                     self.stat_pulse[0]['max_pulse'],
                                     self.stat_pulse[0]['min_pulse'],
                                     self.stat_pulse[0]['mean_pulse'],
                                     self.stat_pulse[0]['max_o2sat'],
                                     self.stat_pulse[0]['min_o2sat'],
                                     self.stat_pulse[0]['mean_o2sat'],
                                     self.imei,))
            if self.patient[0]['currentmode'] == MODE.CONVERTEDTOSURV.value:
                self.cursor.execute("UPDATE patient SET survstartdate = %s, currentmode = %s WHERE currentimei = %s",
                                    (self.today_dt,
                                     MODE.SURV.value,
                                     self.imei,))
            if self.patient[0]['currentmode'] == MODE.SURV.value:
                self.cursor.execute("UPDATE patient SET survmeanfev1 = %s WHERE currentimei = %s",
                                    (float(self.stat_fev1[0]['mean_all_fev1']),
                                     self.imei,))
        if self.app == 'DASHBOARD':
            self.cursor.execute(
                "update patient set currentmode = %s from report where patient.id=report.patientid and report.id=%s",
                (self.mode,
                 self.report_id,))
            if self.decision == 'Conversion to Surveillance' or self.decision == 'Consistent with False Positive Variance':
                self.cursor.execute(
                    "update patient set presurvenddate = %s, survhighfev1 = %s, survlowfev1 = %s, survmeanfev1 = %s"
                    " from report where patient.id=report.patientid and report.id=%s",
                    (self.presurvenddate,
                     self.survfev1high,
                     self.survfev1low,
                     self.survfev1mean,
                     self.report_id,))
            if self.decision == 'Ineligible for Home Spirometry Cohort':
                self.cursor.execute(
                    "update patient set presurvenddate = %s"
                    " from report where patient.id=report.patientid and report.id=%s",
                    (self.presurvenddate,
                     self.report_id,))

    @transaction.atomic
    def update_session(self, func):
        if func == DB.INSERT:
            self.cursor.execute(
                "INSERT INTO session (patientid, imei, datetime, sessionnum, sessionmode, sessionstate, smweekcount, emweekcount, tabletver) VALUES (%s, %s, %s,"
                "%s, %s, %s, %s, %s, %s)", (self.patient[0]['id'],
                                    self.imei,
                                    self.tmpstmp,
                                    self.session_count,
                                    self.patient[0]['currentmode'],
                                    self.fields['session_state'],
                                    self.fields['smweekcount'],
                                    self.fields['emweekcount'],
                                    self.fields['tablet_version']))
        if func == DB.UPDATE:
            self.cursor.execute(
                "update session set nextsession = %s, nextsessiondate = %s, truepositive = %s where session.id=%s",
                (self.nextsession,
                 self.nextsessiondate,
                 self.truepositive,
                 self.session_id))


    @transaction.atomic
    def update_spiro(self, func):
        if func == DB.INSERT:
            self.cursor.execute(
                "INSERT INTO spiro (sessionid, fev11, fev12, fev13, fev14, fev15, fev16, fev17, peakflow,"
                "peakflowtime, extrapolatedvol, maxfev1, spirovariance, persistencecount) VALUES (%s, %s, %s, %s, %s, %s,"
                "%s, %s, %s, %s, %s, %s, %s, %s)", (self.session[0]['id'],
                                                float(self.fields['fev1']),
                                                float(self.fields['fev2']),
                                                float(self.fields['fev3']),
                                                float(self.fields['fev4']),
                                                float(self.fields['fev5']),
                                                float(self.fields['fev6']),
                                                float(self.fields['raw_pef']['fev7']),
                                                float(self.fields['raw_pef']['peakflow']),
                                                float(self.fields['raw_pef']['peakflowtime']),
                                                float(self.fields['raw_pef']['extrapolatedvol']),
                                                self.maxfev,
                                                bool(int(self.fields['spiro_var'])),
                                                self.fields['persistence_count']
                                                ))
        if func == DB.UPDATE:
            self.max_all_fev1 = float(self.stat_fev1[0]['max_all_fev1'])
            self.percentmax_fev1 = (self.maxfev - self.max_all_fev1) / self.max_all_fev1
            self.percentmax_fev1 = self.percentmax_fev1 * 100
            self.mean_all_fev1 = float(self.stat_fev1[0]['mean_all_fev1'])
            self.percentmean_fev1 = (self.maxfev - self.mean_all_fev1) / self.mean_all_fev1
            self.percentmean_fev1 = self.percentmean_fev1 * 100
            self.maxfev1k = self.maxfev * 1000
            self.cursor.execute("UPDATE spiro SET maxpresurv = %s, meanpresurv = %s, percentmaxpresurv = %s, "
                                "percentmeanpresurv = %s, fev1k = %s "
                                "WHERE id = %s",
                                (self.max_all_fev1,
                                 self.mean_all_fev1,
                                 self.percentmax_fev1,
                                 self.percentmean_fev1,
                                 self.maxfev1k,
                                 self.spiro[0]['id'],))

    @transaction.atomic
    def update_pulseox(self, func):
        if func == DB.INSERT:
            self.cursor.execute("INSERT INTO pulseox (sessionid, meanheartrate, minheartrate, maxheartrate, "
                                "timeheartrateabnormal, timeo2satabnormal, pulseboolean, o2satboolean, maxo2sat, mino2sat, "
                                "meano2sat) VALUES (%s, %s, %s, %s, %s, "
                                "%s, %s, %s, %s, %s, %s)", (self.session[0]['id'],
                                                            float(self.fields['raw_pulse']['pulserate'].mean()),
                                                            float(self.fields['minHR']),
                                                            float(self.fields['maxHR']),
                                                            float(self.fields['timeMinRate']),
                                                            float(self.fields['timeAbnormal']),
                                                            bool(int(self.fields['pulse_var'])),
                                                            bool(int(self.fields['o2_var'])),
                                                            float(self.fields['raw_pulse']['oxygensaturation'].max()),
                                                            float(self.fields['raw_pulse']['oxygensaturation'].min()),
                                                            float(self.fields['raw_pulse']['oxygensaturation'].mean()),
                                                            ))

    @transaction.atomic
    def update_spiro_raw(self):
        for index, field in self.fields['raw_blows'].iterrows():
            self.cursor.execute(
                "INSERT INTO spiro_raw (spiroid, pef, fev1, fvc, fevfvcratio, fev6, fef2575) "
                "VALUES (%s, %s, %s, %s, %s, %s, %s)", (self.spiro[0]['id'],
                                                        field['pef'], field['fev1'],
                                                        field['fvc'],
                                                        field['fev1fvcratio'],
                                                        field['fev6'],
                                                        field['fef2575']))

    @transaction.atomic
    def update_pulseox_raw(self):
        for index, field in self.fields['raw_pulse'].iterrows():
            self.cursor.execute("INSERT INTO pulseox_raw (pulseoxid, pulserate, o2sat, pulse_strength, perfusion_index,"
                                "pwave1, pwave2, pwave3) VALUES (%s, %s, %s, %s, %s, "
                                "%s, %s, %s)", (self.pulse[0]['id'],
                                                field['pulserate'],
                                                field['oxygensaturation'],
                                                field['pulsestrength'],
                                                field['perfusionindex'],
                                                field['pulsewave1'],
                                                field['pulsewave2'],
                                                field['pulsewave3']))

    @transaction.atomic
    def update_survey(self):
        if self.fields['survey']:
            self.surveysymptom = bool(int(self.fields['symptoms']))
            if self.surveysymptom:
                self.survey = {}
                for f in range(1, 6):
                    key = "q{}".format(f)
                    if key in self.fields:
                        # if self.fields[key] == '0':
                        if self.fields[key] == 0:
                            self.survey[key] = 'No'
                        # elif self.fields[key] == '1':
                        elif self.fields[key] == 1:

                            self.survey[key] = 'Yes'
                count = 0
                for f in range(10, 60):
                    key = "q{}".format(f)
                    if key in self.fields:
                        count = count + 1
                        if count in [8,13,19,23,28]:
                            count += 1
                        self.cursor.execute("SELECT * FROM survey_answers WHERE id = %s", (count,))
                        survey_resp = self.dictfetchall()
                        # if self.fields[key] == '1':
                        if self.fields[key] == 1:
                            self.survey[key] = survey_resp[0]['answer']
                        # elif self.fields[key] == '0':
                        elif self.fields[key] == 0:
                            self.survey[key] = ''
                self.cursor.execute("INSERT INTO survey (sessionid, surveysymptom, q1, q2, q3, q4, q5, q11, q12, q13, q14, q15,"
                                    " q16, q17, q21, q22, q23, q24, q31, q32, q33, q34, q35, q41, q42, q43, q51, q52, q53, q54) "
                                    "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s,"
                                    " %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", (self.session[0]['id'],
                                                                 self.surveysymptom,
                                                                 self.survey['q1'],
                                                                 self.survey['q2'],
                                                                 self.survey['q3'],
                                                                 self.survey['q4'],
                                                                 self.survey['q5'],
                                                                 self.survey['q11'],
                                                                 self.survey['q12'],
                                                                 self.survey['q13'],
                                                                 self.survey['q14'],
                                                                 self.survey['q15'],
                                                                 self.survey['q16'],
                                                                 self.survey['q17'],
                                                                 self.survey['q21'],
                                                                 self.survey['q22'],
                                                                 self.survey['q23'],
                                                                 self.survey['q24'],
                                                                 self.survey['q31'],
                                                                 self.survey['q32'],
                                                                 self.survey['q33'],
                                                                 self.survey['q34'],
                                                                 self.survey['q35'],
                                                                 self.survey['q41'],
                                                                 self.survey['q42'],
                                                                 self.survey['q43'],
                                                                 self.survey['q51'],
                                                                 self.survey['q52'],
                                                                 self.survey['q53'],
                                                                 self.survey['q54']))
            else:
                self.survey = {}
                for f in range(1, 6):
                    key = "q{}".format(f)
                    self.survey[key] = 'No'
                self.cursor.execute("INSERT INTO survey (sessionid, surveysymptom, q1, q2, q3, q4, q5) "
                                    "VALUES (%s, %s, %s, %s, %s, %s, %s)", (self.session[0]['id'],
                                                        self.surveysymptom,
                                                        self.survey['q1'],
                                                        self.survey['q2'],
                                                        self.survey['q3'],
                                                        self.survey['q4'],
                                                        self.survey['q5']
                                                        ))

    @transaction.atomic
    def update_report(self, func):
        if func == DB.INSERT:
            self.cursor.execute(
                "INSERT INTO report (assessment, patientid, sessionid, reportdate, reporttype, senttosite, filepath, "
                "reportname, cmipriority, sitepriority, "
                "variancetype, symptoms) VALUES (%s, %s, %s, %s, %s, %s, %s,"
                "%s, %s, %s, %s, %s)", (bool(self.assessment),
                        self.patient[0]['id'],
                        self.session[0]['id'],
                        datetime.now(),
                        self.rep_type,
                        bool(self.senttosite),
                        self.csv_filename,
                        self.rep_name,
                        bool(self.cmipriority),
                        bool(self.sitepriority),
                        self.variancetype,
                        self.amisymptoms
                        ))
        if func == DB.UPDATE:
            self.cursor.execute(
                "update report set assessment = %s, senttosite = %s, decision = %s, "
                "reportreviewed = %s, "
                "comments = %s where report.id=%s",
                (self.assessment,
                 self.senttosite,
                 self.decision,
                 datetime.now(),
                 self.comment,
                 self.report_id,
                 ))

    @transaction.atomic
    def update_report_details(self):
        self.cursor.execute(
            "INSERT INTO report_details (reportid, surveyid, fev1var, fev1varpersist, presurvhighfev1, "
            "presurvlowfev1, survhighfev1, survlowfev1, lowestfev1, o2satmean_nl, o2satmin_nl, o2satmax_nl, "
            "o2satmean_abnl, o2satmin_abnl, o2satmax_abnl, o2satduration_abnl, "
            "heartmean_nl, heartmin_nl, heartmax_nl, heartmean_abnl, heartmin_abnl, heartmax_abnl, heartduration_abnl) "
            "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
            (self.report_id,
             self.rep_val['surveyid'],
             self.rep_val['fev1var'],
             self.rep_val['fev1varpersist'],
             self.rep_val['psmaxfev1'],
             self.rep_val['psminfev1'],
             self.rep_val['smaxfev1'],
             self.rep_val['sminfev1'],
             self.rep_val['lowestfev1'],
             self.rep_val['mean_o2sat_nl'],
             self.rep_val['min_o2sat_nl'],
             self.rep_val['max_o2sat_nl'],
             self.rep_val['mean_o2sat_abnl'],
             self.rep_val['min_o2sat_abnl'],
             self.rep_val['max_o2sat_abnl'],
             self.rep_val['o2satduration_abnl'],
             self.rep_val['mean_pulse_nl'],
             self.rep_val['min_pulse_nl'],
             self.rep_val['max_pulse_nl'],
             self.rep_val['mean_pulse_abnl'],
             self.rep_val['min_pulse_abnl'],
             self.rep_val['max_pulse_abnl'],
             self.rep_val['heartduration_abnl']))

    @transaction.atomic
    def update_report_stats(self):
        self.cursor.execute(
            "INSERT INTO report_stats (reportid, summarytype, mean,"
            "min, max, slope, sessions, pvalue, sd, cv, rvalue, rsquare) "
            "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
            (self.report_id,
             self.summarytype,
             float(self.rep_summary['Mean']),
             float(self.rep_summary['Min']),
             float(self.rep_summary['Max']),
             float(self.rep_summary['Slope (mL/Week)']),
             float(self.rep_summary['Sessions']),
             float(self.rep_summary['P-value']),
             float(self.rep_summary['SD 2']),
             float(self.rep_summary['CV (2SD)']),
             float(self.rep_summary['R']),
             float(self.rep_summary['R-square'])
             ))

    @transaction.atomic
    def update_missedsession(self):
        self.cursor.execute(
            "INSERT INTO missedsession (siteid, patientid, imei,"
            "sessionmode, sessionstate, datetime, nextsessiondate, missedpersistence, twomissedsessions, psmissedsessions, missedsessions) "
            "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
            (self.site_id,
             self.patient_id,
             self.imei,
             self.sessionmode,
             self.sessionstate,
             self.datetime,
             self.nextsessiondate,
             self.missedpersistence,
             self.twomissedsessions,
             self.psmissedsessions,
             self.missedsessions
             ))

    def query_patient(self):
        self.cursor.execute("SELECT * FROM patient WHERE currentimei = %s", (self.imei,))
        self.patient = self.dictfetchall()

    def query_session(self):
        self.cursor.execute("SELECT * FROM session WHERE patientid = %s AND sessionmode = %s AND datetime = %s",
                            (self.patient[0]['id'], self.patient[0]['currentmode'], self.tmpstmp))
        self.session = self.dictfetchall()

    def query_spiro(self):
        self.cursor.execute("SELECT * FROM spiro WHERE sessionid = %s", (self.session[0]['id'],))
        self.spiro = self.dictfetchall()

    def query_pulseox(self):
        self.cursor.execute("SELECT * FROM pulseox WHERE sessionid = %s", (self.session[0]['id'],))
        self.pulse = self.dictfetchall()

    def query_survey(self):
        self.cursor.execute("SELECT * FROM survey WHERE sessionid = %s", (self.session[0]['id'],))
        self.survey_table = self.dictfetchall()

    def query_report(self):
        self.cursor.execute("SELECT * FROM report WHERE sessionid = %s", (self.session[0]['id'],))
        self.report = self.dictfetchall()

    def get_report_count(self):
        self.cursor.execute("SELECT count(*) as rep_count FROM report inner join session on session.id=report.sessionid and session.sessionmode = %s WHERE report.patientid = %s", (self.patient[0]['currentmode'], self.patient[0]['id'],))
        self.report_cnt = self.dictfetchall()

    def query_report_details(self):
        self.cursor.execute("SELECT * FROM report_details WHERE reportid = %s", (self.report_id,))
        self.report_details = self.dictfetchall()

    def query_email_info(self, slidenum):
        self.cursor.execute("SELECT * FROM email_info WHERE slidenum = %s", (slidenum,))
        self.email_text = self.dictfetchall()

    def query_survey_answers(self):
        self.cursor.execute("SELECT * FROM survey_answers")
        self.survey_ans = self.dictfetchall()

    def query_sitepersonnel(self, siteid):
        self.cursor.execute("SELECT * FROM sitepersonnel WHERE siteid = %s", (siteid,))
        self.sitepersonnel = self.dictfetchall()

    def query_missedsession(self, dt):
        self.cursor.execute("SELECT * FROM missedsession WHERE DATE(datetime) = %s::date order by siteid", (dt,))
        self.missedsession = DataFrame(self.dictfetchall(), columns=[i[0] for i in self.cursor.description])

    def query_psmissedsession(self, pat_id, dt, num):
        self.cursor.execute("SELECT  %s-count(*) as psmissedsession FROM session WHERE DATE(datetime) "
                            "between %s::date - INTERVAL '%s DAYS' and %s::date - INTERVAL '1 DAYS' and patientid = %s", (num, dt, num, dt, pat_id,))
        self.psmissedsession = self.dictfetchall()


    def get_data(self):
        self.fev1 = float(self.fields['fev1'])
        self.fev2 = float(self.fields['fev2'])
        self.fev3 = float(self.fields['fev3'])
        self.fev4 = float(self.fields['fev4'])
        self.fev5 = float(self.fields['fev5'])
        self.fev6 = float(self.fields['fev6'])
        self.maxfev = max(self.fev1, self.fev2, self.fev3, self.fev4, self.fev5, self.fev6)
        self.minfev = min(self.fev1, self.fev2, self.fev3, self.fev4, self.fev5, self.fev6)
        if self.fields['session_state'] != 'EM' and self.fields['session_state'] != 'TM':
            self.minHR = float(self.fields['minHR'])
            self.maxHR = float(self.fields['maxHR'])
            self.lowo2sat = float(self.fields['lowo2sat'])
            self.pulse_boolean = self.fields['pulse_var']
            self.o2sat_boolean = self.fields['o2_var']

    def get_stats(self):
        if self.fields['session_state'] != 'EM' and self.fields['session_state'] != 'TM':
            self.cursor.execute("SELECT max(pulseox.maxheartrate) as max_pulse, min(pulseox.maxheartrate) as min_pulse, "
                            "avg(pulseox.maxheartrate) as mean_pulse, max(pulseox.maxo2sat) as max_o2sat, "
                            "min(pulseox.maxo2sat) as min_o2sat, avg(pulseox.maxo2sat) as mean_o2sat from "
                            "pulseox inner join "
                            "session on session.id=pulseox.sessionid inner join patient "
                            "on patient.id = session.patientid and patient.currentmode = session.sessionmode where "
                            "patient.currentimei = %s", (self.imei,))
            self.stat_pulse = self.dictfetchall()

        self.cursor.execute(
            "SELECT max(spiro.maxfev1) as max_all_fev1, min(spiro.maxfev1) as min_all_fev1, "
            "avg(spiro.maxfev1) as mean_all_fev1 from spiro "
            "inner join session on session.id=spiro.sessionid inner join patient on patient.id = session.patientid "
            "and patient.currentmode = session.sessionmode where patient.currentimei= %s", (self.imei,))
        self.stat_fev1 = self.dictfetchall()

    def get_report_data(self):
        self.cursor.execute("SELECT * FROM session LEFT JOIN spiro on session.id = spiro.sessionid "
                            "LEFT JOIN patient on patient.id = session.patientid "
                            "WHERE session.patientid=%s ORDER BY session.datetime", (self.patient[0]['id'],))
        self.spiro_values = DataFrame(self.dictfetchall(), columns=[i[0] for i in self.cursor.description])
        self.spiro_values = self.spiro_values[self.spiro_val_cols]

        self.cursor.execute("SELECT * FROM session LEFT JOIN pulseox on session.id = pulseox.sessionid "
                            "LEFT JOIN patient on patient.id = session.patientid "
                            "WHERE session.patientid=%s ORDER BY session.datetime", (self.patient[0]['id'],))
        self.pulse_values = DataFrame(self.dictfetchall(), columns=[i[0] for i in self.cursor.description])
        self.pulse_values = self.pulse_values[['maxheartrate', 'maxo2sat']]

    def parse_truthtable(self):
        df = pd.DataFrame([[bool(x) for x in [int(self.fields['o2_var']), int(self.fields['pulse_var']),
                                              int(self.fields['spiro_var']),
                                              int(self.fields['symptoms']),
                                              int(self.pulsehistmeds),
                                              int(self.truepositive)]]],
                          columns=['o2sat', 'pulse', 'spiro', 'symptoms','pulsehistmeds','truepositive'], dtype=object)
        self.cursor.execute("SELECT * FROM truth_table where sessionstate = %s", (self.fields['session_state'],))
        tt_df = DataFrame(self.dictfetchall(), columns=[i[0] for i in self.cursor.description], dtype=object)
        tt_df.drop(tt_df.columns[[0, 1]], axis=1, inplace=True)
        self.tt_data = pd.merge(tt_df, df, on=list(tt_df.columns[0:6]), how='inner')
        self.tt_data = self.tt_data.to_dict(into=OrderedDict)

    def dictfetchall(self):
        """Return all rows from a cursor as a dict"""
        columns = [col[0] for col in self.cursor.description]
        return [
            dict(zip(columns, row))
            for row in self.cursor.fetchall()
        ]
