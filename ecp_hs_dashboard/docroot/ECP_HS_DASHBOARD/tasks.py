import dill
from base64 import b64decode
from django.template.loader import render_to_string
from ECP_HS_DASHBOARD.database_functions import DBFunctions
from ECP_HS_DASHBOARD.report_functions import ReportFunctions
from ECP_HS_DASHBOARD.conversion_report import ConversionReport
from ECP_HS_DASHBOARD.variance_report import VarianceReport
import os
import pandas as pd
import shutil
from django.conf import settings
from decouple import config, Csv


def mytask(myreport=None, rep=None, csv=None, rep_type=None, email_rep=None, site_id=None, patientnum=None, slidenum=None):
    try:
        db = DBFunctions()
        reports = dill.loads(b64decode(myreport.encode()))
        repo = dill.loads(b64decode(rep.encode()))
        repo.generateReport()
        repo.raw_table()
        repo.excel = True
        if rep_type == 'Conversion':
            repo.summary_table()
        elif rep_type == 'Variance':
            repo.summary_table(reports)
        repo.generatecsv()
        reports.report_parser(file=csv, report_type=rep_type)
        context = {**reports.context, **reports.img_path}        
        if email_rep:
            db.query_sitepersonnel(site_id)
            db.query_email_info(slidenum)
            receivers = [sitep[email] for sitep in db.sitepersonnel for email in sitep.keys() if
                         email == 'emailaddress']
            receivers = receivers[0].split()
            subject = db.email_text[0]['subject']
            name = 'Dear Standardized Home Spirometry Study Team Investigators'
            email_context = {'patient': patientnum,
                             'message_header': db.email_text[0]['messagebody'],
                             'name': name}
            message = render_to_string('ecp_hs_app/email.html', email_context)
            reports.generate_pdf(context=context, file=reports.report_data_pdf, email=True,
                        message=message, subject=subject, attach=True, receivers=receivers)
        else:
            reports.generate_pdf(context=context, file=reports.report_data_pdf)
    except:
        raise


def modify_report(report_file=None, report_data_pdf=None, rep_type=None, imei=None, email=None, comment=None,
                  slidenum=None, text=None):
    db = DBFunctions()
    db.app = 'DASHBOARD'
    db.imei = int(imei)
    rep = ReportFunctions(imei=int(imei), uri=None)
    db.query_patient()
    template_file = os.path.join(rep.path,
                                 os.path.splitext(os.path.basename(report_file))[0] + '.xlsx')
    rep.temp_file = template_file
    rep.filename = template_file
    rep.rep_val = []
    rep.get_summary(report_file)
    rep.get_raw()
    rep.spiro_values = rep.raw_table
    if not comment:
        comment = 'None'
    if rep_type == 'Conversion':
        conrep = ConversionReport(rep)
        conrep.ws['C12'] = text
        conrep.ws['C13'] = comment
        conrep.generateReport()
        conrep.generatecsv()
    elif rep_type == 'Variance':
        varrep = VarianceReport(rep)
        varrep.ws['C12'] = text
        varrep.ws['C13'] = comment
        varrep.generateReport()
        varrep.generatecsv()
        db.query_survey_answers()
        rep.sur_ans = pd.DataFrame(db.survey_ans)
        rep.sur_ans.rename(columns={'id': 'Number', 'questionid': 'Survey Question Number','answer': 'Symptom'}, inplace=True)

    rep.report_parser(file=report_file, report_type=rep_type)
    context = {**rep.context, **rep.img_path}

    if email:
        db.query_email_info(slidenum)
        subject = db.email_text[0]['subject']
        name = 'Dear Standardized Home Spirometry Study Team Investigators'
        email_context = {'patient': db.patient[0]['patientnum'],
                         'message_header': db.email_text[0]['messagebody'],
                         'name': name}
        message = render_to_string('ecp_hs_app/email.html', email_context)
        db.query_sitepersonnel(db.patient[0]['siteid'])
        receivers = [sitep[email] for sitep in db.sitepersonnel for email in sitep.keys() if
                     email == 'emailaddress']
        receivers = receivers[0].split()
        rep.generate_pdf(context=context, file=report_data_pdf, email=True, message=message,
                         subject=subject,
                         attach=True, receivers=receivers)
    else:
        rep.generate_pdf(context=context, file=report_data_pdf, email=False)


def email_tech_support(patientnum='(could not retrieve patient number)', error_code=None, saved_file=None, file=None, attach=False):
    db = DBFunctions()
    rep = ReportFunctions()
    if saved_file != None:
        fail_path = os.path.join(settings.MEDIA_ROOT, 'temp_files')
        if not os.path.isdir(fail_path):
            os.makedirs(fail_path)
        file = os.path.basename(saved_file)
        shutil.move(saved_file, os.path.join(fail_path,file))
    db.query_email_info(99)
    receivers = config('RECEIVERS', cast=Csv())
    subject = db.email_text[0]['subject']
    name = 'Dear Standardized Home Spirometry Study Tech Support'
    email_context = {'patient': patientnum, 'error_code': error_code,
                     'message_header': db.email_text[0]['messagebody'],
                     'name': name}
    message = render_to_string('ecp_hs_app/email_tech.html', email_context)
    rep.send_email(subject=subject, message=message, file=file, attach=attach, receivers=receivers)
