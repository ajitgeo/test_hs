import django

django.setup()
import os
from datetime import datetime, date, timedelta
from django.core.mail import EmailMessage
import pandas as pd
import numpy as np
from bokeh.plotting import figure
from bokeh.embed import components
from bokeh.models import LinearAxis, DataRange1d, ColumnDataSource, Circle, Title, HoverTool
from bokeh.models.glyphs import Line
from sklearn.linear_model import LinearRegression
from decouple import config
from django.template.loader import get_template
from bokeh.io import export_png
from weasyprint import HTML, CSS
from bokeh.document import Document
from bokeh.io.doc import curdoc
import json
from scipy import stats
from django.conf import settings
from .conversion_report import ConversionReport
from .variance_report import VarianceReport
from itertools import islice, takewhile
from selenium.webdriver import Chrome, ChromeOptions
import re
import time


class ReportFunctions:
    def __init__(self, imei=None, uri=None):
        # if imei == 177628 or imei == 176927 or imei == 176893:
        self.folder = 'test'
        # else:
        #self.folder = 'healthy_volunteers'
        self.temp_path = os.path.join(settings.MEDIA_ROOT)
        self.path = os.path.join(settings.MEDIA_ROOT, self.folder, '%s' % imei, 'Report')
        if not os.path.isdir(self.path):
            os.makedirs(self.path)
        self.img_path = {'path_1': os.path.join(settings.MEDIA_URL, self.folder, str(imei), 'Report', 'plot_1.png'),
                         'path_2': os.path.join(settings.MEDIA_URL, self.folder, str(imei), 'Report', 'plot_2.png')}
        self.uri = uri
        self.temp_file = os.path.join(self.temp_path, 'Report.xlsx')

    def create_update_presurv_report_data(self, db=None):
        self.get_report_data(db)
        db.get_report_count()
        # num = int(db.report_cnt[0]['rep_count']) + 1
        num = int(db.session[0]['sessionnum'])
        self.filename = os.path.join(self.path, str(db.imei) +
                                     "_Pre-Surv_Session_" + str(num) + "_Report" + ".xlsx")
        rep = ConversionReport(self)
        rep.header_table()
        self.get_presurv_data(db=db, rep=rep)
        db.rep_name = 'Pre-Surv Session ' + str(num)
        db.rep_type = 'Conversion'
        db.csv_filename = os.path.join(self.path, str(db.imei) +
                                       "_Pre-Surv_Session_" + str(num) + "_Report" + ".csv")
        self.report_data_pdf = os.path.join(self.path,
                                            os.path.splitext(os.path.basename(db.csv_filename))[0] + '.pdf')
        self.rep = rep

        db.rep_val = self.rep_val
        db.spiro_values = self.spiro_values
        db.assessment = False
        db.senttosite = False
        db.cmipriority = True
        db.sitepriority = False
        db.variancetype = ''
        db.amisymptoms = ''

    def create_update_variance_report_data(self, db=None):
        self.get_report_data(db)
        self.spiro_values = self.spiro_values.sort_values(by=['Test Date'], ascending=False).reset_index(drop=True)
        db.get_report_count()
        num = int(db.report_cnt[0]['rep_count']) + 1
        self.filename = os.path.join(self.path, str(db.imei) +
                                     "_Variance_" + str(num) + "_Report" + ".xlsx")
        rep = VarianceReport(self)
        rep.header_table()
        title = db.tt_data['reportheaders'][0]
        self.rep_val['sminfev1'] = db.patient[0]['survlowfev1']
        self.rep_val['smaxfev1'] = db.patient[0]['survhighfev1']
        rep.ws['B1'] = title
        rep.ws['B4'] = 'Surveillance Week'
        rep.ws['D7'] = db.patient[0]['survlowfev1']
        rep.ws['E7'] = db.patient[0]['survhighfev1']
        rep.ws['E9'] = 'NA'
        rep.ws['E10'] = 'NA'
        rep.ws['I3'] = 'Negative'
        self.rep_val['fev1varpersist'] = False
        self.variance = [var for var in list(db.tt_data)[0:3] if db.tt_data[var][0]]
        # if bool(int(db.pulsehistmeds)):
        #     if 'pulse' in self.variance:
        #         self.variance.remove('pulse')
        if 'spiro' in self.variance:
            rep.ws['E9'] = 'Yes'
            self.rep_val['fev1var'] = True
        if db.fields['session_state'] == 'PM':
            if db.tt_data['cmireppriority'][0] == 'High':
                rep.ws['E9'] = 'Yes'
                self.rep_val['fev1var'] = True
                if 'spiro' in self.variance:
                    rep.ws['E10'] = 'Yes'
                    self.rep_val['fev1varpersist'] = True
                else:
                    rep.ws['E10'] = 'No'
                    self.rep_val['fev1varpersist'] = False
        db.amisymptoms = 'None'
        db.variancetype = 'None'
        db.assessment = False
        db.senttosite = False
        if self.variance:
            variance = [var.capitalize() for var in self.variance]
            db.variancetype = ", ".join(variance)
        # if db.surveysymptom:
        if db.survey_bool:
            sym_num = [(1,1),(2,2),(3,3),(4,4),(5,5),(6,6),(7,7),(8,9),(9,10),(10,11),(11,12),(12,14),(13,15),(14,16),(15,17),(16,18),(17,20),
                       (18,21),(19,22),(20,24),(21,25),(22,26),(23,27)]
            not_specified = [(1,8),(2,13),(3,19),(4,23),(5,28)]
            # syms = ", ".join([str(idx+1) for idx, var in enumerate(list(db.survey)[5:]) if db.survey[var] != ''])
            syms = [(idx + 1) for idx, var in enumerate(list(db.survey)[5:]) if db.survey[var] != '']
            amisymptoms = [sym for idx, sym in sym_num if idx in syms]
            pos_q_n = [int(var[1]) for idx, var in enumerate(list(db.survey)[:5]) if db.survey[var] == 'Yes']
            found_q_n  = [int(var[1]) for idx, var in enumerate(list(db.survey)[5:]) if db.survey[var] != '']
            missing = [value for value in pos_q_n if value not in found_q_n]
            if missing != []:
                not_specified_sym = [sym for idx, sym in not_specified if idx in missing]
                amisymptoms += not_specified_sym
                amisymptoms =  sorted(amisymptoms)
            amisymptoms = ", ".join([str(num) for num in amisymptoms])
            # missing_bool = []
            # for txt in pos_q:
            #     missing_bool.append(not all([re.search('^%s\d$' % txt, var) == None for var in found_q]))
            # if not all(missing_bool):
            #     if amisymptoms == '':
            #         amisymptoms = '24'
            #     else:
            #         amisymptoms += ', 24'
        else:
            amisymptoms = 'None'

        if db.tt_data['amireport'][0]:
            rep.ws['J3'] = amisymptoms
            db.amisymptoms = amisymptoms

        if db.tt_data['alertreport'][0]:
            o2_min = str(db.fields['lowo2sat'])
            o2_max = str(db.fields['raw_pulse']['oxygensaturation'].max().round().astype(int))
            o2_mean = str(db.fields['raw_pulse']['oxygensaturation'].mean().round().astype(int))
            o2_abnl_dur = db.fields['timeAbnormal']
            if db.fields['minHR'] != '0':
                pulse_min = str(db.fields['minHR'])
            else:
                pulse_min = str(db.fields['raw_pulse']['pulserate'].min().round().astype(int))
            if db.fields['maxHR'] != '0':
                pulse_max = str(db.fields['maxHR'])
            else:
                pulse_max = str(db.fields['raw_pulse']['pulserate'].max().round().astype(int))
            pulse_mean = str(db.fields['raw_pulse']['pulserate'].mean().round().astype(int))
            pulse_abnl_dur = db.fields['timeMinRate']
            if 'o2sat' in self.variance:
                rep.ws['J7'] = o2_mean
                self.rep_val['mean_o2sat_abnl'] = int(o2_mean)
                rep.ws['J8'] = o2_min + '-' + o2_max
                self.rep_val['min_o2sat_abnl'] = int(o2_min)
                self.rep_val['max_o2sat_abnl'] = int(o2_max)
                rep.ws['J9'] = o2_abnl_dur
                self.rep_val['o2satduration_abnl'] = int(o2_abnl_dur)
            if 'pulse' in self.variance:
                rep.ws['J10'] = pulse_mean + ' (' + pulse_min + '-' + pulse_max + ')'
                self.rep_val['mean_pulse_abnl'] = int(pulse_mean)
                self.rep_val['min_pulse_abnl'] = int(pulse_min)
                self.rep_val['max_pulse_abnl'] = int(pulse_max)
                rep.ws['J11'] = pulse_abnl_dur
                self.rep_val['heartduration_abnl'] = int(pulse_abnl_dur)
        self.get_variance_data(db=db, rep=rep)
        self.rep_val['lowestfev1'] = rep.raw_df.FEV1MAX[rep.raw_df['Month'] == 'Current_Month'].min()
        rep.ws['E11'] = self.rep_val['lowestfev1']
        rep.ws['E4'] = self.surv_week_num + 1
        self.rep = rep

        db.rep_name = 'Variance ' + str(num)
        db.rep_type = 'Variance'
        db.csv_filename = os.path.join(self.path, str(db.imei) +
                                       "_Variance_" + str(num) + "_Report" + ".csv")

        self.report_data_pdf = os.path.join(self.path,
                                            os.path.splitext(os.path.basename(db.csv_filename))[0] + '.pdf')
        db.query_survey_answers()
        self.sur_ans = pd.DataFrame(db.survey_ans)
        self.sur_ans.rename(columns={'id': 'Number', 'questionid': 'Survey Question Number', 'answer': 'Symptom'}, inplace=True)
        db.rep_val = self.rep_val
        db.spiro_values = self.spiro_values
        db.cmipriority = [True if db.tt_data['cmireppriority'][0] == 'High' else False][0]
        db.sitepriority = [True if db.tt_data['sitereppriority'][0] == 'High' else False][0]


    def get_presurv_data(self, db=None, rep=None):
        plot_data_1 = self.spiro_values
        if db.session_count == 30:
            plot_data_2 = plot_data_1
        else:
            plot_data_2 = self.spiro_values.tail(28)
        self.get_plot_data(db, plot_data_1, plot_data_2, rep, 'Conversion')


    def get_variance_data(self, db=None, rep=None):
        low = float(db.patient[0]['survlowfev1'])
        high = float(db.patient[0]['survhighfev1'])

        self.pres_data = self.spiro_values.loc[self.spiro_values['Mode'] == 1].head(30)
        self.pres_five_week_data = self.pres_data.iloc[::7, :]
        self.surv_data = self.spiro_values.loc[self.spiro_values['Mode'] == 3]
        self.surv_week_num = self.surv_data['Actual_weeks'].head(1).astype(int).values[0] - \
                             self.surv_data['Actual_weeks'].tail(1).astype(int).values[0]
        self.surv_data = self.surv_data.reset_index(drop=True)
        self.surv_data = self.add_current_week_info(self.surv_data)

        self.var_surv_data = self.surv_data[self.surv_data['Current_week'].isin([1, 2])]

        self.no_var_surv_data = self.surv_data[self.surv_data['Current_week'] >= 3]

        self.var_surv_data.drop(['Current_week'], axis=1, inplace=True)
        self.no_var_surv_data.drop(['Current_week'], axis=1, inplace=True)

        self.all_data = pd.concat([self.var_surv_data, self.no_var_surv_data, self.pres_five_week_data],
                                  axis=0).sort_values(by=['Test Date'], ascending=False).reset_index(drop=True)

        self.all_data = self.add_current_week_info(self.all_data)

        list_to_split = self.all_data['Current_week'].unique()
        split = list(create_chunks(list_to_split, 25))
        split_first = iter(split[0])
        slices = list(takewhile(bool, (list(islice(split_first, 0, i)) for i in [7, 18])))
        del split[0]
        if len(slices) > 1:
            split.insert(0, np.array(slices[1]))
        split.insert(0, np.array(slices[0]))
        self.month_rows = slices[0]
        self.var_data = []
        for n in range(len(split)):
            self.var_data.append(self.all_data[self.all_data['Current_week'].isin(split[n])])

        df_6wk_novar_data = self.var_data[0][~self.var_data[0]['Current_week'].isin([1, 2])]

        self.var_data.insert(1, df_6wk_novar_data)

        num_list = []

        for wk_val in range(0,len(self.var_data)):
            start_num_list = int(self.var_data[wk_val]['Weeks from Current Session Date'].head(1).apply(np.ceil))
            end_num_list = int(self.var_data[wk_val]['Weeks from Current Session Date'].tail(1).apply(np.ceil))
            # num_list.append('%s-%s weeks from current session' % (start_num_list, end_num_list))
            if wk_val == 0 or wk_val == 1:
                num_list.append('%s-%s weeks' % (start_num_list, end_num_list-1))
            elif wk_val == 2:
                num_list.append('%s-%s weeks' % (start_num_list-wk_val+1, end_num_list-wk_val+1))
            else:
                num_list.append('%s-%s weeks' % (start_num_list-wk_val+2, end_num_list-wk_val+1))

        self.actual_week_list = num_list[0:len(self.var_data)]

        self.presurv_data = self.spiro_values.loc[self.spiro_values['Mode'] == 1].reset_index(drop=True)

        self.current_data = self.spiro_values.loc[(self.spiro_values['FEV1MAX'].astype(float) >= low) &
                                                  (self.spiro_values['FEV1MAX'].astype(float) <= high)]

        self.var_data.append(self.presurv_data)
        self.var_data.append(self.current_data)
        self.actual_week_list.append('Pre-Surv NL Range')
        self.actual_week_list.append('Current NL Range')

        plot_data_2 = self.var_data[0]
        plot_data_1 = self.var_data[1]
        self.get_plot_data(db, plot_data_1, plot_data_2, rep, 'Variance')


    def add_current_week_info(self, df):
        week_cnt = 1
        cur_week = [week_cnt]
        for index, row in df.iterrows():
            if index > 0:
                if df['Session State'].loc[index - 1] != 'PER':
                    week_cnt = week_cnt + 1
                    cur_week.append(week_cnt)
                else:
                    cur_week.append(week_cnt)
        df['Current_week'] = cur_week
        return df

    def get_plot_data(self, db, plot_data_1, plot_data_2, rep, type):
        plot_data_1['Plot_1'] = 1
        plot_data_2['Plot_2'] = 2
        plot_data_1["Color"] = 'steelblue'
        plot_data_1['Alpha'] = 1
        plot_data_2["Color"] = 'steelblue'
        plot_data_2['Alpha'] = 1
        rep.raw_df = pd.merge(self.spiro_values, plot_data_2, how='outer')
        rep.raw_df = pd.merge(rep.raw_df, plot_data_1, how='outer')

        db.query_patient()
        if type == 'Conversion':
            limits = pd.concat([self.spiro_values.loc[self.spiro_values['FEV1MAX'].astype(float) ==
                                                      float(db.patient[0]['presurvlowfev1'])].tail(1),
                                self.spiro_values.loc[self.spiro_values['FEV1MAX'].astype(float) ==
                                                      float(db.patient[0]['presurvhighfev1'])].tail(1)], axis=0)
        if type == 'Variance':
            low = float(db.patient[0]['survlowfev1'])
            high = float(db.patient[0]['survhighfev1'])
            rep.raw_df['Month'] = None
            rep.raw_df['Month'] = np.where(rep.raw_df['Current_week'].isin(self.month_rows), 'Current_Month',
                                           rep.raw_df['Month'])
            rep.raw_df['Status'] = np.where(rep.raw_df['Status'].eq(3), 'Surv', rep.raw_df['Status'])
            rep.raw_df['Status'] = np.where(rep.raw_df['Status'].eq('1'), 'Pre-Surv',
                                            rep.raw_df['Status'])
            limits = pd.concat([self.spiro_values.loc[self.spiro_values['FEV1MAX'].astype(float) ==
                                                      low].tail(1),
                                self.spiro_values.loc[self.spiro_values['FEV1MAX'].astype(float) ==
                                                      high].tail(1)], axis=0)
            limits['Status'] = np.where(limits['Status'].eq(3), 'Surv', limits['Status'])
            limits['Status'] = np.where(limits['Status'].eq('1'), 'Pre-Surv',
                                            limits['Status'])
        plot_intersect = pd.merge(plot_data_2, plot_data_1, how='inner', on=['Weeks from Start Date'])
        limits["Plot_1"] = 1
        limits["Plot_2"] = 2
        limits["Color"] = 'white'
        limits['Alpha'] = 0
        limits['Weeks from Start Date'] = plot_intersect['Weeks from Start Date'].tail(1).values[0]
        rep.raw_df = pd.merge(rep.raw_df, limits, how='outer')

    def get_report_data(self, db=None):
        new_cols = ['PTN', 'FEV11', 'FEV12', 'FEV13', 'FEV14', 'FEV15', 'FEV16', 'FEV1MAX', 'MAX', 'Mean',
                    'FEV1xK', 'Test Date', 'Test Start Date', 'DTx', 'Status', 'Session State']
        cols_rename_dict = dict(zip(list(db.spiro_values.columns), new_cols))
        self.spiro_values = db.spiro_values.rename(columns=cols_rename_dict)
        temp_datetime = self.spiro_values['Test Date'].to_list()
        temp_date = [datetime.date(dt) for dt in temp_datetime]
        self.spiro_values['Test Date'] = temp_date
        self.spiro_values['HEARTRATEMAX'] = db.pulse_values['maxheartrate'].to_list()
        self.spiro_values['O2SATMAX'] = db.pulse_values['maxo2sat'].to_list()
        self.rep_val = {'pat_num': db.patient[0]['patientnum'],
                        'psmaxfev1': db.patient[0]['presurvhighfev1'],
                        'psminfev1': db.patient[0]['presurvlowfev1'], 'max_pulse_nl': db.patient[0]['heartmax'],
                        'min_pulse_nl': db.patient[0]['heartmin'], 'max_o2sat_nl': db.patient[0]['o2satmax'],
                        'min_o2sat_nl': db.patient[0]['o2satmin'], 'mean_o2sat_nl': db.patient[0]['o2satmean'],
                        'mean_pulse_nl': db.patient[0]['heartmean'], 'tx_date': db.patient[0]['dateoftransplant'],
                        'start_test_date': db.patient[0]['starttestdate']}
        if db.app == 'API':
            self.rep_val['session_date'] = datetime.strptime(db.date, '%Y-%m-%d').date()
            self.rep_val['session_count'] = db.session_count
            self.rep_val['fev1'] = [db.fev1, db.fev2, db.fev3, db.fev4, db.fev5, db.fev6]
            self.rep_val['max_all_fev1'] = db.max_all_fev1
            self.rep_val['mean_all_fev'] = db.mean_all_fev1
            # if db.surveysymptom:
            if db.survey_bool:
                db.query_survey()
                self.rep_val['surveyid'] = db.survey_table[0]['id']
            else:
                self.rep_val['surveyid'] = None
            self.rep_val['fev1var'] = False
            self.rep_val['fev1varpersist'] = False
            self.rep_val['lowestfev1'] = None
            self.rep_val['sminfev1'] = None
            self.rep_val['smaxfev1'] = None
            self.rep_val['mean_o2sat_abnl'] = None
            self.rep_val['min_o2sat_abnl'] = None
            self.rep_val['max_o2sat_abnl'] = None
            self.rep_val['o2satduration_abnl'] = None
            self.rep_val['mean_pulse_abnl'] = None
            self.rep_val['min_pulse_abnl'] = None
            self.rep_val['max_pulse_abnl'] = None
            self.rep_val['heartduration_abnl'] = None
        if db.app == 'DASHBOARD':
            self.rep_val['surveyid'] = db.report_details[0]['surveyid']
            self.rep_val['fev1var'] = db.report_details[0]['fev1var']
            self.rep_val['fev1varpersist'] = db.report_details[0]['fev1varpersist']
            self.rep_val['lowestfev1'] = db.report_details[0]['lowestfev1']
            self.rep_val['mean_o2sat_abnl'] = db.report_details[0]['o2satmean_abnl']
            self.rep_val['min_o2sat_abnl'] = db.report_details[0]['o2satmin_abnl']
            self.rep_val['max_o2sat_abnl'] = db.report_details[0]['o2satmax_abnl']
            self.rep_val['o2satduration_abnl'] = db.report_details[0]['o2satduration_abnl']
            self.rep_val['mean_pulse_abnl'] = db.report_details[0]['heartmean_abnl']
            self.rep_val['min_pulse_abnl'] = db.report_details[0]['heartmin_abnl']
            self.rep_val['max_pulse_abnl'] = db.report_details[0]['heartmax_abnl']
            self.rep_val['heartduration_abnl'] = db.report_details[0]['heartduration_abnl']

        self.spiro_values['Percent MAX'] = (self.spiro_values['FEV1MAX'] - self.spiro_values['MAX']) / \
                                           self.spiro_values['MAX']
        self.spiro_values['Percent MAX'] = self.spiro_values['Percent MAX'] * 100
        self.spiro_values['Percent MAX'] = self.spiro_values['Percent MAX'].astype(float).round()
        self.spiro_values['Percent MAX'] = self.spiro_values['Percent MAX'].astype(int)
        self.spiro_values['Percent Mean'] = (self.spiro_values['FEV1MAX'] - self.spiro_values['Mean']) / \
                                            self.spiro_values['Mean']
        self.spiro_values['Percent Mean'] = self.spiro_values['Percent Mean'] * 100
        self.spiro_values['Percent Mean'] = self.spiro_values['Percent Mean'].astype(float).round()
        self.spiro_values['Percent Mean'] = self.spiro_values['Percent Mean'].astype(int)
        self.spiro_values['Days'] = self.spiro_values['Test Date'] - self.spiro_values['Test Start Date']
        self.spiro_values['Days'] = self.spiro_values['Days'].dt.days
        self.spiro_values['Weeks from Start Date'] = self.spiro_values['Days'] / 7
        self.spiro_values['Weeks from Start Date'] = self.spiro_values['Weeks from Start Date'].astype(float).round(1)
        self.spiro_values['Days'] = self.spiro_values['Days'].astype(int)
        self.spiro_values = self.spiro_values[['PTN', 'FEV11', 'FEV12', 'FEV13', 'FEV14', 'FEV15', 'FEV16',
                                               'FEV1MAX', 'MAX', 'Percent MAX', 'Mean', 'Percent Mean', 'FEV1xK',
                                               'Test Date', 'Test Start Date', 'Days', 'DTx',
                                               'Weeks from Start Date', 'Status', 'Session State', 'HEARTRATEMAX',
                                               'O2SATMAX']]
        if db.patient[0]['currentmode'] == 3:
            strt_dt = self.spiro_values['Test Date'].tail(1).values[0]
            self.spiro_values['Days from Current Session Date'] =  strt_dt - self.spiro_values['Test Date']
            self.spiro_values['Days from Current Session Date'] = self.spiro_values['Days from Current Session Date'].dt.days
            self.spiro_values['Weeks from Current Session Date'] = (self.spiro_values['Days from Current Session Date'] / 7) + 1
            self.spiro_values['Weeks from Current Session Date'] = self.spiro_values['Weeks from Current Session Date'].astype(float).round(1)
            self.spiro_values.drop(columns=['Days from Current Session Date'],inplace=True)
            self.spiro_values = self.spiro_values[['PTN', 'FEV11', 'FEV12', 'FEV13', 'FEV14', 'FEV15', 'FEV16',
                                                   'FEV1MAX', 'MAX', 'Percent MAX', 'Mean', 'Percent Mean', 'FEV1xK',
                                                   'Test Date', 'Test Start Date', 'Days', 'DTx',
                                                   'Weeks from Start Date', 'Weeks from Current Session Date', 'Status', 'Session State', 'HEARTRATEMAX',
                                                   'O2SATMAX']]
        self.spiro_values['Actual_weeks'] = self.spiro_values['Weeks from Start Date'].apply(np.ceil)
        self.rep_val['week'] = int(self.spiro_values['Weeks from Start Date'].tail(1).apply(np.ceil))
        self.spiro_values['Mode'] = self.spiro_values['Status']

    def plot_fig(self, x_df, y_df, pmax_df, yu, yl, color, alpha, title):
        x = x_df.to_numpy()
        y = y_df.to_numpy()
        pmax = pmax_df.to_numpy()
        x_mod = x_df.head(-2).to_numpy()
        y_mod = y_df.head(-2).to_numpy()
        # Make and fit a linear regression model
        model = LinearRegression().fit(x_mod.reshape(-1, 1), y_mod)
        # x values need to be in a two-dimensional array, so use .reshape(-1, 1)

        # Find the slope and intercept from the model
        slope = model.coef_[0]  # Takes the first element of the array
        intercept = model.intercept_
        y_predicted = [slope * i + intercept for i in x]
        source = ColumnDataSource(data=dict(
            x=x,
            y=y,
            pmax=pmax,
            y_upper=np.repeat(yu * 1000, len(x_df.to_list())),
            y_lower=np.repeat(yl * 1000, len(x_df.to_list())),
            color=color,
            alpha=alpha,
            y_predicted=y_predicted,
        ))

        TOOLTIPS = [
            ("(Weeks, FEV1, PercentFEV1Max)", "(@x{0}, @y{0}, @pmax{0})"),
        ]
        # Plot the data and regression line
        fig = figure(title=Title(text=title), x_axis_label='Weeks from Start Date', y_axis_label='FEV1 (ml)', x_range=DataRange1d(),
                     y_range=DataRange1d(), plot_width=600, plot_height=400, min_border=0)
        fig.y_range = DataRange1d()
        # Create 1st RHS y-axis

        fig.extra_y_ranges['pmax'] = DataRange1d()
        fig.add_layout(LinearAxis(y_range_name='pmax', axis_label='Percent FEV1 Max'), 'right')
        c1 = Circle(x='x', y='y', line_color=None, fill_color='darkorange', fill_alpha='alpha', size=9)
        c1_renderer = fig.add_glyph(source, c1)
        fig.add_tools(HoverTool(tooltips=TOOLTIPS, renderers=[c1_renderer]))
        fig.y_range.renderers = [c1_renderer]
        c2 = Circle(x='x', y='pmax', line_color=None, fill_color=None)
        c2_renderer = fig.add_glyph(source, c2, y_range_name="pmax")
        fig.extra_y_ranges['pmax'].renderers = [c2_renderer]
        fig.title.align = 'center'
        # Make the regression line
        slope, intercept, r_value, p_value, std_err = stats.linregress(x_mod, y_mod)

        l2 = Line(x='x', y='y_upper', line_color='lightgreen', line_width=2)
        fig.add_glyph(source, l2)
        l3 = Line(x='x', y='y_lower', line_color='lightgreen', line_width=2)
        fig.add_glyph(source, l3)
        if round((p_value / 2), 2) == 0:
            p_text = 'p<0.01'
        else:
            p_text = 'p=' + str(round((p_value / 2), 2))

        fig.line(x, y_predicted, color='indianred', line_width=2,
                 legend='y=' + str(round(slope, 2)) + 'x+' + str(round(intercept, 2)) + '\n' + 'R²=' + str(
                     round(r_value ** 2, 3)) + ', ' + p_text)
        fig.legend.label_text_font_size = "11pt"
        fig.legend.label_text_font_style = "bold"
        fig.legend.location = "bottom_center"
        fig.legend.background_fill_alpha = 0.0
        fig.legend.border_line_alpha = 0.0
        fig.toolbar.logo = None
        fig.toolbar_location = None
        fig.title.text_font_size = '12pt'
        fig.xaxis.axis_label_text_font_size = "12pt"
        fig.yaxis.axis_label_text_font_size = "12pt"
        fig.xaxis.axis_label_text_font_style = "bold"
        fig.yaxis.axis_label_text_font_style = "bold"
        fig.xaxis.major_label_text_font_size = "10pt"
        fig.yaxis.major_label_text_font_size = "10pt"
        # fig.xgrid.grid_line_color = None
        # fig.ygrid.grid_line_color = None
        return fig

    def create_plot_objects(self, x, y, pmax, yu, yl, color, alpha, plot_title):
        plot = self.plot_fig(x, y, pmax, yu, yl, color, alpha, plot_title)
        # serialize as bokeh document to JSON#
        doc = Document()
        curdoc().theme = 'dark_minimal'
        doc.add_root(plot)
        doc_json = doc.to_json()
        plot_string = json.dumps(doc_json)
        #####################################
        plot.sizing_mode = "scale_both"
        scripts, div = components(plot)
        return plot_string, scripts, div

    def get_header(self, file):
        self.headers = pd.read_csv(file, nrows=14, keep_default_na=False, na_values=['_'], header=None)
        excel_data = list()
        self.header_table = dict()
        header = self.headers.values.tolist()
        for head in header:
            row_data = [val for val in head if val != ""]
            excel_data.append(row_data)
        self.title = excel_data[0][0]
        self.header_table['pat_num'] = excel_data[1][1]
        self.header_table['report_date'] = excel_data[2][1]
        self.header_table['questionnare'] = excel_data[2][3]
        self.header_table['symptoms'] = excel_data[2][4]
        self.header_table['week_title'] = excel_data[3][0]
        self.header_table['week'] = excel_data[3][1]
        self.header_table['ps_nl_high'] = excel_data[5][2]
        self.header_table['ps_nl_low'] = excel_data[5][1]
        self.header_table['s_nl_high'] = excel_data[6][2]
        self.header_table['s_nl_low'] = excel_data[6][1]
        self.header_table['meano2sat_nl'] = excel_data[6][4]
        self.header_table['meano2sat_abnl'] = excel_data[6][5]
        self.header_table['o2satrng_nl'] = excel_data[7][1]
        self.header_table['o2satrng_abnl'] = excel_data[7][2]
        self.header_table['fev1_var'] = excel_data[8][1]
        self.header_table['duro2sat_nl'] = excel_data[8][3]
        self.header_table['duro2sat_abnl'] = excel_data[8][4]
        self.header_table['fev1_var_pers'] = excel_data[9][1]
        self.header_table['hr_nl'] = excel_data[9][3]
        self.header_table['hr_abnl'] = excel_data[9][4]
        self.header_table['lowest_fev1'] = excel_data[10][1]
        self.header_table['durhr_nl'] = excel_data[10][3]
        self.header_table['durhr_abnl'] = excel_data[10][4]
        self.header_table['decision'] = excel_data[11][1]
        self.header_table['comment'] = excel_data[12][1]

    def get_summary(self, file):
        self.summary_raw_table = pd.read_csv(file, skiprows=25)
        self.index_to_raw = int(self.summary_raw_table[self.summary_raw_table.eq('PTN').any(1)].index[0])
        self.summary_table = self.summary_raw_table.drop(range(self.index_to_raw, len(self.summary_raw_table.index)))
        self.summary_table = self.summary_table[
            self.summary_table.columns.drop(list(self.summary_table.filter(regex='Unnamed')))]
        self.summary_table = self.summary_table.dropna()
        cols = self.summary_table.columns.drop(list(self.summary_table.filter(regex='Weeks')))
        self.summary_table[cols] = self.summary_table[cols].apply(pd.to_numeric, errors='coerce')


    def get_raw(self):
        self.raw_table = self.summary_raw_table.drop(range(0, self.index_to_raw)).reset_index(drop=True)
        col_names = self.raw_table.iloc[0].tolist()
        self.raw_table.columns = col_names
        self.raw_table = self.raw_table.drop(range(0, 1))
        if not all(self.raw_table.columns.isnull()):
            self.raw_table = self.raw_table.loc[:, self.raw_table.columns.notnull()]
        self.raw_table['Percent MAX'] = self.raw_table['Percent MAX'].astype(int)
        self.raw_table['Weeks from Start Date'] = self.raw_table['Weeks from Start Date'].astype(float)
        self.raw_table['FEV1xK'] = self.raw_table['FEV1xK'].astype(float)
        if 'Weeks from Current Session Date' in self.raw_table.columns:
            self.raw_table['Weeks from Current Session Date'] = self.raw_table['Weeks from Current Session Date'].astype(float)

    def report_parser(self, file=None, report_type=None):
        self.get_header(file)
        plot_title = ['', '']
        self.get_summary(file)
        self.get_raw()
        ##################Bokeh plot###################
        if report_type == 'Conversion':
            y_upper = float(self.header_table['ps_nl_high'])
            y_lower = float(self.header_table['ps_nl_low'])
            plot_title[0] = 'Pre-Surveillance Weeks 1-%s' % (int(self.header_table['week']))
            plot_title[1] = plot_title[0]
            if len(self.summary_table.index) > 1:
                plot_title[1] = 'Pre-Surveillance Weeks %s-%s' % (
                    int(self.raw_table.tail(28)['Weeks from Start Date'].head(1).apply(np.round).values[0]),
                    int(self.header_table['week']))

        if report_type == 'Variance':
            y_upper = float(self.header_table['s_nl_high'])
            y_lower = float(self.header_table['s_nl_low'])
            plot_title[0] = 'Monitoring Weeks (%s from current session with variance)' %(self.summary_table['Weeks from Current Session'].head(2).values[1])
            plot_title[1] = 'Monitoring Weeks (%s from current session with variance)' %(self.summary_table['Weeks from Current Session'].head(2).values[0])

        raw_table = self.raw_table[self.raw_table['Plot_1'] == '1']

        x_1 = raw_table['Weeks from Start Date']
        y_1 = raw_table['FEV1xK']
        pmax_1 = raw_table['Percent MAX']
        color_1 = raw_table['Color']
        alpha_1 = raw_table['Alpha']
        raw_table = self.raw_table[self.raw_table['Plot_2'] == '2']

        x_2 = raw_table['Weeks from Start Date']
        y_2 = raw_table['FEV1xK']
        pmax_2 = raw_table['Percent MAX']
        color_2 = raw_table['Color']
        alpha_2 = raw_table['Alpha']

        plot_1_string, scripts_1, div_1 = self.create_plot_objects(x_1, y_1, pmax_1, y_upper, y_lower, color_1, alpha_1,
                                                                   plot_title[0])
        plot_2_string, scripts_2, div_2 = self.create_plot_objects(x_2, y_2, pmax_2, y_upper, y_lower, color_2, alpha_2,
                                                                   plot_title[1])

        summary_html = self.summary_table.to_html(classes='tg table table-striped table-light', index=False)
        summary_html = summary_html.replace('<table border="1" class="dataframe tg table table-striped table-light">',
                                            '<table id="sumtabled" border="1" class="dataframe tg table table-striped table-light" '
                                            'align="center" style="width:98%"> ')

        self.raw_table = self.raw_table[self.raw_table.Color != 'white']
        self.raw_table.drop(columns=['Mode', 'Actual_weeks', 'Session State', 'Plot_1', 'Color', 'Alpha', 'Plot_2'],
                            axis=1,
                            inplace=True)
        if 'Month' in self.raw_table.columns and 'Current_week' in self.raw_table.columns:
            self.raw_table.drop(columns=['Month', 'Current_week'],
                                axis=1,
                                inplace=True)

        raw_html = pd.DataFrame(columns=list(self.raw_table.columns))
        raw_html = raw_html.to_html(classes='tg table table-striped table-light', index=False)
        raw_html = raw_html.replace('<table border="1" class="dataframe tg table table-striped table-light">',
                                    '<table id="rawtabled" border="1" class="tg table table-striped" '
                                    'align="center" style="width:98%;"> ')
        raw_json = self.raw_table.to_json(orient='values')

        self.context = {"title": self.title, "header_table": self.header_table,
                        "summary_table": summary_html, "raw_table": raw_html,
                        "scripts_1": scripts_1, "div_1": div_1,
                        "scripts_2": scripts_2, "div_2": div_2,
                        "plot_1": plot_1_string, "plot_2": plot_2_string,
                        "raw_json": raw_json, "survey": False}
        if self.headers.iloc[2,9] != 'NA':
            sur_idx = [int(x) for x in self.headers.iloc[2,9].split(',')]
            self.sur_ans = self.sur_ans[self.sur_ans['Number'].isin(sur_idx)]
            sur_ans_html = self.sur_ans.to_html(classes='tg', index=False)
            sur_ans_html = sur_ans_html.replace('<th>', '<th class="tg-km37">')
            sur_ans_html = sur_ans_html.replace('<table border="1" class="dataframe tg">',
                                            '<table id="sur_tbl" border="1" class="dataframe tg">')
            self.context['survey'] =  True
            self.context = {**self.context, **{"survey_table": sur_ans_html}}

    ####Generate PDF Report as attachment to email#####
    def generate_pdf(self, template='ecp_hs_app/report_snap.html', context=None, file=None, email=False,
                     message=None, subject=None, attach=False, receivers=None):
        """Generate pdf."""
        # Rendered
        path_1 = os.path.join(settings.MEDIA_ROOT, context['path_1'].replace(settings.MEDIA_URL, ""))
        path_2 = os.path.join(settings.MEDIA_ROOT, context['path_2'].replace(settings.MEDIA_URL, ""))
        # deserialize as bokeh document#######
        curdoc().theme = 'dark_minimal'
        plot_1 = curdoc().from_json(json.loads(context['plot_1']))
        plot_2 = curdoc().from_json(json.loads(context['plot_2']))
        #####################################
        options = ChromeOptions()
        options.add_argument('--headless')
        options.add_argument('--no-sandbox') 
        driver = Chrome(executable_path='/data/ecp-hs/dashboard/venv/bin/chromedriver', options=options)
        export_png(plot_1, filename=path_1, webdriver=driver)
        export_png(plot_2, filename=path_2, webdriver=driver)
        driver.quit()
        context['path_1'] = 'file:///' + path_1
        context['path_2'] = 'file:///' + path_2
        html_template = get_template(template)
        html = html_template.render(context).encode(encoding="ISO-8859-1")
        html = HTML(string=html, base_url=None)
        stylesheets = [CSS("https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css")]
        html.write_pdf(target=file, stylesheets=stylesheets, presentational_hints=True)
        if email:
            self.send_email(subject=subject, message=message, file=file, attach=attach, receivers=receivers)

    def send_email(self, subject=None, message=None, file=None, attach=True, receivers=None):
        sender = config('EMAIL_HOST_USER', cast=str)

        headers = {'Reply-To': config('REPLY_TO', cast=str)}

        msg = EmailMessage(subject, message, sender, receivers, headers=headers)
        msg.content_subtype = "html"
        if attach:
            msg.attach_file(file)
        msg.send()


def create_chunks(list_name, n):
    if len(list_name) > n:
        for i in range(0, len(list_name), n):
            yield list_name[i:i + n]
    else:
        yield list_name[0:len(list_name)]
