from .basic_report import BasicReport
from datetime import date
from openpyxl import load_workbook
import pandas as pd
import numpy as np
from scipy import stats


class ConversionReport(BasicReport):
    def __init__(self, rep):
        BasicReport.__init__(self, workbook_path=rep.temp_file, save_path=rep.filename)
        self.rep_val = rep.rep_val
        self.sheet = 'Report'
        self.ws = self.wb[self.sheet]
        self.raw_df = rep.spiro_values
        self.raw_df['Status'] = 'Pre-surv'

    def header_table(self):
        self.ws['E2'] = self.rep_val['pat_num']
        self.ws['E3'] = date.today()
        self.ws['E4'] = self.rep_val['week']
        self.ws['D6'] = self.rep_val['psminfev1']
        self.ws['E6'] = self.rep_val['psmaxfev1']
        self.ws['I7'] = self.rep_val['mean_o2sat_nl']
        self.ws['I8'] = str(self.rep_val['min_o2sat_nl']) + '-' + str(self.rep_val['max_o2sat_nl'])
        self.ws['I9'] = 'NA'
        self.ws['I10'] = str(self.rep_val['mean_pulse_nl']) + ' (' + str(self.rep_val['min_pulse_nl']) + '-' + str(
                             self.rep_val['max_pulse_nl']) + ')'

    def summary_table(self):
        columns = ['Weeks from Start Date', 'Mean', 'Min', 'Max', 'SD 2', 'CV (2SD)', 'Slope (mL/Week)',
                                            'R', 'R-square', 'Sessions', 'P-value']
        self.summary_df = pd.concat(
            [pd.DataFrame([['%s weeks' % str(int(self.raw_df['Weeks from Start Date'].head(i).apply(np.round).values[-1])),
                            self.raw_df['FEV1MAX'].head(i).astype(float).mean().round(2),
                self.raw_df['FEV1MAX'].head(i).astype(float).min().round(2), self.raw_df['FEV1MAX'].head(i).astype(float).max().round(2),
                 (self.raw_df['FEV1MAX'].head(i).head(i).astype(float).std() * 2).round(2),
                 ((self.raw_df['FEV1MAX'].head(i).astype(float).std() * 2 / self.raw_df['FEV1MAX'].head(i).astype(float).mean())*100).round(),
                round(stats.linregress(self.raw_df['Weeks from Start Date'].head(i).astype(float),
                                                                       self.raw_df['FEV1xK'].head(i).astype(float))[0]),
                round(stats.linregress(self.raw_df['Weeks from Start Date'].head(i).astype(float),
                                                                       self.raw_df['FEV1xK'].head(i).astype(float))[2], 2),
                round(stats.linregress(self.raw_df['Weeks from Start Date'].head(i).astype(float),
                                                                       self.raw_df['FEV1xK'].head(i).astype(float))[2]**2, 3), int(i),
                round((stats.linregress(self.raw_df['Weeks from Start Date'].head(i).astype(float),
                                                                       self.raw_df['FEV1xK'].head(i).astype(float))[3]/2), 2)]], columns=columns) for i in range(30, min(self.rep_val['session_count']+1, 45), 7)],
                ignore_index=True)
        if self.rep_val['session_count'] > 44:
            for slice in range(51, self.rep_val['session_count']+1, 7):
                raw_tb = self.raw_df.head(slice)
                df_data = pd.DataFrame([['%s weeks' % str(int(raw_tb['Weeks from Start Date'].tail(1).apply(np.round).values[0])),
                                         self.raw_df['FEV1MAX'].head(slice).astype(float).mean().round(2),
                self.raw_df['FEV1MAX'].head(slice).astype(float).min().round(2), self.raw_df['FEV1MAX'].head(slice).astype(float).max().round(2),
                 (self.raw_df['FEV1MAX'].head(slice).head(slice).astype(float).std() * 2).round(2),
                 ((self.raw_df['FEV1MAX'].head(slice).astype(float).std() * 2 / self.raw_df['FEV1MAX'].head(slice).astype(float).mean())*100).round(),
                round(stats.linregress(self.raw_df['Weeks from Start Date'].head(slice).astype(float),
                                                                       self.raw_df['FEV1xK'].head(slice).astype(float))[0]),
                round(stats.linregress(self.raw_df['Weeks from Start Date'].head(slice).astype(float),
                                                                       self.raw_df['FEV1xK'].head(slice).astype(float))[2], 2),
                round(stats.linregress(self.raw_df['Weeks from Start Date'].head(slice).astype(float),
                                                                       self.raw_df['FEV1xK'].head(slice).astype(float))[2]**2, 3), int(slice),
                round((stats.linregress(self.raw_df['Weeks from Start Date'].head(slice).astype(float),
                                                                       self.raw_df['FEV1xK'].head(slice).astype(float))[3]/2), 2)]], columns=columns)
                df_sub_data = pd.concat([pd.DataFrame([['%s - %s weeks' % (str(int(raw_tb['Weeks from Start Date'].tail(i).apply(np.round).values[0])),
                                                                           str(int(raw_tb['Weeks from Start Date'].tail(1).apply(np.round).values[0]))),
                                                    raw_tb['FEV1MAX'].tail(i).astype(float).mean().round(2),
                raw_tb['FEV1MAX'].tail(i).astype(float).min().round(2), raw_tb['FEV1MAX'].tail(i).astype(float).max().round(2),
                 (raw_tb['FEV1MAX'].tail(i).tail(i).astype(float).std() * 2).round(2),
                 ((raw_tb['FEV1MAX'].tail(i).astype(float).std() * 2 / raw_tb['FEV1MAX'].tail(i).astype(float).mean())*100).round(),
                round(stats.linregress(raw_tb['Weeks from Start Date'].tail(i).astype(float),
                                                                       raw_tb['FEV1xK'].tail(i).astype(float))[0]),
                round(stats.linregress(raw_tb['Weeks from Start Date'].tail(i).astype(float),
                                                                       raw_tb['FEV1xK'].tail(i).astype(float))[2], 2),
                round(stats.linregress(raw_tb['Weeks from Start Date'].tail(i).astype(float),
                                                                       raw_tb['FEV1xK'].tail(i).astype(float))[2]**2, 3), int(i),
                round((stats.linregress(raw_tb['Weeks from Start Date'].tail(i).astype(float),
                                                                       raw_tb['FEV1xK'].tail(i).astype(float))[3]/2), 2)]], columns=columns) for i in range(42, 27, -14)],
                ignore_index=True)
                self.summary_df = pd.concat([self.summary_df, df_data, df_sub_data], axis=0)
        if self.excel == True:
            book = load_workbook(self.save_path)
            writer = pd.ExcelWriter(self.save_path, engine='openpyxl')
            writer.book = book
            writer.sheets = {ws.title: ws for ws in writer.book.worksheets}
            self.summary_df.to_excel(writer, sheet_name='Report', startrow=25, index=False, engine='openpyxl')
            writer.save()

    def generateReport(self):
        self.wb.save(filename=self.save_path)




