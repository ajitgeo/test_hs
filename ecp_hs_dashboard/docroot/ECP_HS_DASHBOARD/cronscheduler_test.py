from crontab import CronTab

cron = CronTab(user='root')
# cron.remove_all()
job = cron.new(command='cp -rf /data/ecp-hs/dashboard/bkup/ECP_HS_DASHBOARD/25-ecp-app-hsdev1.nrg.wustl.edu_8081.conf /etc/httpd/conf.d/ && cp -rf /data/ecp-hs/dashboard/bkup/ECP_HS_DASHBOARD/wsgi.conf /etc/httpd/conf.modules.d/ && systemctl restart httpd.service', comment='cronjob')
job.minute.on(32)

cron.write()