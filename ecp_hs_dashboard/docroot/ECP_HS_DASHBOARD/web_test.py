# import os
# import django
# os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'ECP_HS_DASHBOARD.settings')
# django.setup()
# from django.test import Client
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver import Chrome, ChromeOptions
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.action_chains import ActionChains
import time
from ecp_hs_app.models import Patient, Session, Spiro, Pulseox, Survey, PulseoxRaw, SpiroRaw
import json
from pandas import json_normalize
import pandas as pd



# c = Client()
# response = c.post('/login/', {'username': 'tester', 'password': 'ecphs123'})
# response = c.get('/highprworklist/')
# session = c.session
# session['test'] = True
# session['choices'] ='TPV'
# session.save()
# response = c.post('/reports/59/')

# options = ChromeOptions()
# options.add_argument('--headless')
# options.add_argument('--no-sandbox')
# options.add_argument('--window-size=1800,1200')
# driver = Chrome(executable_path='/data/ecp-hs/dashboard/venv/bin/chromedriver', options=options)
# url = 'hstest.wustl.edu'
# driver.get('https://%s/login/'%url)
# driver.find_element_by_id('id_username').send_keys("tester")
# driver.find_element_by_id ('id_password').send_keys("django123")
# driver.find_element_by_id('id_submit').click()
# driver.get('https://%s/highprworklist/'%url)
# num = 74
# from_txt = 'highprworklist'
# driver.find_element_by_xpath('//a[@href="/reports/%s/?from=/%s/"]'%(num,from_txt)).click()
# # driver.save_screenshot('/data/ecp-hs/dashboard/test.png')
# element_to_hover_over = driver.find_element_by_id("btn-form")
# hover = ActionChains(driver).move_to_element(element_to_hover_over)
# hover.perform()
# select = Select(driver.find_element_by_id('id_choices'))
# select.select_by_visible_text('Consistent with False Positive Variance')
# driver.find_element_by_id('id_comment').clear()
# driver.find_element_by_id('id_comment').send_keys("fpv")
# driver.find_element_by_id('mysubmit').click()
# driver.get('http://127.0.0.1:8000/logout/')

def cmi_simulation(report_id, decision):
	options = ChromeOptions()
	options.add_argument('--headless')
	options.add_argument('--no-sandbox')
	options.add_argument('--window-size=1800,1200')
	driver = Chrome(executable_path='/data/ecp-hs/dashboard/venv/bin/chromedriver', options=options)
	url = 'hstest.wustl.edu'
	driver.get('https://%s/login/'%url)
	driver.find_element_by_id('id_username').send_keys("tester")
	driver.find_element_by_id ('id_password').send_keys("django123")
	driver.find_element_by_id('id_submit').click()
	driver.get('https://%s/highprworklist/'%url)
    num = int(report_id)
    from_txt = 'highprworklist'
    try:
        driver.find_element_by_xpath('//a[@href="/reports/%s/?from=/%s/"]'%(num,from_txt)).click()
    except:
        return 1
    element_to_hover_over = driver.find_element_by_id("btn-form")
    hover = ActionChains(driver).move_to_element(element_to_hover_over)
    # driver.save_screenshot('D:\\Project\\test.jpg')
    hover.perform()
    select = Select(driver.find_element_by_id('id_choices'))
    select.select_by_visible_text(decision)
    driver.find_element_by_id('id_comment').clear()
    driver.find_element_by_id('id_comment').send_keys("")
    driver.find_element_by_id('mysubmit').click()
    driver.get('http://%s/logout/'%url)
    return 0


def file_parser(file):
    try:
        with open(file) as json_file:
            data = json.load(json_file)
        fields = {}
        field = json_normalize(data['generalInformation'])
        fields['session_date'] = field['time'][0]
        fields['tablet_version'] = field['versionNumber'][0]
        field = json_normalize(data['generalInformation']['userdata'])
        fields['imei'] = field['imei'][0]
        fields['session_state'] = field['currentSessionState'][0]
        field = json_normalize(data['spirometer']['fvcResults'])
        raw_pef = ['peakflow', 'fev7', 'peakflowtime', 'extrapolatedvol']
        raw_pef_dict = dict(zip(list(field.columns), raw_pef))
        field.rename(columns=raw_pef_dict, inplace=True)
        fields['raw_pef'] = field
        field = json_normalize(data['spirometer']['fev1Results'])
        raw_blows = ['pef', 'fev1', 'fvc', 'fev1fvcratio', 'fev6', 'fef2575']
        raw_blows_dict = dict(zip(list(field.columns), raw_blows))
        field.rename(columns=raw_blows_dict, inplace=True)
        fields['raw_blows'] = field
        fev1 = ['fev1', 'fev2', 'fev3', 'fev4', 'fev5', 'fev6']
        for index, fd in fields['raw_blows'].iterrows():
            fields[fev1[index]] = fd['fev1']
        field = json_normalize(data['spirometer'])
        fields['spiro_var'] = str(int(field['isSpiroVar'][0]))
        fields['survey'] = False

        if fields['session_state'] != 'EM' and fields['session_state'] != 'TM':
            field = json_normalize(data['pulseOx']).iloc[:,0:7]
            pulseox = ['minHR', 'maxHR', 'lowo2sat', 'timeAbnormal', 'timeMinRate', 'o2_var', 'pulse_var']
            pulseox_dict = dict(zip(list(field.columns), pulseox))
            field.rename(columns=pulseox_dict, inplace=True)
            field = field.to_dict('r')[0]
            fields = {**fields, **field}
            fields['pulse_var'] = str(int(fields['pulse_var']))
            fields['o2_var'] = str(int(fields['o2_var']))
            field = json_normalize(data['pulseOx']['pulseData'])
            field[['w1','w2','w3']] = pd.DataFrame(field.waves.tolist())
            field.drop(['waves'],axis=1,inplace=True)
            raw_pulse = ['pulserate', 'oxygensaturation', 'pulsestrength', 'perfusionindex', 'pulsewave1', 'pulsewave2', 'pulsewave3']
            raw_pulse_dict = dict(zip(list(field.columns), raw_pulse))
            field.rename(columns=raw_pulse_dict, inplace=True)
            fields['raw_pulse'] = field
            if 'survey' in data:
                fields['survey'] = True
                field = json_normalize(data['survey'])
                field[['q1', 'q2', 'q3', 'q4', 'q5']] = pd.DataFrame(field.qAnsArr.tolist())
                field[['q11', 'q12', 'q13', 'q14', 'q15', 'q16', 'q17']] = pd.DataFrame(field.q1SubArr.tolist())
                field[['q21', 'q22', 'q23', 'q24']] = pd.DataFrame(field.q2SubArr.tolist())
                field[['q31', 'q32', 'q33', 'q34', 'q35']] = pd.DataFrame(field.q3SubArr.tolist())
                field[['q41', 'q42', 'q43']] = pd.DataFrame(field.q4SubArr.tolist())
                field[['q51', 'q52', 'q53', 'q54']] = pd.DataFrame(field.q5SubArr.tolist())
                field.drop(['qAnsArr','q1SubArr','q2SubArr','q3SubArr','q4SubArr','q5SubArr'],axis=1,inplace=True)
                survey = ['symptoms', 'q1', 'q2', 'q3', 'q4', 'q5', 'q11', 'q12', 'q13', 'q14', 'q15', 'q16', 'q17', 'q21', 'q22', 'q23', 'q24', 'q31', 'q32', 'q33', 'q34', 'q35', 'q41', 'q42', 'q43', 'q51', 'q52', 'q53', 'q54']
                survey_dict = dict(zip(list(field.columns), survey))
                field.rename(columns=survey_dict, inplace=True)
                field = field.to_dict('r')[0]
                fields = {**fields, **field}
                fields['symptoms'] = str(int(fields['symptoms']))
            else:
                fields['symptoms'] = '0'
        fields['smweekcount'] = 0
        fields['emweekcount'] = 0
        fields['persistence_count'] = 0
        return fields
    except:
        return 1


def query_database():
    sessionid = Session.objects.values('id').order_by('-id')[0]['id']
    spiro = Spiro.objects.filter(sessionid=sessionid).values('id', 'peakflow', 'fev17', 'peakflowtime', 'extrapolatedvol')[0]
    spiro_df = pd.DataFrame({k: [v] for k, v in spiro.items()})
    spiroid = spiro_df['id'].values[0]
    spiro_df.drop(columns='id', inplace=True)
    spiro_raw_df = pd.DataFrame(list(SpiroRaw.objects.filter(spiroid=spiroid).values('pef', 'fev1', 'fvc', 'fevfvcratio', 'fev6', 'fef2575')))
    try:
        pulseox = Pulseox.objects.filter(sessionid=sessionid).values('id','minheartrate','maxheartrate','mino2sat','timeo2satabnormal','timeheartrateabnormal')[0]
        pulseox_df = pd.DataFrame({k: [v] for k, v in pulseox.items()})
        pulseoxid = pulseox_df['id'].values[0]
        pulseox_df.drop(columns='id', inplace=True)
        pulseox_raw_df = pd.DataFrame(list(PulseoxRaw.objects.filter(pulseoxid=pulseoxid).values('pulserate', 'o2sat', 'pulse_strength', 'perfusion_index', 'pwave1', 'pwave2', 'pwave3')))
    except:
        pulseox_df = pd.DataFrame(['empty'],columns=['Data'])
        pulseox_raw_df = pd.DataFrame(['empty'],columns=['Data'])
    try:
        survey = Survey.objects.filter(sessionid=sessionid).values()[0]
        survey_df = pd.DataFrame({k: [v] for k, v in survey.items()})
        survey_df.drop(columns=['id','sessionid_id'], inplace=True)
        survey_df.replace(to_replace='Yes', value=1, inplace=True)
        survey_df.replace(to_replace='No', value=0, inplace=True)
        survey_df.replace(to_replace='', value=0, inplace=True)
        survey_df.replace(to_replace='[A-Z][a-z]', value=1, regex=True, inplace=True)
    except:
        survey_df = pd.DataFrame(['empty'],columns=['Data'])

    return [spiro_df, spiro_raw_df, pulseox_df, pulseox_raw_df, survey_df]


def compare_file_db(fields, db_fields):
    spiro_file_df = fields['raw_pef']
    spiro_file_df = spiro_file_df.astype(float)
    spiro_db_df = db_fields[0]
    spiro_db_df.rename(columns={'fev17': 'fev7'}, inplace=True)
    spiro_db_df = spiro_db_df.astype(float)
    bool_spiro = spiro_db_df.equals(spiro_file_df)
    spiro_raw_file_df = fields['raw_blows']
    spiro_raw_file_df = spiro_raw_file_df.astype(float)
    spiro_raw_db_df = db_fields[1]
    spiro_raw_db_df.rename(columns={'fevfvcratio': 'fev1fvcratio'}, inplace=True)
    spiro_raw_db_df = spiro_raw_db_df.astype(float)
    bool_spiro_raw = spiro_raw_db_df.equals(spiro_raw_file_df)

    if 'pulse_var' in fields:
        pulse_file = ['minHR','maxHR','lowo2sat','timeAbnormal','timeMinRate']
        pulse_file = {key: fields[key] for key in pulse_file}
        pulse_file_df = pd.DataFrame({k: [v] for k, v in pulse_file.items()})
        pulse_db_df = db_fields[2]
        pulse_db_df.columns = pulse_file
        pulse_db_df = pulse_db_df.astype(float)
        pulse_file_df = pulse_file_df.astype(float)
        pulse_raw_file_df = fields['raw_pulse']
        pulse_file_raw = list(pulse_raw_file_df.columns)
        pulse_raw_file_df = pulse_raw_file_df.astype(float)
        pulse_raw_db_df = db_fields[3]
        pulse_raw_db_df.columns = pulse_file_raw
        pulse_raw_db_df = pulse_raw_db_df.astype(float)
    else:
        pulse_file_df = pd.DataFrame(['empty'], columns=['Data'])
        pulse_raw_file_df = pd.DataFrame(['empty'], columns=['Data'])
    bool_pulse = pulse_db_df.equals(pulse_file_df)
    bool_pulse_raw = pulse_raw_db_df.equals(pulse_raw_file_df)

    if fields['survey']:
        survey = ['symptoms', 'q1', 'q2', 'q3', 'q4', 'q5', 'q11', 'q12', 'q13', 'q14', 'q15', 'q16', 'q17', 'q21', 'q22',
                  'q23', 'q24', 'q31', 'q32', 'q33', 'q34', 'q35', 'q41', 'q42', 'q43', 'q51', 'q52', 'q53', 'q54']
        survey_file = {key: fields[key] for key in survey}
        survey_file_df = pd.DataFrame({k: [v] for k, v in survey_file.items()})
        survey_db_df = db_fields[4]
        survey_file_df = survey_file_df.astype(int)
        survey_db_df.rename(columns={'surveysymptom': 'symptoms'}, inplace=True)
        survey_db_df = survey_db_df[survey]
        survey_db_df = survey_db_df.astype(int)
    else:
        survey_file_df = pd.DataFrame(['empty'], columns=['Data'])
    bool_survey = survey_db_df.equals(survey_file_df)

    bool_val = all([bool_spiro, bool_spiro_raw, bool_pulse, bool_pulse_raw, bool_survey])
    return bool_val

file = 'D:\\Project\\unit_test\\test_data\\variancesym_20210503.json'
fields = file_parser(file)
db_fields = query_database()
bool_val = compare_file_db(fields, db_fields)