from .basic_report import BasicReport
from datetime import date
from openpyxl import load_workbook
import pandas as pd
from scipy import stats


class VarianceReport(BasicReport):
    def __init__(self, rep):
        BasicReport.__init__(self, workbook_path=rep.temp_file, save_path=rep.filename)
        self.rep_val = rep.rep_val
        self.sheet = 'Report'
        self.ws = self.wb[self.sheet]
        self.raw_df = rep.spiro_values

    def header_table(self):
        self.ws['E2'] = self.rep_val['pat_num']
        self.ws['E3'] = date.today()
        self.ws['E4'] = self.rep_val['week']
        self.ws['D6'] = self.rep_val['psminfev1']
        self.ws['E6'] = self.rep_val['psmaxfev1']
        self.ws['I7'] = self.rep_val['mean_o2sat_nl']
        self.ws['I8'] = str(self.rep_val['min_o2sat_nl']) + '-' + str(self.rep_val['max_o2sat_nl'])
        self.ws['I9'] = 'NA'
        self.ws['I10'] = str(self.rep_val['mean_pulse_nl']) + ' (' + str(self.rep_val['min_pulse_nl']) + '-' + str(
                             self.rep_val['max_pulse_nl']) + ')'


    def summary_table(self, rep):
        columns = ['Weeks from Current Session', 'Mean', 'Min', 'Max', 'SD 2', 'CV (2SD)', 'Slope (mL/Week)',
                   'R', 'R-square', 'Sessions', 'P-value']
        self.summary_df = pd.concat([pd.DataFrame([[name, slices['FEV1MAX'].astype(float).mean().round(2),
                                                    slices['FEV1MAX'].astype(float).min().round(2),
                                                    slices['FEV1MAX'].astype(float).max().round(2),
                                                    (slices['FEV1MAX'].astype(float).std() * 2).round(2),
                                                    ((slices['FEV1MAX'].astype(float).std() * 2 / slices[
                                                        'FEV1MAX'].astype(float).mean()) * 100).round(),
                                                    round(
                                                        stats.linregress(slices['Weeks from Start Date'].astype(float),
                                                                         slices['FEV1xK'].astype(float))[0]),
                                                    round(
                                                        stats.linregress(slices['Weeks from Start Date'].astype(float),
                                                                         slices['FEV1xK'].astype(float))[2], 2),
                                                    round(
                                                        stats.linregress(slices['Weeks from Start Date'].astype(float),
                                                                         slices['FEV1xK'].astype(float))[2] ** 2, 3),
                                                    len(slices),
                                                    round(
                                                        (stats.linregress(slices['Weeks from Start Date'].astype(float),
                                                                          slices['FEV1xK'].astype(float))[3] / 2), 2)]],
                                                  columns=columns) for name, slices in zip(rep.actual_week_list, rep.var_data)], ignore_index=True)
        if self.excel == True:
            book = load_workbook(self.save_path)
            writer = pd.ExcelWriter(self.save_path, engine='openpyxl')
            writer.book = book
            writer.sheets = {ws.title: ws for ws in writer.book.worksheets}
            self.summary_df.to_excel(writer, sheet_name='Report', startrow=25, index=False, engine='openpyxl')
            writer.save()

    def generateReport(self):
        self.wb.save(filename=self.save_path)
